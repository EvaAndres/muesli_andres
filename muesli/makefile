# ****************************************************************************
# *
# *                                 M U E S L I   v 1.9
# *
# *
# *     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
# *     Contact: muesli.materials@imdea.org
# *     Author: Ignacio Romero (ignacio.romero@imdea.org)
# *
# *     This file is part of MUESLI.
# *
# *     MUESLI is free software: you can redistribute it and/or modify
# *     it under the terms of the GNU General Public License as published by
# *     the Free Software Foundation, either version 3 of the License, or
# *     (at your option) any later version.
# *
# *     MUESLI is distributed in the hope that it will be useful,
# *     but WITHOUT ANY WARRANTY; without even the implied warranty of
# *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# *     GNU General Public License for more details.
# *
# *     You should have received a copy of the GNU General Public License
# *     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
# *
# ****************************************************************************

#----------------------------------------------------------------------------
#                 detect operating system at runtime
#
#  To force a compilation for windows use:
#          make ARCH=win64
#----------------------------------------------------------------------------
UNAME_S := $(shell uname -s)
UNAME_P := $(shell uname -p)

ifeq ($(UNAME_S),Linux)
	export ARCH := linux_ia64
	CCFLAGS = -D LINUX
	ifeq ($(UNAME_P),x86_64)
		CCFLAGS += -D AMD64
	endif
endif

ifeq ($(UNAME_S),Darwin)
	ifeq ($(UNAME_P),arm)
		export ARCH:=arm64
		CCFLAGS = -D OSX
	else ifeq ($(UNAME_P),arm64)
		export ARCH:=arm64
		CCFLAGS+= -D OSX -target arm64-apple-macos11
	else ifeq ($(UNAME_P),i386)
		export ARCH:=darwin_ia64
		CCFLAGS = -D OSX
	endif
endif



#----------------------------------------------------------------------------
#                                options
#----------------------------------------------------------------------------

INSTALL_BASE := ../lib

export SOURCEDIR   = $(CURDIR)
export MUESLI_LIB  = libmuesli_$(ARCH).a
export MUESLI_PATH = $(CURDIR)
export MUESLI_SRC  = $(SOURCEDIR)
export LIBPATH     = $(MUESLI_PATH)/../lib

# to use EIGEN uncomment the next line
#export WITHEIGEN = true
ifdef WITHEIGEN
	export CPPFLAGS = -DWITHEIGEN -DNDEBUG
	export EIGEN_PATH = /usr/local/include
endif


# maintenance functions
export REMOVE  = @rm
export TIDY    = @rm -f *.bak *~ .DS_Store *.o *.log
export CLEAN   = @rm -f *.bak *.o *~ .DS_Store

INCLUDEPATH  = -I$(SOURCEDIR)/..

ifeq ($(ARCH),darwin_ia64)
	export CPPFLAGS += -O3 -std=c++11 -fpic -Wall $(INCLUDEPATH)
	LINKERFLAGS += -L/usr/local/lib
	AR           = /usr/bin/ar
	ARFLAGS      = cr
	RANLIB  = /usr/bin/ranlib $(RLIBFLAGS)
    export CPP = g++
	ifndef WITHEIGEN
		LINKERFLAGS += -framework Accelerate
	else
		INCLUDEPATH += -I$(EIGEN_PATH)
	endif

else ifeq ($(ARCH),arm64)
    export CPPFLAGS += -target arm64-apple-macos11 -O3 -std=c++11 -fpic -Wall $(INCLUDEPATH)
	LINKERFLAG += -L/usr/local/lib
	AR           = /usr/bin/ar
	ARFLAGS      = cr
	RANLIB = /usr/bin/ranlib $(RLIBFLAGS)
	export CPP = clang
	ifndef WITHEIGEN
		LINKERFLAGS += -framework Accelerate
	else
		INCLUDEPATH += -I$(EIGEN_PATH)
	endif

else ifeq ($(ARCH),linux_ia64)
    export CPPFLAGS += -O3 -std=c++11 -fpic -Wall -march=native $(INCLUDEPATH)
	LINKERFLAGS += -L$(LIBPATH)
	AR           = /usr/bin/ar
	ARFLAGS      = cr
	RANLIB = ranlib $(RLIBFLAGS)
	export CPP = g++
	ifndef WITHEIGEN
		LINKERFLAGS += -llapack -lblas
	else
		INCLUDEPATH += -I$(EIGEN_PATH)
	endif

else ifeq ($(ARCH),win64)
	export CPP = i686-w64-mingw32-g++
	export CPPFLAGS += -O3 -std=c++11 -fpic -Wall $(INCLUDEPATH)
	LINKERFLAGS += -static-libgcc -static-libstdc++ -static -lwinpthread -L$(LIBPATH)
	AR           = i686-w64-mingw32-ar
	ARFLAGS      = cr
	RANLIB = i686-w64-mingw32-ranlib $(RLIBFLAGS)
	ifndef WITHEIGEN
		LINKERFLAGS += -llapack_win64 -lblas_win64
	else
		INCLUDEPATH += -I$(EIGEN_PATH)
	endif
endif



SUBDIRS = \
	$(SOURCEDIR)/Damage \
	$(SOURCEDIR)/Datadriven \
	$(SOURCEDIR)/Diffusion \
	$(SOURCEDIR)/Failure  \
	$(SOURCEDIR)/Finitestrain  \
	$(SOURCEDIR)/Fluid\
	$(SOURCEDIR)/Fcoupled \
	$(SOURCEDIR)/Interface\
	$(SOURCEDIR)/Math  \
	$(SOURCEDIR)/Scoupled  \
	$(SOURCEDIR)/Smallstrain  \
	$(SOURCEDIR)/Utils


MUESLI_FILES = \
	material.o \
	tensor.o \
	Damage/damage.o \
	Damage/jcdamage.o \
	Datadriven/nnfsolid.o \
	Diffusion/ammaterial.o \
	Diffusion/conductor.o \
	Diffusion/species.o \
	Failure/brownmiller.o \
	Failure/jcfailure.o \
	Finitestrain/arrheniustype.o   \
	Finitestrain/arrudaboyce.o \
	Finitestrain/finitestrain.o \
	Finitestrain/fisotropic.o \
	Finitestrain/fplastic.o \
	Finitestrain/fplastic.o \
	Finitestrain/johnsoncook.o \
	Finitestrain/mooney.o \
	Finitestrain/neohook.o \
	Finitestrain/reducedfinitestrain.o \
	Finitestrain/svk.o \
	Finitestrain/yeoh.o \
	Finitestrain/zerilliarmstrong.o \
	Fluid/fluid.o \
	Fluid/newtonian.o \
	Fcoupled/thermofinitestrain.o \
	Fcoupled/fthermomechmass.o \
	Fcoupled/thermojc.o \
	Fcoupled/thermoza.o \
	Fcoupled/fmechmass.o \
	Interface/interface_abaqus.o \
	Interface/interface_lsdyna.o \
	Scoupled/smechmass.o\
	Scoupled/smechmasshyd.o\
	Scoupled/sphasechemomech.o\
	Scoupled/sphasemech.o\
	Scoupled/sthermomech.o\
	Smallstrain/smallstrain.o\
	Smallstrain/elastic.o \
	Smallstrain/reducedsmallstrain.o \
	Smallstrain/sdamage.o \
	Smallstrain/splastic.o \
	Smallstrain/viscoelastic.o \
	Smallstrain/viscoplastic.o \
	Utils/utils.o

ifndef WITHEIGEN
	MUESLI_FILES +=\
	Math/mtensor.o \
	Math/mmatrix.o \
	Math/mrealvector.o
endif

default: library

export TASKS_FILE = $(SOURCEDIR)/makefile.tasks
include $(TASKS_FILE)

library: material.o tensor.o
	@( \
	for f in $(SUBDIRS); \
	do \
		$(MAKE) -C $$f compile; \
	done );
	($(AR) $(ARFLAGS) $(LIBPATH)/$(MUESLI_LIB) $(MUESLI_FILES))
	$(RANLIB) $(LIBPATH)/$(MUESLI_LIB)


install:
	test -d $(INSTALL_BASE) || mkdir $(INSTALL_BASE)
	test -d $(INSTALL_BASE)/lib || mkdir $(INSTALL_BASE)/lib
	test -d $(INSTALL_BASE)/include || mkdir $(INSTALL_BASE)/include
	rm -rf $(INSTALL_BASE)/include/muesli
	cp -p $(LIBPATH)/$(MUESLI_LIB) $(INSTALL_BASE)/lib
	find . -name '*.h' -print | cpio -pdlv $(INSTALL_BASE)/include/muesli

#   find . -name '*.h' -exec install -m 0755 '{}' $(INSTALL_BASE)/include/muesli ';'

all: library install
	rm -f Test/test.o
	@( $(MAKE) -C Test all );
	g++ Test/test.o -lmuesli -o testmuesli
	./testmuesli

example: Test/example.o
	@( $(MAKE) -C Test compile);
	g++ -L$(LIBPATH) Test/example.o -lmuesli_$(ARCH) -o Test/examplemuesli

help:
	@echo "MUESLI, the Material UnivErSal LIbrary"
	@echo "Usage:"
	@echo "  make, to build the library and install it"
	@echo "  make clean, to remove all but critical files"
	@echo "  make info, to get information about the variables"
	@echo "  make library, to build libmuesli.a"
	@echo "  make test, to compile the test program"
	@echo "  make tidy, to remove all intermediate files"
	@echo "  make zip, to compress the MUESLI creating a zip file"

info:
	@echo ""
	@echo ARCH=$(ARCH)
	@echo tbb=$(tbb)
	@echo ""
	@echo "      Variables used to build muesli"
	@echo "      ------------------------------"
	@echo ""
	@echo UNAME_S=$(UNAME_S)
	@echo UNAME_P=$(UNAME_P)
	@echo ARCH=$(ARCH)
	@echo CPP=$(CPP)
	@echo CPPFLAGS=$(CPPFLAGS)

test: Test/test.o
	@( $(MAKE) -C Test compile );
	g++ -L$(LIBPATH) -llapack -lblas Test/test.o -lmuesli_$(ARCH) -o testmuesli
	@( rm -f Test/test.o )

# use this test if linking with a developing library, not installed yet
devtest: library Test/test.o
	@( $(MAKE) -C Test compile );
	g++ Test/test.o -L$(LIBPATH) $(LINKERFLAGS) -lmuesli_$(ARCH) -o testmuesli
	./testmuesli

headers:
	find . -name "*.h" -exec replace_header.sh {} \;
	find . -name "*.cpp" -exec replace_header.sh {} \;
	find . -name "makefile" -exec replace_header_makefile.sh {} \;

maintenance:
	find . -name '*.h' -print0 | xargs -0 sed -E -i '' 's/subdirs/SUBDIRS/g'

runexample: library example
	@( cd Test; ./examplemuesli; gnuplot generate.gnuplot; mpost example.mp; mptopdf example.0 example.1)
	@( cd Test; rm -f mpx* *.mp *.mpx example.0)

zip: clean
	(cd ..; tar czvf muesli.tgz muesli)
