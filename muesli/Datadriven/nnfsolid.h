/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/


#pragma once
#ifndef _muesli_nnfsolid_
#define _muesli_nnfoslid_

#include "muesli/Finitestrain/finitestrain.h"
#include "muesli/Math/matrix.h"
#include "muesli/Math/realvector.h"
#include <iostream>
#include <vector>
#include <string>

namespace muesli
{

class nnet
{
public:
                                        nnet(const std::string& filename);
                                        ~nnet();
    realvector                          operator()(const realvector& input) const;
    void                                info(std::ostream &of=std::cout) const;

private:
    typedef enum
    {
        ACTIVATION_SIGMOID,
        ACTIVATION_RELU,
        ACTIVATION_TANH,
        ACTIVATION_LINEAR,
        ACTIVATION_SOFTPLUS,
        ACTIVATION_SELU
    } activationFunction;


    std::vector<size_t>                 layerSize;
    std::vector<activationFunction>     activation;
    static std::vector<std::string>  activationNames;
    std::vector<matrix>                 weights;
    std::vector<realvector>             biases;
    std::vector<realvector>             normMean;
    std::vector<realvector>             normStd;
    unsigned                            input_output[2];

    void                                activationFilter(const activationFunction& af, realvector& v) const;
};


class nnFsolidMP;

class nnFsolid : public finiteStrainMaterial
{
public:
                                        nnFsolid(const std::string& name,
                                                 const std::string& strainStressFile);
    virtual                             ~nnFsolid();

    virtual double                      characteristicStiffness() const;
    virtual muesli::finiteStrainMP*     createMaterialPoint() const;
    virtual void                        print(std::ostream &of=std::cout) const;
    virtual void                        setRandom();
    virtual bool                        test(std::ostream &of=std::cout);

protected:
    std::string                         strainStressFilename;
    nnet                                strainStressNN;

    friend class nnFsolidMP;
};



class nnFsolidMP : public finiteStrainMP
{

public:
                                nnFsolidMP(const nnFsolid& m);
    virtual                     ~nnFsolidMP(){}
    virtual  void               setRandom();


    // energies
    virtual double              energyDissipationInStep() const;
    virtual itensor             dissipatedEnergyDF() const;
    virtual double              effectiveStoredEnergy() const;
    virtual double              kineticPotential() const;
    virtual double              storedEnergy() const;

    // stresses
    virtual void                CauchyStress(istensor& sigma) const;
    virtual void                firstPiolaKirchhoffStress(itensor& P) const;
    virtual void                secondPiolaKirchhoffStress(istensor &S) const;

    virtual double              plasticSlip() const;

    // tangents
    virtual void                contractWithSpatialTangent(const ivector& v1, const ivector& v2,itensor& T) const;
    virtual void                contractWithConvectedTangent(const ivector& v1, const ivector& v2,itensor& T) const;
    virtual void                convectedTangentTimesSymmetricTensor(const istensor& T,istensor& result) const;


    virtual void                contractWithDeviatoricTangent(const ivector &v1, const ivector& v2,itensor &T) const;
    virtual void                contractWithMixedTangent(istensor& CM) const;
    virtual void                convectedTangent(itensor4& c) const;
    virtual double              volumetricStiffness() const;

    virtual void                commitCurrentState();
    virtual void                resetCurrentState();
    virtual void                setConvergedState(const double time, const itensor& F);
    virtual void                updateCurrentState(const double theTime, const itensor& F);

    virtual materialState       getConvergedState() const;
    virtual materialState       getCurrentState() const;

protected:
    nnFsolid const              *mat;
    friend class                nnFsolid;
};

}

#endif
