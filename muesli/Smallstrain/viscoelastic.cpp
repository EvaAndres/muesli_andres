/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/


#include "viscoelastic.h"
#include "muesli/Math/realvector.h"
#include "muesli/Math/matrix.h"

#include <cmath>
#include <vector>

#define DEFAULT_LEARNING_RATE 1e-2
#define DEFAULT_REGULARIZATION 1e-5


// Define a structure to hold the Prony series parameters
struct PronySeriesParameters
{
    double G_inf;
    realvector G_i;
    realvector tau_i;

    PronySeriesParameters(int n) : G_i(n), tau_i(n) {}
};


bool computePronyCoefficients(const std::vector<double> &freq,
                              const std::vector<double> &storage,
                              const std::vector<double> &loss, double &Ginf,
                              size_t nprony, std::vector<double> &gvec,
                              std::vector<double> &tau,
                              const double& lr,
                              const double& r);

double evaluateError(const std::vector<double>& omega,
                     const std::vector<double>& Gp_data,
                     const std::vector<double>& Gpp_data,
                     const PronySeriesParameters& params);

void newtonRaphsonStep(const std::vector<double>& omega,
                       const std::vector<double>& Gp_data,
                       const std::vector<double>& Gpp_data,
                       PronySeriesParameters& params,
                       double learning_rate,
                       double regul,
                       realvector &delta_params);

using namespace muesli;


/* the input line of a viscoelastic material should set the elastic constants

 material, label = 1, type = viscoelastic,
 E = 1000.0, nu = 0.3,       # this sets the purely elastic constants

 and then as many pairs (eta,tau) as desired. these can be given in any
 order, but eta_i <-> tau_i are paired

 eta = 1.0, tau = 1.3, eta = 2.0, tau = 0.7, ....

 Alternatively, one could provide triplets (freq_k, storage_k, loss_k) where storage_k and
 loss_k are the storage and loss modulus, respectively, at frequency freq_k. For example, one
 could write in the command line
 ...
 freq = 1.0, loss = 2.0, storage = 3.0, freq = 2.0, loss = 3.2, storage = 7.0, ...
 ...

 The triplets have to be in order (storage_k and loss_k right after freq_k)

 */
viscoelasticMaterial::viscoelasticMaterial(const std::string &name,
                                           const materialProperties &cl)
    : smallStrainMaterial(name, cl), E(0.0), nu(0.0), lambda(0.0), mu(0.0),
      bulk(0.0), cp(0.0), cs(0.0),
      learn_rate(DEFAULT_LEARNING_RATE), regularization(DEFAULT_REGULARIZATION)
{
    muesli::assignValue(cl, "young",   E);
    muesli::assignValue(cl, "poisson", nu);
    muesli::assignValue(cl, "lambda",  lambda);
    muesli::assignValue(cl, "mu",      mu);
    muesli::assignValue(cl, "eta",     eta);
    muesli::assignValue(cl, "tau",     tau);
    muesli::assignValue(cl, "freq",    freq);
    muesli::assignValue(cl, "storage", storage);
    muesli::assignValue(cl, "loss",    loss);
    muesli::assignValue(cl, "learnrate", learn_rate);
    muesli::assignValue(cl, "regularization", regularization);

    if (learn_rate == 0.0) learn_rate = DEFAULT_LEARNING_RATE;
    if (regularization == 0.0) regularization = DEFAULT_REGULARIZATION;

    // E and nu have priority. If they are defined, define lambda and mu
    if (E*E > 0)
    {
        lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
        mu     = E/2.0/(1.0+nu);
    }
    else
    {
        nu = lambda / 2.0 / (lambda+mu);
        E  = mu*2.0*(1.0+nu);
    }

    // we set all the constants, so that later on they can be recovered fast
    bulk = lambda + 2.0/3.0 * mu;
    double rho = density();
    if (rho > 0.0)
    {
        cp   = sqrt((lambda+2.0*mu)/rho);
        cs   = sqrt(mu/rho);
    }

    // compute eta and tau from the Prony series corresponding to G' and G''
    if (!freq.empty())
    {
        std::vector<double> gvec;
        size_t nprony;

        if (!tau.empty())
        {
            nprony = tau.size();
            for (unsigned a=0; a<tau.size(); a++) gvec.push_back(eta[a]/tau[a]);
        }
        else
        {
            double dnprony;
            muesli::assignValue(cl, "nprony", dnprony);
            nprony = static_cast<size_t>(dnprony);

            tau = std::vector<double>(nprony, 1.0);
            gvec = tau;
        }

        computePronyCoefficients(freq, storage, loss, mu, nprony, gvec, tau, learn_rate, regularization);

        // recover eta from G
        eta.resize(nprony);
        for (unsigned a=0; a<gvec.size(); a++)
            eta[a] = gvec[a]*tau[a];
    }
}




viscoelasticMaterial::viscoelasticMaterial(const std::string& name,
                                             const double xE, const double xnu, const double rho,
                                             const size_t nvisco, const double* xeta, const double *xtau)
:
    smallStrainMaterial(name),
    E(xE), nu(xnu), lambda(0.0), mu(0.0), bulk(0.0), cp(0.0), cs(0.0)
{
    setDensity(rho);

    // E and nu have priority. If they are defined, define lambda and mu
    if (E*E > 0)
    {
        lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
        mu     = E/2.0/(1.0+nu);
    }
    else
    {
        nu = lambda / 2.0 / (lambda+mu);
        E  = mu*2.0*(1.0+nu);
    }

    // we set all the constants, so that later on all mlog them can be recovered fast
    bulk = lambda + 2.0/3.0 * mu;
    if (rho > 0.0)
    {
        cp   = sqrt((lambda+2.0*mu)/rho);
        cs   = sqrt(mu/rho);
    }

    if (nvisco > 0 && xeta != 0 && xtau != 0 )
    {
        for (size_t k=0; k<nvisco; k++)
        {
            eta.push_back(xeta[k]);
            tau.push_back(xtau[k]);
        }
    }
}




bool computePronyCoefficients(const std::vector<double>& freq,
                              const std::vector<double>& storage,
                              const std::vector<double>& loss,
                              double& Ginf,
                              size_t nprony,
                              std::vector<double>& gvec,
                              std::vector<double>& tau,
                              const double& lr,
                              const double& r)
{
    PronySeriesParameters params(nprony);
    params.G_inf = Ginf;
    double arr[gvec.size()];
    std::copy(gvec.begin(), gvec.end(), arr);
    params.G_i = realvector(nprony, arr);
    std::copy(tau.begin(), tau.end(), arr);
    params.tau_i = realvector(nprony, arr);

    // Newton-Raphson parameters
    double learning_rate = lr;
    int max_iterations = 10000;
    double tolerance = 1e-6;

    double ps[2*nprony+1];
    std::fill(ps,ps+(2*nprony+1),1);
    realvector delta_params(2*nprony+1, ps);

    // Perform the Newton-Raphson method
    for (int iter = 0; iter < max_iterations; ++iter) {
        double error = evaluateError(freq, storage, loss, params);

        double delP = 0;
        for(int i = 0; i< delta_params.size();++i){
            delP += pow(delta_params[i],2);
        }
        delP = sqrt(delP/delta_params.size());

        if(error < tolerance || delP < tolerance){
            break;
        }

        newtonRaphsonStep(freq, storage, loss,
                          params, learning_rate, r, delta_params);
    }

    Ginf = params.G_inf;

    for (int iter = 0; iter < nprony; ++iter) {
        gvec[iter] = params.G_i[iter];
        tau[iter] = params.tau_i[iter];
    }
    return true;
}



// Function to compute the storage modulus G' from Prony series parameters
double computeStorageModulus(double omega, const PronySeriesParameters& params)
{
    double Gp = params.G_inf;
    for (int i = 0; i < params.G_i.size(); ++i) {
        Gp += (params.G_i[i] * pow(omega * params.tau_i[i], 2)) / (1 + pow(omega * params.tau_i[i], 2));
    }
    return Gp;
}




// Function to compute the loss modulus G'' from Prony series parameters
double computeLossModulus(double omega, const PronySeriesParameters& params)
{
    double Gpp = 0.0;
    for (int i = 0; i < params.G_i.size(); ++i) {
        Gpp += (params.G_i[i] * omega * params.tau_i[i]) / (1 + pow(omega * params.tau_i[i], 2));
    }
    return Gpp;
}




// Function to evaluate the error for a given data point and parameters
double evaluateError(const std::vector<double>& omega,
                     const std::vector<double>& Gp_data,
                     const std::vector<double>& Gpp_data,
                     const PronySeriesParameters& params)
{
    double error = 0.0;
    for (size_t i = 0; i < omega.size(); ++i) {
        double Gp_model = computeStorageModulus(omega[i], params);
        double Gpp_model = computeLossModulus(omega[i], params);
        error += pow(Gp_data[i] - Gp_model, 2) + pow(Gpp_data[i] - Gpp_model, 2);
    }
    error = sqrt(error/omega.size());
    return error;
}




// Function to compute the Jacobian matrix and residuals for the system of equations
void computeJacobianAndResiduals(const std::vector<double>& omega,
                                 const std::vector<double>& Gp_data,
                                 const std::vector<double>& Gpp_data,
                                 const PronySeriesParameters& params,
                                 realvector& residuals,
                                 matrix& Jacobian)
{
    int n = params.G_i.size();
    residuals.setZero();
    Jacobian.setZero();

    for (size_t i = 0; i < omega.size(); ++i) {
        double omega_i = omega[i];
        double Gp_model = computeStorageModulus(omega_i, params);
        double Gpp_model = computeLossModulus(omega_i, params);

        double Gp_residual = Gp_data[i] - Gp_model;
        double Gpp_residual = Gpp_data[i] - Gpp_model;

        // Residuals
        residuals[i] = -Gp_residual;
        residuals[omega.size() + i] = -Gpp_residual;

        // Partial derivatives for Jacobian
        for (int j = 0; j < n; ++j) {
            double omega_tau = omega_i * params.tau_i[j];
            double denom = 1 + pow(omega_tau, 2);
            double dGp_dGi = pow(omega_tau, 2) / denom;
            double dGpp_dGi = omega_tau / denom;
            double dGp_dtaui = params.G_i[j] * 2 * omega_i * omega_tau / pow(denom, 2);
            double dGpp_dtaui = params.G_i[j] * omega_i * (1 - pow(omega_tau, 2)) / pow(denom, 2);

            Jacobian(i, j) = -dGp_dGi;
            Jacobian(i, n + j) = -dGp_dtaui;
            Jacobian(omega.size() + i, j) = -dGpp_dGi;
            Jacobian(omega.size() + i, n + j) = -dGpp_dtaui;
        }
        Jacobian(i, 2*n) = -1.0;
    }
}




// Function to perform the Newton-Raphson method to update Prony series parameters
void newtonRaphsonStep(const std::vector<double>& omega,
                       const std::vector<double>& Gp_data,
                       const std::vector<double>& Gpp_data,
                       PronySeriesParameters& params,
                       double learning_rate, double r, realvector &delta_params)
{
    int m = omega.size();
    int n = params.G_i.size();

    realvector residuals(2 * m);
    matrix Jacobian(2 * m, 2 * n + 1);

    computeJacobianAndResiduals(omega, Gp_data, Gpp_data, params, residuals, Jacobian);
    matrix J = Jacobian;
    Jacobian.transpose();

    matrix JTJ = Jacobian * J;
    realvector JTR = Jacobian * residuals;

    // regularization
    double lambda = r;
    for (int i = 0; i < JTJ.rows(); ++i) {
        JTJ(i, i) += lambda;
    }

    //Jacobian.solveFull(residuals);
    JTJ.solveFull(JTR);
    //realvector delta_params = residuals;

    delta_params = JTR;

    //Eigen::VectorXd delta_params = Jacobian.colPivHouseholderQr().solve(residuals);

    // Define upper limit for tau_i
    double tau_upper_limit = 10.0; // Adjust this value based on your requirements

    // Update the parameters using the computed delta
    for (int i = 0; i < n; ++i) {
        params.G_i[i] += learning_rate * delta_params[i];
        params.tau_i[i] += learning_rate * delta_params[n + i];
        // Ensure positivity of G_i and tau_i
        if (params.G_i[i] < 0) params.G_i[i] = 1e-6;
        if (params.tau_i[i] < 0) params.tau_i[i] = 1e-6;
        if (params.tau_i[i] > tau_upper_limit) params.tau_i[i] = tau_upper_limit;
    }
    params.G_inf += learning_rate * delta_params[2 * n];
    // Ensure positivity of G_inf
    if (params.G_inf < 0) params.G_inf = 1e-6;
}




//--------------------------------------------------------------------





bool viscoelasticMaterial::check() const
{
    bool ret = true;

    if ( eta.size() != tau.size() )  ret = false;

    return ret;
}




/* an object of the type "viscoelastic" creates a material point of type viscoelastic. The
 material point holds information that is not part of the material itself but
 that is particular of the specific (physical) point.
 */
smallStrainMP* viscoelasticMaterial::createMaterialPoint() const
{
    smallStrainMP* mp = new viscoelasticMP(*this);
    return mp;
}




// this function is much faster than the one with string property names, because
// avoids string comparisons. It should be used.
double viscoelasticMaterial::getProperty(const propertyName p) const
{
    return 0.0;
}





/* this function is always called once the material is defined, so apart from
 printing its information, we take the opportunity to clean up some of its
 data, in particular, setting all the possible constants
 */
void viscoelasticMaterial::print(std::ostream &of) const
{
    of  << "\n   Small strain, viscoelastic, isotropic material. ";
    of  << "\n   In 3D problems, the viscoelastic response affects only the";
    of  << "\n   deviatoric strain. In 1D problems, the whole respose is";
    of  << "\n   viscoelastic and the limit value of the stiffness is E.\n";
    of  << "\n   Young modulus   E      : " << E;
    of  << "\n   Poisson ratio   nu     : " << nu;
    of  << "\n   Lame constants  Lambda : " << lambda;
    of  << "\n                   Mu     : " << mu;
    of  << "\n   Bulk modulus    K      : " << bulk;
    of  << "\n   Density                : " << density();
    of  << "\n   Wave velocities C_p    : " << cp;
    of  << "\n                   C_s    : " << cs;

    if (!freq.empty())
    {
        of << "\n\n Frequency storage and loss moduli"
           << "\n ----------------------------------";
        for (size_t a=0; a<freq.size(); a++)
            of  << "\n   (f_i, G'_i, G''_i)     : (" << freq[a] << ", " << storage[a] << ", " << loss[a] << ")";

        of << "\n\n Parameter for Prony series computations:"
           << "\n ---------------------------------------"
           << "\n Learning rate: " << learn_rate
           << "\n Regularization: " << regularization
           << "\n\n Prony series coefficients"
           << "\n -------------------------";
    }

    for (size_t a=0; a<eta.size(); a++)
        of  << "\n   (eta_i, tau_i) pair    : (" << eta[a] << ", " << tau[a] << ")";
}




void viscoelasticMaterial::setRandom()
{
    material::setRandom();

    E      = muesli::randomUniform(1.0, 10000.0);
    nu     = muesli::randomUniform(0.05, 0.45);
    lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
    mu     = E/2.0/(1.0+nu);
    cp     = sqrt((lambda+2.0*mu)/density());
    cs     = sqrt(2.0*mu/density());
    bulk   = lambda + 2.0/3.0 * mu;

    size_t n = (size_t) muesli::discreteUniform(1, 5);
    for (size_t a=0; a<n; a++)
    {
        eta.push_back( muesli::randomUniform(1.0,10.0) );
        tau.push_back( muesli::randomUniform(0.1,2.0) );
    }
}




bool viscoelasticMaterial::test(std::ostream  &of)
{
    bool isok=true;
    this->setRandom();

    smallStrainMP* p = this->createMaterialPoint();
    p->setRandom();

    isok = p->testImplementation(of);
    delete p;

    return isok;
}




double viscoelasticMaterial::waveVelocity() const
{
    return cp;
}




viscoelasticMP::viscoelasticMP(const viscoelasticMaterial &m) :
smallStrainMP(m),
theta_n(0.0),
theta_c(0.0),
theViscoelasticIsotropicMaterial(m)
{
    size_t v = theViscoelasticIsotropicMaterial.eta.size();
    epsvisco_n.resize(v);
    epsvisco_c.resize(v);

    for (size_t a=0; a<v; a++)
    {
        epsvisco_n[a].setZero();
        epsvisco_c[a].setZero();
    }
}




viscoelasticMP::~viscoelasticMP()
{
    epsvisco_n.clear();
    epsvisco_c.clear();
}




void viscoelasticMP::commitCurrentState()
{
    smallStrainMP::commitCurrentState();
    epsvisco_n = epsvisco_c;
    edev_n     = edev_c;
    theta_n    = theta_c;
}




/* Given the fourth order tensor of viscoelasticities C, and two vectors v, w
 * compute the second order tensor T with components
 *   T_ij = C_ipjq v_p w_q
 *
 *  Note that the result is not symmetric.
 */

void viscoelasticMP::contractWithTangent(const ivector &v1, const ivector &v2,
                                           itensor &T) const
{
    // shear response
    const double dt    = time_c - time_n;
    const double muinf = theViscoelasticIsotropicMaterial.mu;
    double       mu_eq = muinf;

    for (size_t a=0; a < epsvisco_n.size(); a++)
    {
        double   taua  = theViscoelasticIsotropicMaterial.tau[a];
        double   mua   = theViscoelasticIsotropicMaterial.eta[a]/(2.0*taua);
        mu_eq         +=  mua * taua/(dt+taua);
    }

    // bulk response
    const double& kinf     = theViscoelasticIsotropicMaterial.bulk;
    const double lambda_eq = kinf - 2.0/3.0*mu_eq;

    itensor A, B;

    T = itensor::identity();
    T *= v1.dot(v2) * mu_eq;

    A = itensor::dyadic(v1, v2);
    B = A.transpose();

    A *= lambda_eq;
    B *= mu_eq;

    T += A;
    T += B;
}




void viscoelasticMP::contractWithDeviatoricTangent(const ivector &v1, const ivector &v2, itensor &T) const
{
    const double dt = time_c - time_n;

    // shear response
    double   muinf = theViscoelasticIsotropicMaterial.mu;
    double   mu_eq = muinf;
    for (size_t a=0; a < epsvisco_n.size(); a++)
    {
        double   tau   = theViscoelasticIsotropicMaterial.tau[a];
        double   mua   = theViscoelasticIsotropicMaterial.eta[a]/(2.0*tau);

        mu_eq +=  mua * tau/(dt+tau);
    }

    T = mu_eq * v1.dot(v2) * itensor::identity()
    +   mu_eq *              itensor::dyadic(v2, v1)
    -   2.0/3.0*mu_eq *      itensor::dyadic(v1, v2);
}




double viscoelasticMP::deviatoricEnergy() const
{
    return theViscoelasticIsotropicMaterial.mu * edev_c.contract(edev_c);
}




void viscoelasticMP::deviatoricStress(istensor& s) const
{
    const double dt    = time_c - time_n;
    const double muinf = theViscoelasticIsotropicMaterial.mu;

    // contribution from purely elastic terms
    s = 2.0*muinf*edev_c;

    // viscous contributions from elements
    for (size_t a=0; a < epsvisco_n.size(); a++)
    {
        double   taua  = theViscoelasticIsotropicMaterial.tau[a];
        double   mua   = theViscoelasticIsotropicMaterial.eta[a]/(2.0*taua);
        istensor einc  = edev_c - epsvisco_n[a];

        s += 2.0 * mua * taua/(dt+taua) * einc;
    }
}




double viscoelasticMP::energyDissipationInStep() const
{
    double diss  = 0.0;
    double dt = time_c - time_n;

    // elastic potential
    for (size_t a=0; a < epsvisco_n.size(); a++)
    {
        double   taua  = theViscoelasticIsotropicMaterial.tau[a];
        double   etaa  = theViscoelasticIsotropicMaterial.eta[a];
        double   mua   = etaa/(2.0*taua);
        istensor einc  = edev_c - epsvisco_n[a];

        diss += mua * (taua/dt)/(dt+taua) * einc.squaredNorm();
    }

    return diss;
}




double viscoelasticMP::effectiveStoredEnergy() const
{
    const double dt = time_c - time_n;
    return storedEnergy() + dt*kineticPotential();
}




// necessary to compute in smallthermo material the dissipated energy
istensor viscoelasticMP::getConvergedPlasticStrain() const
{
    return istensor();
}




materialState viscoelasticMP::getConvergedState() const
{
    materialState state_n = smallStrainMP::getConvergedState();

    state_n.theDouble.push_back(theta_n);
    state_n.theStensor.push_back(edev_n);
    state_n.theStensor.insert( state_n.theStensor.end(), epsvisco_n.begin(), epsvisco_n.end() );

    return state_n;
}




// necessary to compute in smallthermo material the dissipated energy
istensor viscoelasticMP::getCurrentPlasticStrain() const
{
    return istensor();
}




materialState viscoelasticMP::getCurrentState() const
{
    materialState state_c = smallStrainMP::getCurrentState();

    state_c.theDouble.push_back(theta_c);
    state_c.theStensor.push_back(edev_c);
    state_c.theStensor.insert( state_c.theStensor.end(), epsvisco_c.begin(), epsvisco_c.end() );

    return state_c;
}




double viscoelasticMP::kineticPotential() const
{
    double dt = time_c - time_n;

    double psi = 0.0;
    for (size_t a=0; a < epsvisco_n.size(); a++)
    {
        double   etaa  = theViscoelasticIsotropicMaterial.eta[a];
        istensor einc  = epsvisco_c[a] - epsvisco_n[a];

        psi += 0.5 * etaa * einc.squaredNorm() / (dt*dt);
    }
    return psi;
}




double viscoelasticMP::plasticSlip() const
{
    return 0.0;
}




void viscoelasticMP::resetCurrentState()
{
    smallStrainMP::resetCurrentState();
    epsvisco_c = epsvisco_n;
    edev_c     = edev_n;
    theta_c    = theta_n;
}




void viscoelasticMP::setConvergedState(const double& tn, const istensor& epsn,
                                         const std::vector<istensor>& epsv, const istensor& epsdev, const double& theta)
{
    time_n     = tn;
    eps_n      = epsn;
    epsvisco_n = epsv;
    edev_n     = epsdev;
    theta_n    = theta;
}




void viscoelasticMP::setRandom()
{
    smallStrainMP::setRandom();
    theta_c = theta_n = muesli::randomUniform(0.01, 0.1);
    istensor tmp;
    tmp.setRandom();
    edev_c  = edev_n  = istensor::deviatoricPart(tmp);

    for (size_t a=0; a < epsvisco_n.size(); a++)
    {
        tmp.setRandom();
        epsvisco_n[a] = istensor::deviatoricPart(tmp);
    }
    epsvisco_c = epsvisco_n;

}




double viscoelasticMP::shearStiffness() const
{
    return theViscoelasticIsotropicMaterial.mu;
}




double viscoelasticMP::storedEnergy() const
{
    // elastic potential
    const double  muinf = theViscoelasticIsotropicMaterial.mu;
    const double  kinf  = theViscoelasticIsotropicMaterial.bulk;

    double W = 0.5*kinf*theta_c*theta_c + muinf*edev_c.squaredNorm();

    for (size_t a=0; a < epsvisco_n.size(); a++)
    {
        double   taua  = theViscoelasticIsotropicMaterial.tau[a];
        double   mua   = theViscoelasticIsotropicMaterial.eta[a]/(2.0*taua);
        istensor einc  = edev_c - epsvisco_c[a];
        W += mua * einc.squaredNorm();
    }
    return W;
}




void viscoelasticMP::stress(istensor& sigma) const
{
    deviatoricStress(sigma);
    sigma += theViscoelasticIsotropicMaterial.bulk * theta_c * istensor::identity();
}




void viscoelasticMP::tangentTensor(itensor4& C) const
{
    // shear response
    const double dt    = time_c - time_n;
    const double muinf = theViscoelasticIsotropicMaterial.mu;

    double mu_eq = muinf;
    for (size_t a=0; a < epsvisco_n.size(); a++)
    {
        double   taua  = theViscoelasticIsotropicMaterial.tau[a];
        double   mua   = theViscoelasticIsotropicMaterial.eta[a]/(2.0*taua);
        mu_eq +=  mua * taua/(dt+taua);
    }

    // bulk response
    const double& kinf  = theViscoelasticIsotropicMaterial.bulk;
    double lambda_eq = kinf - 2.0/3.0*mu_eq;


    C.setZero();

    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    if (i==j && k==l) C(i,j,k,l) += lambda_eq;
                    if (i==k && j==l) C(i,j,k,l) += mu_eq;
                    if (i==l && j==k) C(i,j,k,l) += mu_eq;
                }
}




thPotentials viscoelasticMP::thermodynamicPotentials() const
{
    thPotentials tp;
    return tp;
}




void viscoelasticMP::updateCurrentState(const double theTime, const istensor& strain)
{
    smallStrainMP::updateCurrentState(theTime, strain);
    edev_c  = istensor::deviatoricPart(strain);
    theta_c = strain.trace();

    // viscous contributions from prony elements
    const double dt = time_c - time_n;
    for (size_t a=0; a < epsvisco_n.size(); a++)
    {
        const double& taua = theViscoelasticIsotropicMaterial.tau[a];
        epsvisco_c[a]      = epsvisco_n[a] + dt/(dt+taua)* (edev_c - epsvisco_n[a]);
    }
}




double viscoelasticMP::volumetricEnergy() const
{
    double V = 0.5* theViscoelasticIsotropicMaterial.bulk * theta_c * theta_c;
    return V;
}




double viscoelasticMP::volumetricStiffness() const
{
    return theViscoelasticIsotropicMaterial.bulk;
}
