/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/

#include <stdio.h>
#include "fthermomechmass.h"
#include "muesli/Finitestrain/finitestrainlib.h"

using namespace muesli;



fThermoMechMassMaterial::fThermoMechMassMaterial(const std::string& name,
                                                 const materialProperties& cl)
:
material(name, cl),
_thermalExpansion(0.0),
_heatCapacity(0.0),
_massExpansion(0.0),
_mu0(0.0),
_R(8.34)
{
    muesli::assignValue(cl, "thermal_expansion", _thermalExpansion);
    muesli::assignValue(cl, "heat_capacity", _heatCapacity);
    muesli::assignValue(cl, "mass_expansion", _massExpansion);
    muesli::assignValue(cl, "muref", _mu0);
    muesli::assignValue(cl, "r", _R);
}




bool fThermoMechMassMaterial::check() const
{
    return true;
}




void fThermoMechMassMaterial::print(std::ostream &of) const
{
    of  << "\n Thermo - chemo - mechanical coupled material."
    << "\n Variational formulation: " << isVariational()
    << "\n Model constants:"
    << "\n    Thermal expansion coeff. : " << _thermalExpansion
    << "\n    Mass expansion coeff.    : " << _massExpansion
    << "\n    Heat capacity            : " << _heatCapacity
    << "\n    Reference chemical pot.  : " << _mu0
    << "\n    Gas constant             : " << _R
    << "\n    Reference temperature    : " << referenceTemperature();
}




double fThermoMechMassMaterial::referenceChemicalPotential() const
{
    return _mu0;
}




void fThermoMechMassMaterial::setRandom()
{
    material::setRandom();

    _thermalExpansion = muesli::randomUniform(1.0, 5.0)*1e-3;
    _massExpansion    = muesli::randomUniform(1.0, 5.0)*1e-3;
    _heatCapacity     = muesli::randomUniform(1.0, 5.0)*1e-5;
    _R                = muesli::randomUniform(1.0, 10.0);
    _mu0 = muesli::randomUniform(1.0, 10.0);
}




bool fThermoMechMassMaterial::test(std::ostream &of)
{
    bool isok = true;
    setRandom();
    muesli::fThermoMechMassMP* p = this->createMaterialPoint();

    isok = p->testImplementation(of);
    return isok;
}




fThermoMechMassMP::fThermoMechMassMP(const fThermoMechMassMaterial& m) :
theThermoMechMassMaterial(m),
time_n(0.0), temp_n(m.referenceTemperature()), J_n(1.0), mu_n(0.0), c_n(0.0),
time_c(0.0), temp_c(m.referenceTemperature()), J_c(1.0), mu_c(0.0), c_c(0.0)
{
    mu_n = mu_c = m._mu0;
    gradT_n.setZero();
    gradT_c.setZero();
    gradMu_n.setZero();
    gradMu_c.setZero();
    F_n = itensor::identity();
    F_c = itensor::identity();
}




double fThermoMechMassMP::chemicalPotential() const
{
    return mu_c;
}




void fThermoMechMassMP::CauchyStress(istensor& sigma) const
{
    istensor tau;
    KirchhoffStress(tau);
    sigma = 1.0/J_c * tau;
}




void fThermoMechMassMP::CauchyStressVector(double S[6]) const
{
    istensor sigma;
    CauchyStress(sigma);
    muesli::ContraContraSymTensorToVector(sigma, S);
}




void fThermoMechMassMP::commitCurrentState()
{
    time_n   = time_c;
    temp_n   = temp_c;
    J_n      = J_c;
    mu_n     = mu_c;
    c_n      = c_c;
    gradMu_n = gradMu_c;
    F_n      = F_c;
}




double fThermoMechMassMP::concentration() const
{
    return c_c;
}




void fThermoMechMassMP::contractWithAllTangents(const ivector &v1,
                                                const ivector& v2,
                                                itensor&  Tdev,
                                                istensor& Tmixed,
                                                double&   Tvol) const
{

}




void fThermoMechMassMP::contractWithConvectedTangent(const ivector& v1, const ivector& v2, itensor& T) const
{
    itensor4 c;
    convectedTangent(c);

    T.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    T(i,k) += c(i,j,k,l)*v1(j)*v2(l);
                }
}




void fThermoMechMassMP::contractWithDeviatoricTangent(const ivector& v1, const ivector& v2, itensor& T) const
{
    const istensor id = istensor::identity();
    itensor4 Pdev;
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                    Pdev(i,j,k,l) = 0.5 * ( id(i,k)*id(j,l) + id(i,l)*id(j,k) ) - 1.0/3.0 * id(i,j)*id(k,l);

    itensor4 st;
    spatialTangent(st);

    itensor4 cdev;
    cdev.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                    for (unsigned m=0; m<3; m++)
                        for (unsigned n=0; n<3; n++)
                            for (unsigned p=0; p<3; p++)
                                for (unsigned q=0; q<3; q++)
                                    cdev(i,j,p,q) += Pdev(i,j,k,l)*st(k,l,m,n)*Pdev(m,n,p,q);

    T.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    T(i,k) += cdev(i,j,k,l)*v1(j)*v2(l);
                }
}




// CM_ij = P_ijkl*c_klmm
void fThermoMechMassMP::contractWithMixedTangent(istensor& CM) const
{
    const istensor id = istensor::identity();
    itensor4 Pdev;
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                    Pdev(i,j,k,l) = 0.5 * ( id(i,k)*id(j,l) + id(i,l)*id(j,k) ) - 1.0/3.0 * id(i,j)*id(k,l);

    itensor4 st;
    spatialTangent(st);

    CM.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                    for (unsigned m=0; m<3; m++)
                        CM(i,j) += Pdev(i,j,k,l)*st(k,l,m,m);
}




void fThermoMechMassMP::contractWithSpatialTangent(const ivector& v1, const ivector& v2, itensor& T) const
{
    itensor4 c;
    spatialTangent(c);

    T.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                    T(i,k) += c(i,j,k,l)*v1(j)*v2(l);
}




void fThermoMechMassMP::contractTangent(const ivector& na, const ivector& nb, double& tg) const
{

}




void fThermoMechMassMP::convectedTangentTimesSymmetricTensor(const istensor &M, istensor &CM) const
{
    itensor4 C;
    convectedTangent(C);

    CM.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    CM(i,j) += C(i,j,k,l)*M(k,l);
                }
}




void fThermoMechMassMP::energyMomentumTensor(itensor &EM) const
{

}




void fThermoMechMassMP::firstPiolaKirchhoffStress(itensor &P) const
{
    istensor S;
    secondPiolaKirchhoffStress(S);
    P = F_c*S;
}




double fThermoMechMassMP::freeEntropy() const
{
    double tref = theThermoMechMassMaterial.referenceTemperature();
    return -freeEnergy()/tref;
}



// Legendre transform of the free energy wrt the concentration
// gives potential that is function of F, theta, mu
double fThermoMechMassMP::grandCanonicalPotential() const
{
    return freeEnergy() - c_c * mu_c;
}




double fThermoMechMassMP::heatCapacityPerUnitVolume() const
{
    return theThermoMechMassMaterial._heatCapacity;
}




double fThermoMechMassMP::internalEnergy() const
{
    return grandCanonicalPotential() + temp_c*entropy() + c_c*mu_c;
}




void fThermoMechMassMP::KirchhoffStress(istensor& tau) const
{
    istensor S;
    secondPiolaKirchhoffStress(S);
    tau = istensor::FSFt(F_c, S);
}




void fThermoMechMassMP::KirchhoffStressVector(double tauv[6]) const
{
    istensor tau;
    KirchhoffStress(tau);
    muesli::tensorToVector(tau, tauv);
}




void fThermoMechMassMP::materialTangent(itensor4& cm) const
{
    istensor S;
    secondPiolaKirchhoffStress(S);

    itensor4 cc;
    convectedTangent(cc);

    cm.setZero();
    for (unsigned a=0; a<3; a++)
        for (unsigned b=0; b<3; b++)
            for (unsigned A=0; A<3; A++)
                for (unsigned B=0; B<3; B++)
                {
                    if (a == b) cm(a,A,b,B) += S(A,B);

                    for (unsigned C=0; C<3; C++)
                        for (unsigned D=0; D<3; D++)
                            cm(a,A,b,B) += F_c(a,C) * F_c(b,D) * cc(C,A,D,B);
                }
}




const fThermoMechMassMaterial& fThermoMechMassMP::parentMaterial() const
{
    return theThermoMechMassMaterial;
}




void fThermoMechMassMP::resetCurrentState()
{
    time_c   = time_n;
    temp_c   = temp_n;
    J_c      = J_n;
    mu_c     = mu_n;
    c_c      = c_n;
    gradMu_c = gradMu_n;
    F_c      = F_n;
}




void fThermoMechMassMP::secondPiolaKirchhoffStressVector(double Sv[6]) const
{
    istensor S;
    secondPiolaKirchhoffStress(S);
    muesli::tensorToVector(S, Sv);
}




void fThermoMechMassMP::spatialTangent(itensor4& Cs) const
{
    const itensor& Fc = F_c;
    itensor4 Cc;
    convectedTangent(Cc);

    Cs.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                    for (unsigned m=0; m<3; m++)
                        for (unsigned n=0; n<3; n++)
                            for (unsigned p=0; p<3; p++)
                                for (unsigned q=0; q<3; q++)
                                    Cs(i,j,k,l) += Fc(i,m)*Fc(j,n)*Fc(k,p)*Fc(l,q)*Cc(m,n,p,q);

    Cs *= 1.0/J_c;
}




// coupling tensor M = 2.0 * theta * d^2(psi)/( dC dTheta ) = theta * d[S]/d[Theta]
istensor fThermoMechMassMP::symmetricMaterialStressTemperatureTensor() const
{
    itensor  Finv = F_c.inverse();
    istensor Cinv = istensor::tensorTimesTensorTransposed(Finv);

    const double alpha = theThermoMechMassMaterial._thermalExpansion;
    const double bulk  = theThermoMechMassMaterial.getProperty(muesli::PR_BULK);

    std::cout << "\n Needs review";

    return -3.0/2.0 * alpha * bulk * temp_c * Cinv;
}




double fThermoMechMassMP::volumetricStiffness() const
{
    itensor4 tg;
    spatialTangent(tg);

    double vs = 0.0;
    for (unsigned i=0; i<3; i++)
    {
        for (unsigned j=0; j<3; j++)
        {
            vs += tg(i,i,j,j);
        }
    }
    return vs/9.0;
}




double fThermoMechMassMP::waveVelocity() const
{
    return theThermoMechMassMaterial.waveVelocity();
}




AnandIJSS2011Material::AnandIJSS2011Material(const std::string& name,
                                             const materialProperties& cl)
:
fThermoMechMassMaterial(name, cl),
theFSMaterial(nullptr),
_thermalConductivity(0.0),
_massMobility(0.0),
_referenceConcentration(0.0),
_NM(0.0), _bulkModulus(0.0)
{
    theFSMaterial = new neohookeanMaterial(name, cl);
    
    muesli::assignValue(cl, "thermal_conductivity", _thermalConductivity);
    muesli::assignValue(cl, "themal_expansion", _thermalExpansion);
    muesli::assignValue(cl, "heat_capacity", _heatCapacity);
    
    muesli::assignValue(cl, "mobility", _massMobility);
    muesli::assignValue(cl, "mass_expansion", _massExpansion);
    
    muesli::assignValue(cl, "muref", _mu0);
    
    muesli::assignValue(cl, "nm", _NM);
    if (theFSMaterial != nullptr) _bulkModulus = theFSMaterial->getProperty(PR_BULK);
    
    _referenceConcentration = fconcentration(1.0, referenceTemperature(), 0.0);
}




bool AnandIJSS2011Material::check() const
{
    return true;
}




fThermoMechMassMP* AnandIJSS2011Material::createMaterialPoint() const
{
    return new AnandIJSS2011MP(*this);
}




// computes the value of the concentration for a given state, not necessarily
// the current one of the point
double AnandIJSS2011Material::fconcentration(double J, double temp, double mu) const
{
    const double beta  = _massExpansion;
    const double R     = _R;
    const double NM    = _NM;
    const double kappa = _bulkModulus;
    const double logJ  = log(J);
    const double muref = referenceChemicalPotential();
    return NM*exp((mu - muref + 3.0*kappa*beta*logJ)/(R*temp));
}




double AnandIJSS2011Material::getProperty(const propertyName p) const
{
    double ret= theFSMaterial->getProperty(p);
    return ret;
}




void AnandIJSS2011Material::print(std::ostream &of) const
{
    fThermoMechMassMaterial::print(of);
    of  << "\n    Thermal (isotropic) conductivity : " << _thermalConductivity
    << "\n    Mass mobility                    : " << _massMobility
    << "\n    Moles of host material/unit vol  : " << _NM
    << "\n    Reference concentration          : " << _referenceConcentration
    << "\n    Stored bulk modulus              : " << _bulkModulus
    << "\n";
    theFSMaterial->print(of);
}




void AnandIJSS2011Material::setRandom()
{
    fThermoMechMassMaterial::setRandom();
    
    int mattype = discreteUniform(0, 0);
    std::string name = "surrogate finite strain material";
    materialProperties mp;
    
    if (mattype == 0)
        theFSMaterial = new neohookeanMaterial(name, mp);
    
    else if (mattype == 1)
        theFSMaterial = new fplasticMaterial(name, mp);
    
    else if (mattype == 2)
        theFSMaterial = new svkMaterial(name, mp);
    
    else if (mattype == 3)
        theFSMaterial = new arrudaboyceMaterial(name, mp);
    
    else if (mattype == 4)
        theFSMaterial = new mooneyMaterial(name, mp);
    
    else if (mattype == 5)
        theFSMaterial = new yeohMaterial(name, mp);
    
    else if (mattype == 6)
        theFSMaterial = new johnsonCookMaterial(name, mp);
    
    else if (mattype == 7)
        theFSMaterial = new zerilliArmstrongMaterial(name, mp);
    
    theFSMaterial->setRandom();
    
    _thermalConductivity = muesli::randomUniform(1.0e3, 1e5);
    _massMobility        = muesli::randomUniform(1e-3, 1e-2);
    _referenceConcentration = muesli::randomUniform(1e-3, 1e-2);
    _NM                  = muesli::randomUniform(2.0, 8.0)*1e5;
    _bulkModulus         = theFSMaterial->getProperty(PR_BULK);
    _mu0 = -_R*referenceTemperature()*log(_referenceConcentration/_NM);
}




bool AnandIJSS2011Material::test(std::ostream &of)
{
    bool isok = true;
    setRandom();
    muesli::fThermoMechMassMP* p = this->createMaterialPoint();
    
    isok = p->testImplementation(of);
    return isok;
}




double AnandIJSS2011Material::waveVelocity() const
{
    return theFSMaterial->waveVelocity();
}




AnandIJSS2011MP::AnandIJSS2011MP(const AnandIJSS2011Material& m):
fThermoMechMassMP(m),
theAnandMaterial(m)
{
    theFSMP = theAnandMaterial.theFSMaterial->createMaterialPoint();
    c_n = c_c = theAnandMaterial._referenceConcentration;
}




// d^2 [GCP] / d mu^2 = - d [ concentration] / d [ chemical potential ]
double AnandIJSS2011MP::chemicalTangent() const
{
    const double R = theAnandMaterial._R;

    return -concentration()/(R*temp_c);
}




void AnandIJSS2011MP::commitCurrentState()
{
    fThermoMechMassMP::commitCurrentState();
    theFSMP->commitCurrentState();
}




void AnandIJSS2011MP::convectedTangent(itensor4& ctg) const
{
    const double alpha = theAnandMaterial._thermalExpansion;
    const double beta  = theAnandMaterial._massExpansion;
    const double cref  = theAnandMaterial._referenceConcentration;
    const double kappa = theAnandMaterial._bulkModulus;
    const double tref  = theAnandMaterial.referenceTemperature();
    const double R     = theAnandMaterial._R;

    theFSMP->convectedTangent(ctg);

    istensor C = istensor::tensorTransposedTimesTensor(F_c);
    istensor Cinv = C.inverse();

    const double dtheta= temp_c - tref;
    const double f     = 6.0 * alpha * kappa * dtheta;

    const double dcon  = c_c - cref;
    const double g     = 6.0 * beta * kappa * dcon;
    const double gg    = 9.0 * beta * beta * kappa * kappa * c_c / R /temp_c;

    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    ctg(i,j,k,l) += (f+g) * 0.5 * ( Cinv(i,k)* Cinv(l,j) + Cinv(i,l) * Cinv(k,j) )
                                 - gg * Cinv(i,j) * Cinv(k,l);
                }
}



// if variational, return omega-bar, otherwise, omega
double AnandIJSS2011MP::diffusionPotential() const
{
    const double m = theAnandMaterial._massMobility;

    ivector D;
    if (theAnandMaterial.isVariational())
    {
        D = -temp_n/temp_c * gradMu_c;
    }
    else
    {
        D = -gradMu_c;
    }

    return -0.5*m*D.squaredNorm();
}




double AnandIJSS2011MP::dissipation() const
{
    return theFSMP->energyDissipationInStep();
}




double AnandIJSS2011MP::effectiveFreeEnergy() const
{
    return freeEnergy();
}




double AnandIJSS2011MP::entropy() const
{
    const double alpha = theAnandMaterial._thermalExpansion;
    const double cap   = theAnandMaterial._heatCapacity;
    const double R     = theAnandMaterial._R;
    const double NM    = theAnandMaterial._NM;
    const double kappa = theAnandMaterial._bulkModulus;
    const double tref  = theAnandMaterial.referenceTemperature();

    double s = cap*log(temp_c/tref) + 3.0*kappa*alpha*log(J_c) - R*c_c*(log(c_c/NM)-1.0);

    return s;
}



// free energy as a function of F, theta, c
double AnandIJSS2011MP::freeEnergy() const
{
    const double alpha = theAnandMaterial._thermalExpansion;
    const double beta  = theAnandMaterial._massExpansion;
    const double cap   = theAnandMaterial._heatCapacity;
    const double cref  = theAnandMaterial._referenceConcentration;
    const double R     = theAnandMaterial._R;
    const double muref = theAnandMaterial._mu0;
    const double NM    = theAnandMaterial._NM;
    const double kappa = theAnandMaterial._bulkModulus;
    const double tref  = theAnandMaterial.referenceTemperature();
    const double dTemp = temp_c - tref;
    const double dCon  = c_c-cref;

    double logJ = log(J_c);

    double psi = theFSMP->effectiveStoredEnergy()
        - 3.0*kappa*alpha*dTemp*logJ
        - 3.0*kappa*beta*dCon*logJ
        + cap*dTemp - cap*temp_c*log(temp_c/tref)
        + muref*c_c + R*temp_c*c_c*(log(c_c/NM)-1.0);

    return psi;
}




materialState AnandIJSS2011MP::getConvergedState() const
{
    materialState state;

    state.theTime = time_n;
    state.theDouble.push_back(temp_n);
    state.theDouble.push_back(J_n);
    state.theDouble.push_back(mu_n);
    state.theDouble.push_back(c_n);
    state.theVector.push_back(gradMu_n);
    state.theTensor.push_back(F_n);

    return state;
}




materialState AnandIJSS2011MP::getCurrentState() const
{
    materialState state;

    state.theTime = time_c;
    state.theDouble.push_back(temp_c);
    state.theDouble.push_back(J_c);
    state.theDouble.push_back(mu_c);
    state.theDouble.push_back(c_c);
    state.theVector.push_back(gradMu_c);
    state.theTensor.push_back(F_c);

    return state;
}




double AnandIJSS2011MP::kineticPotential() const
{
    return diffusionPotential() + thermalPotential();
}




istensor AnandIJSS2011MP::materialConductivity() const
{
    const double kappa = theAnandMaterial._thermalConductivity;

    istensor K;
    if (theAnandMaterial.isVariational())
    {
        K = istensor::scaledIdentity(kappa*temp_n);
    }
    else
    {
        K = istensor::scaledIdentity(kappa);
    }
    return K;
}




// -d[J]/d[D]
istensor AnandIJSS2011MP::materialMobility() const
{
    const double m = theAnandMaterial._massMobility;

    istensor K;
    if (theAnandMaterial.isVariational())
    {
        K = istensor::scaledIdentity(m*temp_n/temp_c);
    }
    else
    {
        K = istensor::scaledIdentity(m);
    }
    return K;
}




ivector AnandIJSS2011MP::materialHeatFlux() const
{
    ivector H;
    if (theAnandMaterial.isVariational())
    {
        ivector G = gradT_c/(-temp_c);
        H = theAnandMaterial._thermalConductivity*temp_n * G;
    }
    else
    {
        ivector G = -gradT_c;
        H = theAnandMaterial._thermalConductivity * G;
    }
    return H;
}




// for the variational formulation the materialMassFlus is not
// the derivative of the diffusion potential
ivector AnandIJSS2011MP::materialMassFlux() const
{
    const double m = theAnandMaterial._massMobility;

    ivector J;
    if (theAnandMaterial.isVariational())
    {
        J = -m * temp_n /temp_c * gradMu_c;
    }
    else
    {
        J = -m*gradMu_c;
    }
    return J;
}




ivector AnandIJSS2011MP::materialMassFluxDTheta() const
{
    const double m = theAnandMaterial._massMobility;

    ivector Jprime;
    if (theAnandMaterial.isVariational())
    {
        Jprime = 2.0 * m * temp_n * temp_n /(temp_c * temp_c * temp_c) * gradMu_c;
    }
    else
    {
        Jprime.setZero();
    }
    return Jprime;
}




// coupling tensor M = d_P/d_mu
itensor AnandIJSS2011MP::materialStressChemicalTensor() const
{
    itensor Fcinv = F_c.inverse();

    const double beta  = theAnandMaterial._massExpansion;
    const double kappa = theAnandMaterial._bulkModulus;
    const double R     = theAnandMaterial._R;

    return -c_c * 3.0 * beta * kappa /(R*temp_c) * Fcinv.transpose();
}




// coupling tensor M = d^2(GCP)/(d_F d_Theta) = d_P/d_Theta
itensor AnandIJSS2011MP::materialStressTemperatureTensor() const
{
    itensor Fcinv = F_c.inverse();

    const double alpha = theAnandMaterial._thermalExpansion;
    const double kappa = theAnandMaterial._bulkModulus;
    const double beta  = theAnandMaterial._massExpansion;
    const double NM    = theAnandMaterial._NM;

    return (log(c_c/NM)*3.0*beta*kappa*c_c/temp_c-3.0*alpha*kappa)*Fcinv.transpose();
}




double AnandIJSS2011MP::plasticSlip() const
{
    return theFSMP->plasticSlip();
}




void AnandIJSS2011MP::resetCurrentState()
{
    fThermoMechMassMP::resetCurrentState();
    theFSMP->resetCurrentState();
}




// 4 d^2 GCP / dC^2
void AnandIJSS2011MP::secondPiolaKirchhoffStress(istensor& S) const
{
    theFSMP->secondPiolaKirchhoffStress(S);

    itensor  Finv = F_c.inverse();
    istensor Cinv = istensor::tensorTimesTensorTransposed(Finv);

    const double alpha = theAnandMaterial._thermalExpansion;
    const double kappa = theAnandMaterial._bulkModulus;
    const double tref  = theAnandMaterial.referenceTemperature();
    const double dTemp = temp_c - tref;

    const double beta  = theAnandMaterial._massExpansion;
    const double cref  = theAnandMaterial._referenceConcentration;
    const double dCon  = c_c - cref;

    S -= 3.0 * kappa * (alpha * dTemp + beta * dCon) * Cinv;
}



// - d chi / d theta = d^ GCP / (dtheta dmu)
double AnandIJSS2011MP::temperatureChemicalCoupling() const
{
    const double NM = theAnandMaterial._NM;
    return c_c/temp_c * log(c_c/NM);
}




double AnandIJSS2011MP::thermalPotential() const
{
    ivector G = gradT_c/(-temp_c);
    const double k  = theAnandMaterial._thermalConductivity*temp_n;
    return -0.5 * k * G.squaredNorm();
}




// d^2 GCP / d(theta)^2
double AnandIJSS2011MP::temperatureTangent() const
{
    const double R  = theAnandMaterial._R;
    const double NM = theAnandMaterial._NM;

    return -heatCapacityPerUnitVolume()/temp_c
        - temperatureChemicalCoupling() * R * log(c_c/NM);
}




void AnandIJSS2011MP::updateCurrentState(const double theTime, const itensor& F,
                                         const double temp, const ivector& gradT,
                                         const double mu, const ivector& gradMu)
{
    time_c   = theTime;
    F_c      = F;
    J_c      = F.determinant();
    temp_c   = temp;
    gradT_c  = gradT;
    mu_c     = mu;
    gradMu_c = gradMu;
    theFSMP->updateCurrentState(theTime, F);

    c_c = theAnandMaterial.fconcentration(J_c, temp_c, mu_c);
    if (std::isnan(c_c))
        c_c = theAnandMaterial.fconcentration(J_c, temp_c, mu_c);
}




double AnandIJSS2011MP::volumeFraction() const
{
    return c_c/theAnandMaterial._NM;
}




bool fThermoMechMassMP::testImplementation(std::ostream& of, const bool testDE, const bool testDDE) const
{
    bool isok = true;
    fThermoMechMassMP& theMP = const_cast<fThermoMechMassMP&>(*this);

    // set a random state in the material
    double t = randomUniform(0.0, 1.0);
    itensor F; F.setRandom(); F *= 2e-1;
    F += itensor::identity();
    if (F.determinant() < 0.0) F *= -1.0;

    double temp = theMP.parentMaterial().referenceTemperature()*randomUniform(0.5, 0.8);
    ivector gradT; gradT.setRandom(); gradT *= 1000.0;

    double mu = randomUniform(-5000.0, -2000.0);
    ivector gradMu; gradMu.setRandom(); gradMu *= 1000.0;

    std::cout << "\n angelortiz_TRAZA_c_c_ANTES_1 " << c_c;//AQUI QUITAR
    theMP.updateCurrentState(t, F, temp, gradT, mu, gradMu);
    theMP.commitCurrentState();
    std::cout << "\n angelortiz_TRAZA_c_c_DESPUES_1 " << c_c;//AQUI QUITAR

    t = muesli::randomUniform(0.1,1.0);
    F.setRandom(); F *= 2e-1;
    F += itensor::identity();
    if (F.determinant() < 0.0) F *= -1.0;

    temp = theMP.parentMaterial().referenceTemperature()*randomUniform(2.0, 3.0);
    gradT.setRandom();  gradT *= 100.0;

    mu = randomUniform(-5000.0, -4000.0);
    gradMu.setRandom(); gradMu *= 100.0;

    std::cout << "\n angelortiz_TRAZA_c_c_ANTES_2 " << c_c;//AQUI QUITAR
    theMP.updateCurrentState(t, F, temp, gradT, mu, gradMu);
    std::cout << "\n angelortiz_TRAZA_c_c_DESPUES_2 " << c_c;//AQUI QUITAR

    if (testDE)
    {
        {
            itensor pr_P; firstPiolaKirchhoffStress(pr_P);

            class gcpDF : public muesli::NumDiff
            {
            public:
                gcpDF(unsigned ii, unsigned jj, fThermoMechMassMP& xp, double xt, itensor& xF, double xtemp, ivector& xgradT, double xmu, ivector xgradMu)
                : i(ii), j(jj), point(xp), _t(xt), _temp(xtemp), _mu(xmu), _gradT(xgradT), _gradMu(xgradMu), _F(xF) {}

                virtual double eval(){return point.grandCanonicalPotential();}
                virtual void update(double dx)
                {
                    itensor F = _F;
                    F(i,j) += dx;
                    point.updateCurrentState(_t, F, _temp, _gradT, _mu, _gradMu);
                }

            private:
                unsigned i, j;
                fThermoMechMassMP& point;
                double _t, _temp, _mu;
                ivector _gradT, _gradMu;
                itensor _F;
            };

            itensor numP;
            for (unsigned i=0; i<3; i++)
            {
                for (unsigned j=0; j<3; j++)
                {
                    gcpDF dd(i, j, theMP, t, F, temp, gradT, mu, gradMu);
                    numP(i,j) = dd();
                }
            }

            itensor errorP = numP - pr_P;
            
            std::cout << "\n angelortiz_TRAZA_TEST1_pr_P " << pr_P;//AQUI QUITAR
            std::cout << "\n angelortiz_TRAZA_TEST1_numP " << numP;//AQUI QUITAR
            
            isok = (errorP.norm()/pr_P.norm() < 1e-4);
            of << "\n   1. Comparing P with derivative [d GCP / d F].";
            if (isok)
            {
                of << " TEST PASSED.";
            }
            else
            {
                of << "\n *****TEST FAILED*****. Relative error in 1st PK computation: " << errorP.norm()/pr_P.norm();
                of << "\n " << pr_P;
                of << "\n " << numP;
            }
        }

        {
            class gcpDTheta : public muesli::NumDiff
            {
            public:
                gcpDTheta(fThermoMechMassMP& xp, double xt, itensor& xF, double xtemp, ivector& xgradT, double xmu, ivector xgradMu)
                : point(xp), _t(xt), _temp(xtemp), _mu(xmu), _gradT(xgradT), _gradMu(xgradMu), _F(xF) {}

                virtual double eval(){return point.grandCanonicalPotential();}
                virtual void update(double dx) {point.updateCurrentState(_t, _F, _temp+dx, _gradT, _mu, _gradMu);}

            private:
                fThermoMechMassMP& point;
                double _t, _temp, _mu;
                ivector _gradT, _gradMu;
                itensor _F;
            };

            gcpDTheta dd(theMP, t, F, temp, gradT, mu, gradMu);
            double numEntropy = -dd();
            double error = numEntropy - entropy();

            std::cout << "\n angelortiz_TRAZA_TEST2_entropy " << entropy();//AQUI QUITAR
            std::cout << "\n angelortiz_TRAZA_TEST2_numEntropy " << numEntropy;//AQUI QUITAR
            
            isok = fabs(error)/fabs(entropy()) < 1e-4;
            of << "\n   2. Comparing entropy with derivative [- d GCP / d theta].";
            if (isok)
            {
                of << " TEST PASSED.";
            }
            else
            {
                of << "\n *****TEST FAILED*****. Relative error in entropy computation: " << error;
                of << "\n " << entropy();
                of << "\n " << numEntropy;
            }
        }

        {
            class gcpDmu : public muesli::NumDiff
            {
            public:
                gcpDmu(fThermoMechMassMP& xp, double xt, itensor& xF, double xtemp, ivector& xgradT, double xmu, ivector xgradMu)
                :point(xp), _t(xt), _temp(xtemp), _mu(xmu), _gradT(xgradT), _gradMu(xgradMu), _F(xF){}
                virtual double eval(){return point.grandCanonicalPotential();}
                virtual void update(double dx){point.updateCurrentState(_t, _F, _temp, _gradT, _mu+dx, _gradMu);}

            private:
                fThermoMechMassMP& point;
                const double _t, _temp, _mu;
                const ivector _gradT, _gradMu;
                const itensor _F;
            };

            gcpDmu dd(theMP, t, F, temp, gradT, mu, gradMu);
            double numConcentration = -dd();
            double error = numConcentration - c_c;

            std::cout << "\n angelortiz_TRAZA_TEST3_c_c " << c_c;//AQUI QUITAR
            std::cout << "\n angelortiz_TRAZA_TEST3_numConcentration " << numConcentration;//AQUI QUITAR
            
            isok = fabs(error)/c_c < 1e-4;
            of << "\n   3. Comparing concentration with derivative [- d GCP / d mu].";
            if (isok)
            {
                of << " TEST PASSED.";
            }
            else
            {
                of << "\n *****TEST FAILED*****. Relative error in concentration computation: " << error;
                of << "\n " << c_c;
                of << "\n " << numConcentration;
            }
        }

        {
            class cDmu : public muesli::NumDiff
            {
            public:
                cDmu(fThermoMechMassMP& xp, double xt, itensor& xF, double xtemp, ivector& xgradT, double xmu, ivector xgradMu)
                : point(xp), _t(xt), _temp(xtemp), _mu(xmu), _gradT(xgradT), _gradMu(xgradMu), _F(xF){}
                virtual double eval(){return point.concentration();}
                virtual void update(double dx){point.updateCurrentState(_t, _F, _temp, _gradT, _mu+dx, _gradMu);}

            private:
                fThermoMechMassMP& point;
                const double _t, _temp, _mu;
                const ivector _gradT, _gradMu;
                const itensor _F;
            };

            cDmu dd(theMP, t, F, temp, gradT, mu, gradMu);
            double numChemicalTangent = -dd();
            double error = numChemicalTangent - chemicalTangent();

            std::cout << "\n angelortiz_TRAZA_TEST4_chemicalTangent " << chemicalTangent();//AQUI QUITAR
            std::cout << "\n angelortiz_TRAZA_TEST4_numChemicalTangent " << numChemicalTangent;//AQUI QUITAR
            
            isok = fabs(error)/chemicalTangent() < 1e-4;
            of << "\n   4. Comparing chemical tangent with [- d c / d mu].";
            if (isok)
            {
                of << " TEST PASSED.";
            }
            else
            {
                of << "\n *****TEST FAILED*****. Relative error in chemical tangent: " << error;
                of << "\n " << chemicalTangent();
                of << "\n " << numChemicalTangent;
            }
        }

        {
            class conDgradT : public muesli::NumDiff
            {
            public:
                conDgradT(unsigned xi, fThermoMechMassMP& xp, double xt, itensor& xF, double xtemp, ivector& xgradT, double xmu, ivector xgradMu)
                : i(xi), point(xp), _t(xt), _temp(xtemp), _mu(xmu), _gradT(xgradT), _gradMu(xgradMu), _F(xF){}
                virtual double eval(){return point.thermalPotential();}
                virtual void update(double dx){ivector g=_gradT; g(i)+=dx; point.updateCurrentState(_t, _F, _temp, g, _mu, _gradMu);}

            private:
                const unsigned i;
                fThermoMechMassMP& point;
                const double _t, _temp, _mu;
                const ivector _gradT, _gradMu;
                const itensor _F;
            };

            ivector numHeat;
            for (unsigned i=0; i<3; i++)
            {
                conDgradT dd(i, theMP, t, F, temp, gradT, mu, gradMu);
                numHeat(i) = -dd();
            }

            numHeat *= -temp_c;
            ivector heat = materialHeatFlux();
            double error = (numHeat - heat).norm();

            std::cout << "\n angelortiz_TRAZA_TEST5_heat " << heat;//AQUI QUITAR
            std::cout << "\n angelortiz_TRAZA_TEST5_numHeat " << numHeat;//AQUI QUITAR
            
            isok = error/heat.norm() < 1e-4;
            of << "\n   5. Comparing heat flux with temp*[d Potential / d gradT].";
            if (isok)
            {
                of << " TEST PASSED.";
            }
            else
            {
                of << "\n *****TEST FAILED*****. Relative error in heat flux: " << error;
                of << "\n " << heat;
                of << "\n " << numHeat;
            }
        }

        {
            class heatDgradT : public muesli::NumDiff
            {
            public:
                heatDgradT(unsigned xi, unsigned xj, fThermoMechMassMP& xp, double xt, itensor& xF, double xtemp, ivector& xgradT, double xmu, ivector xgradMu)
                : i(xi), j(xj), point(xp), _t(xt), _temp(xtemp), _mu(xmu), _gradT(xgradT), _gradMu(xgradMu), _F(xF){}
                virtual double eval(){return point.materialHeatFlux()(i);}
                virtual void update(double dx){ivector g=_gradT; g(j)+=dx; point.updateCurrentState(_t, _F, _temp, g, _mu, _gradMu);}

            private:
                const unsigned i, j;
                fThermoMechMassMP& point;
                const double _t, _temp, _mu;
                const ivector _gradT, _gradMu;
                const itensor _F;
            };

            istensor numK;
            for (unsigned i=0; i<3; i++)
            {
                for (unsigned j=0; j<3; j++)
                {
                    heatDgradT dd(i, j, theMP, t, F, temp, gradT, mu, gradMu);
                    numK(i,j) = dd();
                }
            }
            numK *= -temp_c;

            istensor K = materialConductivity();
            double error = (numK - K).norm();

            std::cout << "\n angelortiz_TRAZA_TEST6_materialConductivity " << K;//AQUI QUITAR
            std::cout << "\n angelortiz_TRAZA_TEST6_numMaterialConductivity " << numK;//AQUI QUITAR
            
            isok = error/K.norm() < 1e-4;
            of << "\n   6. Comparing conductivity with (-temp) [d heat / d gradT].";
            if (isok)
            {
                of << " TEST PASSED.";
            }
            else
            {
                of << "\n *****TEST FAILED*****. Relative error in materialConductivity: " << error;
                of << "\n " << K;
                of << "\n " << numK;
            }
        }

        {
            class conDgradT : public muesli::NumDiff
            {
            public:
                conDgradT(unsigned xi, fThermoMechMassMP& xp, double xt, itensor& xF, double xtemp, ivector& xgradT, double xmu, ivector xgradMu)
                : i(xi), point(xp), _t(xt), _temp(xtemp), _mu(xmu), _gradT(xgradT), _gradMu(xgradMu), _F(xF){}
                virtual double eval(){return point.diffusionPotential();}
                virtual void update(double dx){ivector g=_gradMu; g(i)-=dx; point.updateCurrentState(_t, _F, _temp, _gradT, _mu, g);}

            private:
                const unsigned i;
                fThermoMechMassMP& point;
                const double _t, _temp, _mu;
                const ivector _gradT, _gradMu;
                const itensor _F;
            };

            ivector numFlux;
            for (unsigned i=0; i<3; i++)
            {
                conDgradT dd(i, theMP, t, F, temp, gradT, mu, gradMu);
                numFlux(i) = -dd();
            }

            ivector flux = materialMassFlux();
            double error = (numFlux - flux).norm();

            std::cout << "\n angelortiz_TRAZA_TEST7_flux " << flux;//AQUI QUITAR
            std::cout << "\n angelortiz_TRAZA_TEST7_numFlux " << numFlux;//AQUI QUITAR
            
            isok = error/flux.norm() < 1e-4;
            of << "\n   7. Comparing mass flux with  -[d Potential / d (-gradMu)].";
            if (isok)
            {
                of << " TEST PASSED.";
            }
            else
            {
                of << "\n *****TEST FAILED*****. Relative error in mass flux: " << error;
                of << "\n " << flux;
                of << "\n " << numFlux;
            }
        }

        {
            class heatDgradT : public muesli::NumDiff
            {
            public:
                heatDgradT(unsigned xi, unsigned xj, fThermoMechMassMP& xp, double xt, itensor& xF, double xtemp, ivector& xgradT, double xmu, ivector xgradMu)
                : i(xi), j(xj), point(xp), _t(xt), _temp(xtemp), _mu(xmu), _gradT(xgradT), _gradMu(xgradMu), _F(xF){}
                virtual double eval(){return point.materialMassFlux()(i);}
                virtual void update(double dx){ivector g=_gradMu; g(j)+=dx; point.updateCurrentState(_t, _F, _temp, _gradT, _mu, g);}
                virtual void reset(){point.updateCurrentState(_t, _F, _temp, _gradT, _mu, _gradMu);}

            private:
                const unsigned i, j;
                fThermoMechMassMP& point;
                const double _t, _temp, _mu;
                const ivector _gradT, _gradMu;
                const itensor _F;
            };

            istensor numK;
            for (unsigned i=0; i<3; i++)
            {
                for (unsigned j=0; j<3; j++)
                {
                    heatDgradT dd(i, j, theMP, t, F, temp, gradT, mu, gradMu);
                    numK(i,j) = -dd();
                }
            }

            istensor K = materialMobility();
            double error = (numK - K).norm();

            std::cout << "\n angelortiz_TRAZA_TEST8_materialMobility " << K;//AQUI QUITAR
            std::cout << "\n angelortiz_TRAZA_TEST8_numMaterialMobility " << numK;//AQUI QUITAR
            
            isok = error/K.norm() < 1e-4;
            of << "\n   8. Comparing diffusivity with  -[d flux / d gradMu].";
            if (isok)
            {
                of << " TEST PASSED.";
            }
            else
            {
                of << "\n *****TEST FAILED*****. Relative error in diffusivity: " << error;
                of << "\n " << K;
                of << "\n " << numK;
            }
        }

        {
            class stressDmu : public muesli::NumDiff
            {
            public:
                stressDmu(unsigned xi, unsigned xj, fThermoMechMassMP& xp, double xt, itensor& xF, double xtemp, ivector& xgradT, double xmu, ivector xgradMu)
                : i(xi), j(xj), point(xp), _t(xt), _temp(xtemp), _mu(xmu), _gradT(xgradT), _gradMu(xgradMu), _F(xF){}
                virtual double eval(){itensor P; point.firstPiolaKirchhoffStress(P); return P(i,j);}
                virtual void update(double dx){point.updateCurrentState(_t, _F, _temp, _gradT, _mu+dx, _gradMu);}

            private:
                const unsigned i, j;
                fThermoMechMassMP& point;
                const double _t, _temp, _mu;
                const ivector _gradT, _gradMu;
                const itensor _F;
            };

            itensor numK;
            for (unsigned i=0; i<3; i++)
            {
                for (unsigned j=0; j<3; j++)
                {
                    stressDmu dd(i, j, theMP, t, F, temp, gradT, mu, gradMu);
                    numK(i,j) = dd();
                }
            }

            itensor K = materialStressChemicalTensor();
            double error = (numK - K).norm();

            std::cout << "\n angelortiz_TRAZA_TEST9_materialStressChemicalTensor " << K;//AQUI QUITAR
            std::cout << "\n angelortiz_TRAZA_TEST9_numMaterialStressChemicalTensor " << numK;//AQUI QUITAR
            
            isok = error/K.norm() < 1e-4;
            of << "\n   9. Comparing stressChemicalTensor with  [d P / d mu].";
            if (isok)
            {
                of << " TEST PASSED.";
            }
            else
            {
                of << "\n *****TEST FAILED*****. Relative error in stressChemical tensor: " << error;
                of << "\n " << K;
                of << "\n " << numK;
            }
        }

        {
            class stressDtheta : public muesli::NumDiff
            {
            public:
                stressDtheta(unsigned xi, unsigned xj, fThermoMechMassMP& xp, double xt, itensor& xF, double xtemp, ivector& xgradT, double xmu, ivector xgradMu)
                : i(xi), j(xj), point(xp), _t(xt), _temp(xtemp), _mu(xmu), _gradT(xgradT), _gradMu(xgradMu), _F(xF){}
                virtual double eval(){itensor P; point.firstPiolaKirchhoffStress(P); return P(i,j);}
                virtual void update(double dx){point.updateCurrentState(_t, _F, _temp+dx, _gradT, _mu, _gradMu);}

            private:
                const unsigned i, j;
                fThermoMechMassMP& point;
                const double _t, _temp, _mu;
                const ivector _gradT, _gradMu;
                const itensor _F;
            };

            itensor numK;
            for (unsigned i=0; i<3; i++)
            {
                for (unsigned j=0; j<3; j++)
                {
                    stressDtheta dd(i, j, theMP, t, F, temp, gradT, mu, gradMu);
                    numK(i,j) = dd();
                }
            }

            theMP.updateCurrentState(t, F, temp, gradT, mu, gradMu);
            itensor K = materialStressTemperatureTensor();
            double error = (numK - K).norm();

            
            std::cout << "\n angelortiz_TRAZA_TEST10_materialStressTemperatureTensor " << K;//AQUI QUITAR
            std::cout << "\n angelortiz_TRAZA_TEST10_numMaterialStressTemperatureTensor " << numK;//AQUI QUITAR
            
            isok = error/K.norm() < 1e-4;
            of << "\n   10. Comparing stressTemperatureTensor with  [d P / d theta].";
            if (isok)
            {
                of << " TEST PASSED.";
            }
            else
            {
                of << "\n *****TEST FAILED*****. Relative error in stress-temp tensor: " << error;
                of << "\n " << K;
                of << "\n " << numK;
            }
        }

        {
            class minuscDtheta : public muesli::NumDiff
            {
            public:
                minuscDtheta(fThermoMechMassMP& xp, double xt, itensor& xF, double xtemp, ivector& xgradT, double xmu, ivector xgradMu)
                : point(xp), _t(xt), _temp(xtemp), _mu(xmu), _gradT(xgradT), _gradMu(xgradMu), _F(xF){}
                virtual double eval(){return -point.concentration();}
                virtual void update(double dx){point.updateCurrentState(_t, _F, _temp+dx, _gradT, _mu, _gradMu);}

            private:
                fThermoMechMassMP& point;
                const double _t, _temp, _mu;
                const ivector _gradT, _gradMu;
                const itensor _F;
            };

            minuscDtheta dd(theMP, t, F, temp, gradT, mu, gradMu);
            double numK = dd();

            theMP.updateCurrentState(t, F, temp, gradT, mu, gradMu);
            double K = temperatureChemicalCoupling();
            double error = fabs(K-numK);

            std::cout << "\n angelortiz_TRAZA_TEST11_temperatureChemicalCoupling " << K;//AQUI QUITAR
            std::cout << "\n angelortiz_TRAZA_TEST11_numTemperatureChemicalCoupling " << numK;//AQUI QUITAR
            
            isok = error/fabs(K) < 1e-4;
            of << "\n   11. Comparing temperatureChemicalTensor with  -[d c / d theta].";
            if (isok)
            {
                of << " TEST PASSED.";
            }
            else
            {
                of << "\n *****TEST FAILED*****. Relative error in temp-chemical tensor: " << error;
                of << "\n " << K;
                of << "\n " << numK;
            }
        }

        {
            class minuscDF : public muesli::NumDiff
            {
            public:
                minuscDF(unsigned ii, unsigned jj, fThermoMechMassMP& xp, double xt, itensor& xF, double xtemp, ivector& xgradT, double xmu, ivector xgradMu)
                : i(ii), j(jj), point(xp), _t(xt), _temp(xtemp), _mu(xmu), _gradT(xgradT), _gradMu(xgradMu), _F(xF){}
                virtual double eval(){return -point.concentration();}
                virtual void update(double dx){itensor F(_F); F(i,j)+=dx; point.updateCurrentState(_t, F, _temp, _gradT, _mu, _gradMu);}

            private:
                unsigned i, j;
                fThermoMechMassMP& point;
                const double _t, _temp, _mu;
                const ivector _gradT, _gradMu;
                const itensor _F;
            };

            itensor numK;
            for (unsigned i=0; i<3; i++)
            {
                for (unsigned j=0; j<3; j++)
                {
                    minuscDF dd(i, j, theMP, t, F, temp, gradT, mu, gradMu);
                    numK(i,j) = dd();
                }
            }

            theMP.updateCurrentState(t, F, temp, gradT, mu, gradMu);
            itensor K = materialStressChemicalTensor();
            double error = (K-numK).norm();

            std::cout << "\n angelortiz_TRAZA_TEST12_materialStressChemicalTensor " << K;//AQUI QUITAR
            std::cout << "\n angelortiz_TRAZA_TEST12_numMaterialStressChemicalTensor " << numK;//AQUI QUITAR

            isok = error/K.norm() < 1e-4;
            of << "\n   12. Comparing stressChemicalTensor with  -[d c / d F].";
            if (isok)
            {
                of << " TEST PASSED.";
            }
            else
            {
                of << "\n *****TEST FAILED*****. Relative error in stress-chemical tensor: " << error;
                of << "\n " << K;
                of << "\n " << numK;
            }
        }
    }

    if (testDDE)
    {
        itensor4 A, numA;
        materialTangent(A);

        class stressDF : public muesli::NumDiff
        {
        public:
            stressDF(unsigned xi, unsigned xj, unsigned xk, unsigned xl,
                     fThermoMechMassMP& xp, double xt, itensor& xF, double xtemp, ivector& xgradT, double xmu, ivector xgradMu)
            : i(xi), j(xj), k(xk), l(xl), point(xp), _t(xt), _temp(xtemp), _mu(xmu), _gradT(xgradT), _gradMu(xgradMu), _F(xF){}
            virtual double eval(){itensor P; point.firstPiolaKirchhoffStress(P); return P(i,j);}
            virtual void update(double dx){itensor F(_F); F(k,l)+= dx; point.updateCurrentState(_t, F, _temp, _gradT, _mu, _gradMu);}

        private:
            const unsigned i, j, k, l;
            fThermoMechMassMP& point;
            const double _t, _temp, _mu;
            const ivector _gradT, _gradMu;
            const itensor _F;
        };

        itensor numK;
        for (unsigned i=0; i<3; i++)
        {
            for (unsigned j=0; j<3; j++)
            {
                for (unsigned k=0; k<3; k++)
                {
                    for (unsigned l=0; l<3; l++)
                    {
                        stressDF dd(i, j, k, l, theMP, t, F, temp, gradT, mu, gradMu);
                        numA(i,j,k,l) = dd();
                    }
                }
            }
        }

        double error = (A - numA).norm();

        std::cout << "\n angelortiz_TRAZA_TEST13_materialTangent " << A;//AQUI QUITAR
        std::cout << "\n angelortiz_TRAZA_TEST13_numMaterialTanget " << numA;//AQUI QUITAR

        isok = error/A.norm() < 1e-4;
        of << "\n   13. Comparing materialTangent with  [d P / d F].";
        if (isok)
        {
            of << " TEST PASSED.";
        }
        else
        {
            of << "\n *****TEST FAILED***** Relative error in materialTangent tensor: " << error;
            of << "\n " << A;
            of << "\n " << numA;
        }
    }

    return true;
}





AOT_ThermoMechMassMaterial::AOT_ThermoMechMassMaterial(const std::string& name,
                                       const materialProperties& cl)
:
fThermoMechMassMaterial(name, cl),
_massMobility(0.0),//NUEVA DEPENDE DE 1
_Omega(0.0),
_K(0.0),
_thermalCapacity(0.0),
_thermalConductivity(0.0),
_diff(0.0),
_nr(0.0),
_kb(0.0),
_ChiL(0.0),
_ChiH(0.0),
_Nabla(0.0)
{
    muesli::assignValue(cl, "chil", _ChiL);
    muesli::assignValue(cl, "chih", _ChiH);
    muesli::assignValue(cl, "conductivity", _thermalConductivity);
    muesli::assignValue(cl, "diffusivity", _diff);
    muesli::assignValue(cl, "k", _K);
    muesli::assignValue(cl, "kb", _kb);
    muesli::assignValue(cl, "nabla", _Nabla);
    muesli::assignValue(cl, "heat_capacity", _thermalCapacity);
    muesli::assignValue(cl, "mobility", _massMobility);//NUEVA DEPENDE DE 1
    muesli::assignValue(cl, "nr", _nr);
    muesli::assignValue(cl, "omega", _Omega);
}




fThermoMechMassMP* AOT_ThermoMechMassMaterial::createMaterialPoint() const
{
    muesli::fThermoMechMassMP* mp = new AOT_ThermoMechMassMP(*this);
    return mp;
}




bool AOT_ThermoMechMassMaterial::check() const
{
    bool isok = true;
    
    if (_diff <= 0.0) isok = false;
    if (_thermalCapacity <= 0.0) isok = false;
    if (_thermalConductivity <= 0.0) isok = false;
    
    return isok;
}




double AOT_ThermoMechMassMaterial::getProperty(const propertyName p) const
{
    return 0.0;
}




void AOT_ThermoMechMassMaterial::print(std::ostream &of) const
{
    of  << "\n Mechanical - mass - temperature transport coupled material by AOT."
        << "\n Model based on the work by Chester & Anand.";
    
    of  << "\n\n Properties of general 3-field material:";
    fThermoMechMassMaterial::print(of);
    
    of << "\n\n Specific properties of this model:"
        << "\n omega:  " << _Omega
        << "\n k:      " << _K
        << "\n diffusivity: " << _diff
        << "\n nr: " << _nr
        << "\n kb: " << _kb
        << "\n chil: " << _ChiL
        << "\n chih: " << _ChiH
        << "\n nabla: " << _Nabla
        << "\n thermal_expansion: " << _thermalExpansion
        << "\n mass mobility : " << _massMobility //NUEVA DEPENDE DE 1
        << "\n heat_capacity: " << _thermalCapacity
        << "\n conductivity: " << _thermalConductivity;
}




void AOT_ThermoMechMassMaterial::setRandom()
{
    setReferenceTemperature(298.0); //muesli::randomUniform(250.0, 350.0);
    _Omega = 1e-4; //muesli::randomUniform(1.0, 5.0)*1e-4;
    _K     = 1e8; //muesli::randomUniform(1.0, 10.0)*1e3;
    _R     = 8.1; //muesli::randomUniform(1.0, 10.0);
    _diff  = 1e-6; //muesli::randomUniform(1.0, 10.0)*1e-4;
    
    _thermalConductivity = muesli::randomUniform(1.0, 10.0);
    _thermalExpansion    = 1e-6 * muesli::randomUniform(1.0, 10.0);
    _thermalCapacity     = muesli::randomUniform(10.0, 100.0);
    _massMobility        = muesli::randomUniform(1e-3, 1e-2); //NUEVA DEPENDE DE 1
    _nr                 = 2.43e25;
    _kb                 = 1.37e-22;
    _ChiL               = 0.1;
    _ChiH               = 0.7;
    _Nabla              = 5.0;
    
    double J    = 1.0;
    double c    = 1.0e-4;
    double Js   = 1.0 + _Omega*c;
    double Je   = J/Js;
    double phi  = 1.0/Js;
    double lje  = log(Je);
    double chi = 0.1;
    double theta0 = referenceTemperature();
    _mu0 = -(_R* theta0 *(log(1.0-phi) + phi + chi*phi*phi)
             - _Omega * _K * lje + 0.5 * _K * _Omega * lje * lje);
}




bool AOT_ThermoMechMassMaterial::test(std::ostream &of)
{
    bool isok = true;
    setRandom();
    muesli::fThermoMechMassMP* p = this->createMaterialPoint();
    isok = p->testImplementation(of);
    return isok;
}




double AOT_ThermoMechMassMaterial::waveVelocity() const
{
    const double rho = density();
    return sqrt(2.0/rho);
}




AOT_ThermoMechMassMP::AOT_ThermoMechMassMP(const AOT_ThermoMechMassMaterial& m) :
    fThermoMechMassMP(m),
    theThermoMechMassMaterial(m)
{
    const double initConcentration = 0.5;
    c_c = c_n = fConcentration(F_c, mu_c, initConcentration, temp_c);
}




void AOT_ThermoMechMassMP::CauchyStress(istensor& sigma) const
{
    istensor S;
    secondPiolaKirchhoffStress(S);
    sigma = istensor::FSFt(F_c, S);
    sigma *= 1.0/J_c;
}




double AOT_ThermoMechMassMP::chemicalPotential() const
{
    return mu_c;
}




// (minus) d [ concentration] / d [ chemical potential ]
double AOT_ThermoMechMassMP::chemicalTangent() const
{
    const double inc = std::max<double>(1e-5, 1e-5*theThermoMechMassMaterial._mu0);
    double Xip1 = fConcentration(F_c, mu_c+inc, c_c, temp_c);
    double Xip2 = fConcentration(F_c, mu_c+2.0*inc, c_c, temp_c);
    double Xim1 = fConcentration(F_c, mu_c-inc, c_c, temp_c);
    double Xim2 = fConcentration(F_c, mu_c-2.0*inc, c_c, temp_c);

    // fourth order approximation of the (negative) derivative
    return -(-Xip2 + 8.0*Xip1 - 8.0*Xim1 + Xim2)/(12.0*inc);
}




itensor AOT_ThermoMechMassMP::der_cr_F() const
{
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    double inc = 1.0e-3;
    itensor F = F_c;
    itensor probando;

    for (size_t i=0; i<3; i++)
    {
        for (size_t j=0; j<3; j++)
        {
            const double original = F(i,j);

            F(i,j) = original + inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c,gradMu_c);

            double Xp1 = fConcentration(F, mu_c, c_c, temp_c);

            F(i,j) = original + 2.0*inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double Xp2 = fConcentration(F, mu_c, c_c, temp_c);

            F(i,j) = original - inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double Xm1 = fConcentration(F, mu_c, c_c, temp_c);

            F(i,j) = original -2.0*inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double Xm2 = fConcentration(F, mu_c, c_c, temp_c);

            probando(i,j) = (-Xp2 + 8.0*Xp1 - 8.0*Xm1 + Xm2)/(12.0*inc);

            F(i,j) = original;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c,  gradMu_c);
        }
    }
    return probando;
}




double AOT_ThermoMechMassMP::der_cr_Temp() const
{
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    double TTTemp3 = temp_c;
    double incc3 = 1.0e-3;
    double prueba3;
    {
        const double Ooriginal3 = TTTemp3;
        incc3 = 1e-5*theThermoMechMassMaterial.referenceTemperature();

        TTTemp3 = Ooriginal3 + incc3;
        theMP.updateCurrentState(time_c, F_c, TTTemp3, gradT_c, mu_c, gradMu_c);
        double Xp11 = fConcentration(F_c, mu_c, c_c, TTTemp3);

        TTTemp3 = Ooriginal3 + 2.0*incc3;
        theMP.updateCurrentState(time_c, F_c, TTTemp3, gradT_c, mu_c, gradMu_c);
        double Xp22 = fConcentration(F_c, mu_c, c_c, TTTemp3);

        TTTemp3 = Ooriginal3 - incc3;
        theMP.updateCurrentState(time_c, F_c, TTTemp3,  gradT_c, mu_c, gradMu_c);
        double Xm11 = fConcentration(F_c, mu_c, c_c, TTTemp3);

        TTTemp3 = Ooriginal3 - 2.0*incc3;
        theMP.updateCurrentState(time_c, F_c, TTTemp3, gradT_c, mu_c, gradMu_c);
        double Xm22 = fConcentration(F_c, mu_c, c_c, TTTemp3);

        prueba3 = (-Xp22 + 8.0*Xp11 - 8.0*Xm11 + Xm22)/(12.0*incc3);

        TTTemp3 = Ooriginal3;
        theMP.updateCurrentState(time_c, F_c, TTTemp3, gradT_c, mu_c, gradMu_c);

    }
    return prueba3;
}




double AOT_ThermoMechMassMP::Deriv_T_theta() const
{
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    double temp = temp_c;
    double num_Deriv_Y_theta;
    {
        const double original = temp;
        
        inc = 1e-5*theThermoMechMassMaterial.referenceTemperature();
        
        temp = original + inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double Y_p1 = DiffThermalCouplingParameter();
        
        temp = original + 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double Y_p2 = DiffThermalCouplingParameter();
        
        temp = original - inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double Y_m1 = DiffThermalCouplingParameter();
        
        temp = original - 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double Y_m2 = DiffThermalCouplingParameter();
        
        
        temp = original;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        
        // fourth order approximation of the derivative
        num_Deriv_Y_theta = (-Y_p2 + 8.0*Y_p1 - 8.0*Y_m1 + Y_m2)/(12.0*inc);
    }
    return num_Deriv_Y_theta;
}




double AOT_ThermoMechMassMP::Deriv_T_mu() const
{
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    double mu = mu_c;
    double num_Deriv_Y_mu;
    {
        const double original = mu;
        
        inc = std::max<double>(1e-5, 1e-5*theThermoMechMassMaterial._mu0);
        
        mu = original + inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double Y_p1 = DiffThermalCouplingParameter();
        
        mu = original + 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double Y_p2 = DiffThermalCouplingParameter();
        
        mu = original - inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double Y_m1 = DiffThermalCouplingParameter();
        
        mu = original - 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double Y_m2 = DiffThermalCouplingParameter();
        
        
        mu = original;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu_c, gradMu_c);
        
        // fourth order approximation of the derivative
        num_Deriv_Y_mu = (-Y_p2 + 8.0*Y_p1 - 8.0*Y_m1 + Y_m2)/(12.0*inc);
    }
    return num_Deriv_Y_mu;
}




itensor AOT_ThermoMechMassMP::Deriv_T_F() const
{
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    itensor F = F_c;
    itensor num_Deriv_T_F;
    itensor iF = F_c.inverse();
    for (size_t i=0; i<3; i++)
    {
        for (size_t j=0; j<3; j++)
        {
            const double original = F(i,j);
            
            F(i,j) = original + inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double Xp1 = DiffThermalCouplingParameter();
            
            F(i,j) = original + 2.0*inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double Xp2 = DiffThermalCouplingParameter();
            
            F(i,j) = original - inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double Xm1 = DiffThermalCouplingParameter();
            
            F(i,j) = original -2.0*inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double Xm2 = DiffThermalCouplingParameter();
            
            num_Deriv_T_F(i,j) = (-Xp2 + 8.0*Xp1 - 8.0*Xm1 + Xm2)/(12.0*inc);
            
            F(i,j) = original;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
        }
    }
    return num_Deriv_T_F;
}



itensor AOT_ThermoMechMassMP::Deriv_M_theta() const
{
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    double temp = temp_c;
    itensor DDeriv_M_theta;
    {
        const double original = temp;
        
        inc = 1e-3*theThermoMechMassMaterial.referenceTemperature();
        
        temp = original + inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        itensor Y_pp1 = ThermoMechCouplingTensor();
        
        temp = original + 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        itensor Y_pp2 = ThermoMechCouplingTensor();
        
        temp = original - inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        itensor Y_mm1 = ThermoMechCouplingTensor();
        
        temp = original - 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        itensor Y_mm2 = ThermoMechCouplingTensor();
        
        
        temp = original;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        
        // fourth order approximation of the derivative
        DDeriv_M_theta = (-Y_pp2 + 8.0*Y_pp1 - 8.0*Y_mm1 + Y_mm2)/(12.0*inc);
    }
    
    return DDeriv_M_theta;
}




itensor AOT_ThermoMechMassMP::Deriv_M_mu() const
{
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    double mu = mu_c;
    
    itensor DDeriv_M_mu;
    {
        const double original1 = mu;
        inc = std::max<double>(1e-3, 1e-3*theThermoMechMassMaterial._mu0);
        
        mu = original1 + inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        itensor YY_p1 = ThermoMechCouplingTensor();
        
        mu = original1 + 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        itensor YY_p2 = ThermoMechCouplingTensor();
        
        mu = original1 - inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        itensor YY_m1 = ThermoMechCouplingTensor();
        
        mu = original1 - 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        itensor YY_m2 = ThermoMechCouplingTensor();
        
        mu = original1;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        
        // fourth order approximation of the derivative
        DDeriv_M_mu = (-YY_p2 + 8.0*YY_p1 - 8.0*YY_m1 + YY_m2)/(12.0*inc);
    }
    return DDeriv_M_mu;
}




itensor AOT_ThermoMechMassMP::Deriv_M_F() const
{
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    itensor DDeriv_M_F;
    itensor F = F_c;
    itensor original = F;
    {
        F = original + inc * (itensor::identity());
        theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
        itensor YY_p1 = ThermoMechCouplingTensor();
        
        F = original + 2.0 * inc * (itensor::identity());
        theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
        itensor YY_p2 = ThermoMechCouplingTensor();
        
        F = original - inc * (itensor::identity());
        theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
        itensor YY_m1 = ThermoMechCouplingTensor();
        
        F = original -2.0*inc * (itensor::identity());
        theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
        itensor YY_m2 = ThermoMechCouplingTensor();
        
        DDeriv_M_F = (-YY_p2 + 8.0*YY_p1 - 8.0*YY_m1 + YY_m2)/(12.0*inc);
        
        F = original;
        theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
    }
    
    return DDeriv_M_F;
}




itensor AOT_ThermoMechMassMP::Deriv_S_theta() const
{
    itensor MMMs;
    double temp = temp_c;
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    
    const double original = temp;
    
    
    inc = 1e-3*theThermoMechMassMaterial.referenceTemperature();
    
    temp = original + inc;
    theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
    istensor S_p1; secondPiolaKirchhoffStress(S_p1);
    
    
    temp = original + 2.0*inc;
    theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
    istensor S_p2; secondPiolaKirchhoffStress(S_p2);
    
    temp = original - inc;
    theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
    istensor S_m1; secondPiolaKirchhoffStress(S_m1);
    
    temp = original - 2.0*inc;
    theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
    istensor S_m2; secondPiolaKirchhoffStress(S_m2);
    
    temp = original;
    theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
    
    // fourth order approximation of the derivative
    MMMs =  0.5 * temp * (-S_p2 + 8.0*S_p1 - 8.0*S_m1 + S_m2)/(12.0*inc);
    
    return MMMs;
}




itensor AOT_ThermoMechMassMP::Deriv_s_F() const
{
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    itensor F = F_c;
    itensor num_Deriv_s_F;
    itensor iF = F_c.inverse();
    for (size_t i=0; i<3; i++)
    {
        for (size_t j=0; j<3; j++)
        {
            const double original = F(i,j);
            
            F(i,j) = original + inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double sp1 = entropy();
            
            F(i,j) = original + 2.0*inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double sp2 = entropy();
            
            F(i,j) = original - inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double sm1 = entropy();
            
            F(i,j) = original -2.0*inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double sm2 = entropy();
            
            num_Deriv_s_F(i,j) = (-sp2 + 8.0*sp1 - 8.0*sm1 + sm2)/(12.0*inc);
            
            F(i,j) = original;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
        }
    }
    return num_Deriv_s_F;
}




double AOT_ThermoMechMassMP::Deriv_s_theta() const
{
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    double temp = temp_c;
    double num_Deriv_s_theta;
    {
        const double original = temp;
        
        inc = 1e-3*theThermoMechMassMaterial.referenceTemperature();
        
        temp = original + inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double ss_p1 = entropy();
        
        temp = original + 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double ss_p2 = entropy();
        
        temp = original - inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double ss_m1 = entropy();
        
        temp = original - 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double ss_m2 = entropy();
        
        
        temp = original;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        
        // fourth order approximation of the derivative
        num_Deriv_s_theta = (-ss_p2 + 8.0*ss_p1 - 8.0*ss_m1 + ss_m2)/(12.0*inc);
    }
    return num_Deriv_s_theta;
}




double AOT_ThermoMechMassMP::Deriv_s_mu() const
{
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    double mu = mu_c;
    double num_Deriv_s_mu;
    {
        const double original = mu;
        
        inc = std::max<double>(1e-5, 1e-5*theThermoMechMassMaterial._mu0);
        
        mu = original + inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double sss_p1 = entropy();
        
        mu = original + 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double sss_p2 = entropy();
        
        mu = original - inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double sss_m1 = entropy();
        
        mu = original - 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double sss_m2 = entropy();
        
        
        mu = original;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu_c, gradMu_c);
        
        // fourth order approximation of the derivative
        num_Deriv_s_mu = (-sss_p2 + 8.0*sss_p1 - 8.0*sss_m1 + sss_m2)/(12.0*inc);
    }
    return num_Deriv_s_mu;
}




double AOT_ThermoMechMassMP::concentration() const
{
    return c_c;
}




double AOT_ThermoMechMassMP::entropy_n() const
{
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    double temp = temp_n;
    double ss_n;
    {
        const double original = temp;

        inc = 1e-3*theThermoMechMassMaterial.referenceTemperature();

        temp = original + inc;
        theMP.updateCurrentState(time_n, F_n, temp, gradT_n, mu_n, gradMu_n);
        double freeE_p1 = grandCanonicalPotential();

        temp = original + 2.0*inc;
        theMP.updateCurrentState(time_n, F_n, temp,  gradT_n, mu_n, gradMu_n);
        double freeE_p2 = grandCanonicalPotential();

        temp = original - inc;
        theMP.updateCurrentState(time_n, F_n, temp, gradT_n, mu_n, gradMu_n);
        double freeE_m1 = grandCanonicalPotential();

        temp = original - 2.0*inc;
        theMP.updateCurrentState(time_n, F_n, temp, gradT_n, mu_n, gradMu_n);
        double freeE_m2 = grandCanonicalPotential();

        temp = original;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu_c, gradMu_c);

        // fourth order approximation of the derivative
        ss_n = (-freeE_p2 + 8.0*freeE_p1 - 8.0*freeE_m1 + freeE_m2)/(12.0*inc);
    }
    return -ss_n;
}




double AOT_ThermoMechMassMP::entropy_c() const
{
    return entropy();
}




itensor AOT_ThermoMechMassMP::prueba_F_c() const
{
    return F_c;
}




void AOT_ThermoMechMassMP::contractWithAllTangents(const ivector &v1,
                                            const ivector& v2,
                                            itensor&  Tdev,
                                            istensor& Tmixed,
                                            double&   Tvol) const
{

}




void AOT_ThermoMechMassMP::contractWithConvectedTangent(const ivector& v1, const ivector& v2, itensor& T) const
{
    itensor4 c;
    convectedTangent(c);

    T.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    T(i,k) += c(i,j,k,l)*v1(j)*v2(l);
                }
}




void AOT_ThermoMechMassMP::contractWithDeviatoricTangent(const ivector& v1, const ivector& v2, itensor& T) const
{
    const istensor id = istensor::identity();
    itensor4 Pdev;
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                    Pdev(i,j,k,l) = 0.5 * ( id(i,k)*id(j,l) + id(i,l)*id(j,k) ) - 1.0/3.0 * id(i,j)*id(k,l);

    itensor4 st;
    spatialTangent(st);

    itensor4 cdev;
    cdev.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                    for (unsigned m=0; m<3; m++)
                        for (unsigned n=0; n<3; n++)
                            for (unsigned p=0; p<3; p++)
                                for (unsigned q=0; q<3; q++)
                                    cdev(i,j,p,q) += Pdev(i,j,k,l)*st(k,l,m,n)*Pdev(m,n,p,q);

    T.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    T(i,k) += cdev(i,j,k,l)*v1(j)*v2(l);
                }
}




// CM_ij = P_ijkl*c_klmm
void AOT_ThermoMechMassMP::contractWithMixedTangent(istensor& CM) const
{
    const istensor id = istensor::identity();
    itensor4 Pdev;
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                    Pdev(i,j,k,l) = 0.5 * ( id(i,k)*id(j,l) + id(i,l)*id(j,k) ) - 1.0/3.0 * id(i,j)*id(k,l);

    itensor4 st;
    spatialTangent(st);

    CM.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                    for (unsigned m=0; m<3; m++)
                        CM(i,j) += Pdev(i,j,k,l)*st(k,l,m,m);
}




void AOT_ThermoMechMassMP::contractWithSpatialTangent(const ivector& v1, const ivector& v2, itensor& T) const
{
    itensor4 c;
    spatialTangent(c);

    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                    T(i,k) += c(i,j,k,l)*v1(j)*v2(l);
}




void AOT_ThermoMechMassMP::contractTangent(const ivector& na, const ivector& nb, double& tg) const
{
    std::cout << "\n contractTangent not implemented";
}




void AOT_ThermoMechMassMP::convectedTangent(itensor4& ctg) const
{
    // transform material tangent to get the convected tangent
    istensor C  = istensor::tensorTransposedTimesTensor(F_c);
    istensor Ci = C.inverse();
    itensor4 mtang;
    materialTangent(mtang);
    itensor  Fi  = F_c.inverse();
    istensor SS;
    secondPiolaKirchhoffStress(SS);
    itensor4 pruebactg;
    for (unsigned a=0; a<3; a++)
        for (unsigned b=0; b<3; b++)
            for (unsigned c=0; c<3; c++)
                for (unsigned d=0; d<3; d++)
                {
                    pruebactg(c,a,d,b) = - SS(a,b)*Ci(c,d);

                    for (unsigned i=0; i<3; i++)
                        for (unsigned j=0; j<3; j++)
                            pruebactg(c,a,d,b) += Fi(c,i)*mtang(i,a,j,b)*Fi(d,j);
                }
    ctg = pruebactg;
}




void AOT_ThermoMechMassMP::convectedTangentMatrix(double C[6][6]) const
{
    itensor4 ct;
    convectedTangent(ct);
    muesli::tensorToMatrix(ct, C);
}




void AOT_ThermoMechMassMP::convectedTangentTimesSymmetricTensor(const istensor &M, istensor &CM) const
{
    itensor4 C;
    convectedTangent(C);

    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    CM(i,j) += C(i,j,k,l)*M(k,l);
                }
}




double AOT_ThermoMechMassMP::convergedConcentration() const
{
    return c_n;
}




double AOT_ThermoMechMassMP::diffusionPotential() const
{
    const double diff   = theThermoMechMassMaterial._diff;
    const double R      = theThermoMechMassMaterial._R;

    return -0.5 * c_c/J_c * diff/R/temp_c * gradMu_c.squaredNorm();
}




istensor AOT_ThermoMechMassMP::diffusionTangent() const
{
    const double diff   = theThermoMechMassMaterial._diff;
    const double R      = theThermoMechMassMaterial._R;

    return c_c/J_c * diff/R/temp_c * istensor::identity();
}




istensor AOT_ThermoMechMassMP::diffusionTangent_div_cr() const
{
    const double diff   = theThermoMechMassMaterial._diff;
    const double R      = theThermoMechMassMaterial._R;

    return 1.0/J_c * diff/R/temp_c * istensor::identity();
}




double AOT_ThermoMechMassMP::dissipation() const
{
    return 0.0;
}




double AOT_ThermoMechMassMP::effectiveFreeEnergy() const
{
    return freeEnergy();
}




void AOT_ThermoMechMassMP::energyMomentumTensor(itensor &EM) const
{

}




double AOT_ThermoMechMassMP::fChemicalPotential(const itensor& F, double c) const
{
    const double K = theThermoMechMassMaterial._K;
    const double O = theThermoMechMassMaterial._Omega;
    const double mu0 = theThermoMechMassMaterial._mu0;
    const double R = theThermoMechMassMaterial._R;

    double J    = F.determinant();
    double Js   = 1.0 + O*c;
    double Je   = J/Js;
    double phi  = 1.0/Js;
    double lje  = log(Je);

    double mu = mu0 + R * temp_c * (log(1.0-phi) + phi + CHItheta(temp_c)*phi*phi)
    - O*K*lje + 0.5*K*O*lje*lje;

    return mu;
}




double AOT_ThermoMechMassMP::fConcentration(const itensor&F, const double mu, const double c0, const double tempe) const
{
    const double K = theThermoMechMassMaterial._K;
    const double O = theThermoMechMassMaterial._Omega;
    const double R = theThermoMechMassMaterial._R;
    const double mu0 = theThermoMechMassMaterial._mu0;

    double c = c0;
    double error =  mu - fChemicalPotential(F, c);
    unsigned count = 0;
    const double tol = std::max<double>(1e-15*mu0, 1e-15);
    double J    = F.determinant();

    while ( fabs(error) > tol && count++ < 90)
    {
        double Js   = 1.0 + O*c;
        double Je   = J/Js;
        double phi  = 1.0/Js;
        double lje  = log(Je);

        double tg = - K*O*O/Js*(1.0 - lje) + R*tempe*O/Js/Js*( (-phi)/(1.0-phi) + 2.0*CHItheta(tempe)*phi);
        c -= error/tg;
        c = std::max<double>(1e-7, c);
        error =  mu - fChemicalPotential(F, c);
    }

    if (count == 90)
        std::cout << "\n fConcentration not converged.";
    return c;
}




itensor AOT_ThermoMechMassMP::fFirstPiolaKirchhoffStress(const itensor& F, double mu) const
{
    const double K = theThermoMechMassMaterial._K;
    const double O = theThermoMechMassMaterial._Omega;

    double c  = fConcentration(F, mu, c_c, temp_c);
    double J  = F.determinant();
    itensor iFt = F.inverse().transpose();

    double Js = 1.0 + O*c;
    double Je = J/Js;

    return Gtheta()*(F - iFt) + K*Js*log(Je)*iFt;
}




void AOT_ThermoMechMassMP::firstPiolaKirchhoffStress(itensor &P) const
{
    istensor S;
    secondPiolaKirchhoffStress(S);
    P = F_c*S;
}




double AOT_ThermoMechMassMP::freeEnergy() const
{
    const double K = theThermoMechMassMaterial._K;
    const double O = theThermoMechMassMaterial._Omega;
    const double mu0 = theThermoMechMassMaterial._mu0;
    const double R = theThermoMechMassMaterial._R;
    const double tref = theThermoMechMassMaterial.referenceTemperature();
    const double cc00 = theThermoMechMassMaterial._thermalCapacity;

    const istensor C = istensor::tensorTransposedTimesTensor(F_c);
    double lbar  = sqrt(C.trace()/3.0);
    double Js    = 1.0 + O*c_c;
    double Je    = J_c/Js;
    double logJe = log(Je);

    double psi = mu0*c_c + R*temp_c*c_c*( log(O*c_c/Js) + CHItheta(temp_c)/Js)
                + 0.5*Gtheta()*(3.0*(lbar*lbar - 1.0) - 2.0*log(J_c))
                + Js*0.5*K*logJe*logJe
                + cc00 * ((temp_c - tref) - temp_c * log(temp_c/tref));
    
    return psi;
}




double AOT_ThermoMechMassMP::Gtheta() const
{
    const double N_r = theThermoMechMassMaterial._nr;
    const double K_b = theThermoMechMassMaterial._kb;

    double Gt = N_r * K_b * temp_c;
    return Gt;
}




double AOT_ThermoMechMassMP::CHItheta(double tempe) const
{
    const double tref = theThermoMechMassMaterial.referenceTemperature();
    const double ChiL = theThermoMechMassMaterial._ChiL;
    const double ChiH = theThermoMechMassMaterial._ChiH;
    const double Nabla = theThermoMechMassMaterial._Nabla;
    double CHIt = 0.5 * (ChiL + ChiH) - 0.5 * (ChiL - ChiH) * tanh((tempe - tref)/Nabla);
    return CHIt;
}




double AOT_ThermoMechMassMP::Deriv_CHItheta_theta() const
{
    const double ChiL = theThermoMechMassMaterial._ChiL;
    const double ChiH = theThermoMechMassMaterial._ChiH;
    const double Nabla = theThermoMechMassMaterial._Nabla;
    double valor;
    const double tref = theThermoMechMassMaterial.referenceTemperature();
    valor = -0.5 * (ChiL - ChiH) * (1.0/Nabla) *  1.0/((cosh((temp_c - tref)/Nabla))*(cosh((temp_c - tref)/Nabla)));
    return valor;
}




double AOT_ThermoMechMassMP::freeEntropy() const
{
    return -freeEnergy()/temp_c;
}




materialState AOT_ThermoMechMassMP::getConvergedState() const
{
    materialState state;

    state.theTime = time_n;
    state.theDouble.push_back(mu_n);
    state.theDouble.push_back(c_n);
    state.theDouble.push_back(J_n);
    state.theVector.push_back(gradMu_n);
    state.theTensor.push_back(F_n);
    state.theDouble.push_back(temp_n);
    state.theVector.push_back(gradT_n);

    return state;
}




materialState AOT_ThermoMechMassMP::getCurrentState() const
{
    materialState state;

    state.theTime = time_c;
    state.theDouble.push_back(mu_c);
    state.theDouble.push_back(c_c);
    state.theDouble.push_back(J_c);
    state.theVector.push_back(gradMu_c);
    state.theTensor.push_back(F_c);
    state.theDouble.push_back(temp_c);
    state.theVector.push_back(gradT_c);

    return state;
}




double AOT_ThermoMechMassMP::grandCanonicalPotential() const
{
    return freeEnergy() - c_c * mu_c;
}




double AOT_ThermoMechMassMP::dissipatedEnergy() const
{
    return 0.0;
}




ivector& AOT_ThermoMechMassMP::gradT()
{
    return gradT_c;
}




const ivector& AOT_ThermoMechMassMP::gradT() const
{
    return gradT_c;
}




ivector& AOT_ThermoMechMassMP::gradMU()
{
    return gradMu_c;
}




const ivector& AOT_ThermoMechMassMP::gradMU() const
{
    return gradMu_c;
}




double AOT_ThermoMechMassMP::internalEnergy() const
{
    return freeEnergy();
}




double AOT_ThermoMechMassMP::kineticPotential() const
{
    return diffusionPotential();
}




void AOT_ThermoMechMassMP::KirchhoffStress(istensor &tau) const
{
    istensor sigma;
    CauchyStress(sigma);
    tau = sigma * J_c;
}




ivector AOT_ThermoMechMassMP::materialMassFlux() const
{
    const double diff = theThermoMechMassMaterial._diff;
    const double R    = theThermoMechMassMaterial._R;

    ivector j = - c_c/J_c * diff/R/temp_c *gradMu_c;
    return j;
}




// -d[J]/d[D]
istensor AOT_ThermoMechMassMP::materialMobility() const  //NUEVA1
{
    const double m = theThermoMechMassMaterial._massMobility;
    istensor K;
    if (theThermoMechMassMaterial.isVariational())
    {
        K = istensor::scaledIdentity(m*temp_n/temp_c);
    }
    else
    {
        const double diff = theThermoMechMassMaterial._diff; //NUEVA22
        const double R    = theThermoMechMassMaterial._R; //NUEVA22
        
        //K = istensor::scaledIdentity(m); //NUEVA22 COMENTADA
        K= istensor::scaledIdentity(c_c/J_c * diff/R/temp_c);
    }
    return K;
}




ivector AOT_ThermoMechMassMP::materialMassFluxDTheta() const //NUEVA2
{
    const double m = theThermoMechMassMaterial._massMobility;
    
    ivector Jprime;
    if (theThermoMechMassMaterial.isVariational())
    {
        Jprime = 2.0 * m * temp_n * temp_n /(temp_c * temp_c * temp_c) * gradMu_c;
    }
    else
    {
        Jprime.setZero();
    }
    return Jprime;
}




itensor AOT_ThermoMechMassMP::materialCouplingTensor() const
{
    const double inc = std::max<double>(1e-5*theThermoMechMassMaterial._mu0, 1e-5);
    itensor Pp1 = fFirstPiolaKirchhoffStress(F_c, mu_c+inc);
    itensor Pp2 = fFirstPiolaKirchhoffStress(F_c, mu_c+2.0*inc);
    itensor Pm1 = fFirstPiolaKirchhoffStress(F_c, mu_c-inc);
    itensor Pm2 = fFirstPiolaKirchhoffStress(F_c, mu_c-2.0*inc);

    // fourth order approximation of the derivative
    itensor M = (-Pp2 + 8.0*Pp1 - 8.0*Pm1 + Pm2)/(12.0*inc);

    return M;
}




itensor AOT_ThermoMechMassMP::ThermoMechCouplingTensor() const
{
     double inc = 10e-3;
     AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
     itensor F = F_c;
     itensor MMMs;

    for (size_t i=0; i<3; i++)
    {
        for (size_t j=0; j<3; j++)
        {
            const double original = F(i,j);

            F(i,j) = original + inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double Xp1 = entropy();

            F(i,j) = original + 2.0*inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double Xp2 = entropy();

            F(i,j) = original - inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double Xm1 = entropy();

            F(i,j) = original -2.0*inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double Xm2 = entropy();

            MMMs(i,j) = (-Xp2 + 8.0*Xp1 - 8.0*Xm1 + Xm2)/(12.0*inc);

            F(i,j) = original;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
        }
    }

    return temp_c*MMMs;
}




double AOT_ThermoMechMassMP::DiffThermalCouplingParameter() const
{
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    double mu = mu_c;
    double deriv_Entr_mu;
    {
        const double original = mu;

        inc = std::max<double>(1e-5, 1e-5*theThermoMechMassMaterial._mu0);

        mu = original + inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double YY_p1 = entropy();

        mu = original + 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double YY_p2 = entropy();

        mu = original - inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double YY_m1 = entropy();

        mu = original - 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double YY_m2 = entropy();

        mu = original;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu_c, gradMu_c);

        // fourth order approximation of the derivative
        deriv_Entr_mu = (-YY_p2 + 8.0*YY_p1 - 8.0*YY_m1 + YY_m2)/(12.0*inc);
    }

    double conc = c_c;
    double deriv_Entr_c;

    {
        const double original = conc;

        inc = 1e-5;

        conc = original + inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu_c, gradMu_c);
        double YY_pp1 = entropy();

        conc = original + 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu_c, gradMu_c);
        double YY_pp2 = entropy();

        conc = original - inc;
        theMP.updateCurrentState(time_c, F_c, temp_c,  gradT_c, mu_c, gradMu_c);
        double YY_mm1 = entropy();

        conc = original - 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu_c, gradMu_c);
        double YY_mm2 = entropy();

        conc = original;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu_c, gradMu_c);

        // fourth order approximation of the derivative
        deriv_Entr_c = (-YY_pp2 + 8.0*YY_pp1 - 8.0*YY_mm1 + YY_mm2)/(12.0*inc);
    }


    double mu1 = mu_c;
    {
        const double original = mu1;

        inc = std::max<double>(1e-5, 1e-5*theThermoMechMassMaterial._mu0);

        mu1 = original + inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu1, gradMu_c);
        double YY_p11 = c_c;

        mu1 = original + 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu1, gradMu_c);
        double YY_p22 = entropy();

        mu1 = original - inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu1, gradMu_c);
        double YY_m11 = entropy();

        mu1 = original - 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu1, gradMu_c);
        double YY_m22 = entropy();

        mu1 = original;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu1, gradMu_c);

        // fourth order approximation of the derivative
        deriv_Entr_mu = (-YY_p22 + 8.0*YY_p11 - 8.0*YY_m11 + YY_m22)/(12.0*inc);
    }

    return temp_c*deriv_Entr_mu+ temp_c*deriv_Entr_c*deriv_Entr_mu;
}




double AOT_ThermoMechMassMP::entropy() const
{
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    double temp = temp_c;
    double derivada_Phi_theta;
    {
        const double original = temp;

        inc = 1e-3*theThermoMechMassMaterial.referenceTemperature();

        temp = original + inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double freeE_p1 = grandCanonicalPotential();

        temp = original + 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double freeE_p2 = grandCanonicalPotential();

        temp = original - inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double freeE_m1 = grandCanonicalPotential();

        temp = original - 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double freeE_m2 = grandCanonicalPotential();

        temp = original;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);

        // fourth order approximation of the derivative
        derivada_Phi_theta = (-freeE_p2 + 8.0*freeE_p1 - 8.0*freeE_m1 + freeE_m2)/(12.0*inc);
    }

    //return derivada_Phi_theta;
    return -derivada_Phi_theta;
}




double AOT_ThermoMechMassMP::heatCapacity() const
{
    itensor num_Ms;
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    double temp = temp_c;
    double num_heatCap;
    {
        const double original = temp;

        inc = 1e-5*theThermoMechMassMaterial.referenceTemperature();

        temp = original + inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double entrop_p1 = entropy();

        temp = original + 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double entrop_p2 = entropy();

        temp = original - inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double entrop_m1 = entropy();

        temp = original - 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double entrop_m2 = entropy();


        temp = original;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);

        // fourth order approximation of the derivative
        num_heatCap = temp*(-entrop_p2 + 8.0*entrop_p1 - 8.0*entrop_m1 + entrop_m2)/(12.0*inc);
    }
    return num_heatCap;
}




// coupling tensor M = d_P/d_mu
itensor AOT_ThermoMechMassMP::materialStressChemicalTensor() const //NUEVA 3 //sería materialCouplingTensor en material de Ángel
{
    //itensor Fcinv = F_c.inverse();

    //const double beta  = theThermoMechMassMaterial._massExpansion;
    //const double R     = theThermoMechMassMaterial._R;

    //return -c_c * 3.0 * beta * kappa /(R*temp_c) * Fcinv.transpose();

    const double inc = std::max<double>(1e-5*theThermoMechMassMaterial._mu0, 1e-5);
    itensor Pp1 = fFirstPiolaKirchhoffStress(F_c, mu_c+inc);
    itensor Pp2 = fFirstPiolaKirchhoffStress(F_c, mu_c+2.0*inc);
    itensor Pm1 = fFirstPiolaKirchhoffStress(F_c, mu_c-inc);
    itensor Pm2 = fFirstPiolaKirchhoffStress(F_c, mu_c-2.0*inc);

    // fourth order approximation of the derivative
    itensor M = (-Pp2 + 8.0*Pp1 - 8.0*Pm1 + Pm2)/(12.0*inc);

    return M;
}




// coupling tensor M = d^2(GCP)/(d_F d_Theta) = d_P/d_Theta
itensor AOT_ThermoMechMassMP::materialStressTemperatureTensor() const //NUEVA 4 //sería Deriv_s_F en material de Ángel
{
    //itensor Fcinv = F_c.inverse();

    //const double alpha = theThermoMechMassMaterial._thermalExpansion;
    //const double beta  = theThermoMechMassMaterial._massExpansion;
    //return (log(c_c/NM)*3.0*beta*kappa*c_c/temp_c-3.0*alpha*kappa)*Fcinv.transpose();

    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    itensor F = F_c;
    itensor num_Deriv_s_F;
    itensor iF = F_c.inverse();
    for (size_t i=0; i<3; i++)
    {
        for (size_t j=0; j<3; j++)
        {
            const double original = F(i,j);

            F(i,j) = original + inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double sp1 = entropy();

            F(i,j) = original + 2.0*inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double sp2 = entropy();

            F(i,j) = original - inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double sm1 = entropy();

            F(i,j) = original -2.0*inc;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            double sm2 = entropy();

            num_Deriv_s_F(i,j) = (-sp2 + 8.0*sp1 - 8.0*sm1 + sm2)/(12.0*inc);

            F(i,j) = original;
            theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
        }
    }
    return num_Deriv_s_F;
}




// - d chi / d theta = d^ GCP / (dtheta dmu)
double AOT_ThermoMechMassMP::temperatureChemicalCoupling() const //NUEVA 5 //sería Deriv_s_mu en material de Ángel
{
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    double mu = mu_c;
    double num_Deriv_s_mu;
    {
        const double original = mu;

        inc = std::max<double>(1e-5, 1e-5*theThermoMechMassMaterial._mu0);

        mu = original + inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double sss_p1 = entropy();

        mu = original + 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double sss_p2 = entropy();

        mu = original - inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double sss_m1 = entropy();

        mu = original - 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu, gradMu_c);
        double sss_m2 = entropy();


        mu = original;
        theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu_c, gradMu_c);

        // fourth order approximation of the derivative
        num_Deriv_s_mu = (-sss_p2 + 8.0*sss_p1 - 8.0*sss_m1 + sss_m2)/(12.0*inc);
    }
    return num_Deriv_s_mu;
}




// d^2 GCP / d(theta)^2
double AOT_ThermoMechMassMP::temperatureTangent() const //NUEVA 6 //sería Deriv_s_theta de material de Ángel
{
    double inc = 10e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);
    double temp = temp_c;
    double num_Deriv_s_theta;
    {
        const double original = temp;

        inc = 1e-3*theThermoMechMassMaterial.referenceTemperature();

        temp = original + inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double ss_p1 = entropy();

        temp = original + 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double ss_p2 = entropy();

        temp = original - inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double ss_m1 = entropy();

        temp = original - 2.0*inc;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);
        double ss_m2 = entropy();


        temp = original;
        theMP.updateCurrentState(time_c, F_c, temp, gradT_c, mu_c, gradMu_c);

        // fourth order approximation of the derivative
        num_Deriv_s_theta = (-ss_p2 + 8.0*ss_p1 - 8.0*ss_m1 + ss_m2)/(12.0*inc);
    }
    return num_Deriv_s_theta;
}




ivector AOT_ThermoMechMassMP::materialHeatFlux() const
{
    itensor iF = F_c.inverse();
    itensor iFt = iF.transpose();
    return -iF * theThermoMechMassMaterial._thermalConductivity * iFt * gradT_c;
}




istensor AOT_ThermoMechMassMP::materialConductivity() const
{
    return theThermoMechMassMaterial._thermalConductivity * istensor::identity();
}




void AOT_ThermoMechMassMP::materialTangent(itensor4& cm) const
{
    itensor4 CmPRUEBA;
    itensor4 CmPRUEBAparte;
    const double K = theThermoMechMassMaterial._K;
    const double O = theThermoMechMassMaterial._Omega;

    double Js   = 1.0 + O*c_c;
    double Je   = J_c/Js;

    itensor Fi=F_c.inverse();
    itensor Fit=F_c.inverse().transpose();

    itensor d_P_cr = K*O*(log(Je)-1.0)*Fit;
    itensor d_cr_F;
    d_cr_F = der_cr_F();

    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                {
                    CmPRUEBA(i,j,k,l) += Gtheta()*Fi(l,i)*Fi(j,k)   +   Js*K* Fi(j,i)*Fi(l,k)   -  Js*K*(log(Je))*Fi(l,i)*Fi(j,k);
                    CmPRUEBA(i,j,k,l) += d_P_cr(i,j) * d_cr_F(k,l);
                }
    CmPRUEBA += Gtheta()*itensor4::identity();

    cm = CmPRUEBA;
}




double AOT_ThermoMechMassMP::plasticSlip() const
{
    return 0.0;
}



/*
double AOT_ThermoMechMassMP::referenceChemicalPotential() const
{
    return theThermoMechMassMaterial._mu0;
}
*/



void AOT_ThermoMechMassMP::secondPiolaKirchhoffStress(istensor& S) const
{
    const double K = theThermoMechMassMaterial._K;
    const double O = theThermoMechMassMaterial._Omega;

    istensor Cinv = istensor::tensorTimesTensorTransposed(F_c.inverse());
    double Js  = 1.0 + O*c_c;
    double Je  = J_c/Js;

    S =  Gtheta() * (istensor::identity()-Cinv) + K*Js*log(Je)*Cinv;
}




void AOT_ThermoMechMassMP::spatialTangent(itensor4& Cs) const
{

    const itensor& Fc = F_c;
    itensor4 Cc;
    convectedTangent(Cc);

    Cs.setZero();
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            for (unsigned k=0; k<3; k++)
                for (unsigned l=0; l<3; l++)
                    for (unsigned m=0; m<3; m++)
                        for (unsigned n=0; n<3; n++)
                            for (unsigned p=0; p<3; p++)
                                for (unsigned q=0; q<3; q++)
                                    Cs(i,j,k,l) += Fc(i,m)*Fc(j,n)*Fc(k,p)*Fc(l,q)*Cc(m,n,p,q);

    Cs *= 1.0/J_c;
}




void AOT_ThermoMechMassMP::spatialTangentMatrix(double c[6][6]) const
{
    itensor4 st;
    spatialTangent(st);
    muesli::tensorToMatrix(st, c);
}




double& AOT_ThermoMechMassMP::temperature()
{
    return temp_c;
}




const double& AOT_ThermoMechMassMP::temperature() const
{
    return temp_c;
}




bool AOT_ThermoMechMassMP::testImplementation(std::ostream& of, const bool testDE, const bool testDDE) const
{
    bool isok = true;
    double inc = 1.0e-3;
    AOT_ThermoMechMassMP& theMP = const_cast<AOT_ThermoMechMassMP&>(*this);

    // set a random update in the material

    itensor F_n;
    F_n.setRandom();
    F_n *= 1e-2;
    F_n += itensor::identity();
    if (F_n.determinant() < 0.0) F_n *= -1.0;

    ivector gradT; gradT.setRandom();
    double  temp;
    temp = randomUniform(0.8, 1.2) * theThermoMechMassMaterial.referenceTemperature();

    ivector gradMu_n;
    gradMu_n.setRandom();

    double  mu_n;
    mu_n = randomUniform(-5000.0, -5000.0); //AGO 21 QUITAR ESTE


    theMP.updateCurrentState(0.0, F_n, temp, gradT, mu_n, gradMu_n);
    theMP.commitCurrentState();

    double tn1 = muesli::randomUniform(0.1,1.0); //AQUI (0.1,1.0) este da error

    itensor F;
    F.setRandom();
    F *= 1e-1;
    F += itensor::identity();
    if (F.determinant() < 0.0) F *= -1.0;

    ivector gradMu;
    gradMu.setRandom();

    double mu;

    mu = randomUniform(0.0, 0.0);
    //double conc;


    theMP.updateCurrentState(tn1, F, temp, gradT, mu, gradMu);


    itensor4 ctg;
    theMP.convectedTangent(ctg);
    itensor4 ctg2;

    // >>>>>>>Derivatives with respect to F
    // Compute numerical value of P, 1PK stress tensor, = d Psi / d F
    // Compute numerical value of A, material tangent A_{iAjB} = d (P_iA) / d F_jB
    itensor num_P;
    itensor4 num_A;
    {
        itensor dP, Pp1, Pp2, Pm1, Pm2;

        for (size_t i=0; i<3; i++)
        {
            for (size_t j=0; j<3; j++)
            {
                const double original = F(i,j);

                F(i,j) = original + inc;
                theMP.updateCurrentState(tn1, F, temp, gradT, mu, gradMu);
                double Wp1 = grandCanonicalPotential();
                firstPiolaKirchhoffStress(Pp1);

                F(i,j) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, F, temp, gradT, mu, gradMu);
                double Wp2 = grandCanonicalPotential();
                firstPiolaKirchhoffStress(Pp2);

                F(i,j) = original - inc;
                theMP.updateCurrentState(tn1, F, temp, gradT, mu, gradMu);
                double Wm1 = grandCanonicalPotential();
                firstPiolaKirchhoffStress(Pm1);

                F(i,j) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, F, temp, gradT, mu, gradMu);
                double Wm2 = grandCanonicalPotential();
                firstPiolaKirchhoffStress(Pm2);

                // fourth order approximation of the derivative
                num_P(i,j) = (-Wp2 + 8.0*Wp1 - 8.0*Wm1 + Wm2)/(12.0*inc);


                // derivative of PK stress
                dP = (-Pp2 + 8.0*Pp1 - 8.0*Pm1 + Pm2)/(12.0*inc);
                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                        num_A(k,l,i,j) = dP(k,l);

                F(i,j) = original;
                theMP.updateCurrentState(tn1, F, temp, gradT, mu, gradMu);
            }
        }
    }



    // >>>>>>>Derivatives with respect to mu
    // Compute numerical value of concentration c =  -(d GCP / d mu)
    // Compute numerical value of coupling tensor M = d P / d mu
    // Compute numerical value of chemical tangent =  -(d c / d mu)
    inc = std::max<double>(1e-5, 1e-5*theThermoMechMassMaterial._mu0);
    double  num_c;
    itensor num_coupling;
    double  num_kmu;
    {
        theMP.updateCurrentState(tn1, F, temp, gradT, mu+inc, gradMu);
        double  GCP_p1 = grandCanonicalPotential();
        itensor PK_p1; firstPiolaKirchhoffStress(PK_p1);
        double  c_p1 = concentration();

        theMP.updateCurrentState(tn1, F, temp, gradT, mu+2.0*inc, gradMu);
        double  GCP_p2 = grandCanonicalPotential();
        itensor PK_p2; firstPiolaKirchhoffStress(PK_p2);
        double  c_p2 = concentration();

        theMP.updateCurrentState(tn1, F, temp, gradT, mu-inc, gradMu);
        double  GCP_m1 = grandCanonicalPotential();
        itensor PK_m1; firstPiolaKirchhoffStress(PK_m1);
        double  c_m1 = concentration();

        theMP.updateCurrentState(tn1, F, temp, gradT, mu-2.0*inc, gradMu);
        double  GCP_m2 = grandCanonicalPotential();
        itensor PK_m2; firstPiolaKirchhoffStress(PK_m2);
        double  c_m2 = concentration();

        theMP.updateCurrentState(tn1, F, temp, gradT, mu, gradMu);

        // fourth order approximation of the derivative
        num_c        = -(-GCP_p2 + 8.0*GCP_p1 - 8.0*GCP_m1 + GCP_m2)/(12.0*inc);
        num_coupling =  (-PK_p2 + 8.0*PK_p1 - 8.0*PK_m1 + PK_m2)/(12.0*inc);
        num_kmu      = -(-c_p2 + 8.0*c_p1  - 8.0*c_m1  + c_m2)/(12.0*inc);
    }


    itensor num_M1;
    itensor num_M;
    double num_DiffThercoup;
    double num_HeatCapac;
    double incr_T = 10e-3;
    double incr_C = 10e-3;
    double conce = c_c;
    double nP1MU;
    double nP2MU;
    double nPtethaC;
    {
        const double original = temp;
        const double K = theThermoMechMassMaterial._K;
        const double O = theThermoMechMassMaterial._Omega;
        const double mu0 = theThermoMechMassMaterial._mu0;
        const double R = theThermoMechMassMaterial._R;
        //const double tref = theMechMassMaterial._theta0;

        inc = 1e-5*theThermoMechMassMaterial.referenceTemperature();

        temp = original + inc;
        theMP.updateCurrentState(tn1, F, temp, gradT, mu, gradMu);
        double Ent_p1 = entropy();

        temp = original + 2.0*inc;
        theMP.updateCurrentState(tn1, F, temp, gradT, mu, gradMu);
        double Ent_p2 = entropy();

        temp = original - inc;
        theMP.updateCurrentState(tn1, F, temp, gradT, mu, gradMu);
        double Ent_m1 = entropy();

        temp = original - 2.0*inc;
        theMP.updateCurrentState(tn1, F, temp, gradT, mu, gradMu);
        double Ent_m2 = entropy();


        temp = original;
        theMP.updateCurrentState(tn1, F, temp, gradT, mu, gradMu);

        // fourth order approximation of the derivative
        num_HeatCapac = temp * (-Ent_p2 + 8.0*Ent_p1 - 8.0*Ent_m1 + Ent_m2)/(12.0*inc);




        for (size_t i=0; i<3; i++)
        {
            for (size_t j=0; j<3; j++)
            {
                const double original = F(i,j);

                F(i,j) = original + inc;
                theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
                double Xp1 = entropy();

                F(i,j) = original + 2.0*inc;
                theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
                double Xp2 = entropy();

                F(i,j) = original - inc;
                theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
                double Xm1 = entropy();

                F(i,j) = original -2.0*inc;
                theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
                double Xm2 = entropy();

                num_M1(i,j) = (-Xp2 + 8.0*Xp1 - 8.0*Xm1 + Xm2)/(12.0*inc);

                F(i,j) = original;
                theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);
            }
        }


        double conc = c_c;
        double Deriv_entropy_c;
        {
            const double original = conc;

            inc = std::max<double>(1e-5, 1e-5*theThermoMechMassMaterial._mu0);

            conc = original + inc;
            theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu_c, gradMu_c);
            double Y_p1 = entropy();

            conc = original + 2.0*inc;
            theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu_c, gradMu_c);
            double Y_p2 = entropy();

            conc = original - inc;
            theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu_c, gradMu_c);
            double Y_m1 = entropy();

            conc = original - 2.0*inc;
            theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu_c, gradMu_c);
            double Y_m2 = entropy();


            conc = original;
            theMP.updateCurrentState(time_c, F_c, temp_c, gradT_c, mu_c, gradMu_c);

            // fourth order approximation of the derivative
            Deriv_entropy_c = (-Y_p2 + 8.0*Y_p1 - 8.0*Y_m1 + Y_m2)/(12.0*inc);
        }


        itensor Deriv_c_F;
        for (size_t i=0; i<3; i++)
        {
            for (size_t j=0; j<3; j++)
            {
                const double original = F(i,j);

                F(i,j) = original + inc;
                double Xp1 = fConcentration(F, mu_c, c_c, temp_c);

                F(i,j) = original + 2.0*inc;
                double Xp2 = fConcentration(F, mu_c, c_c, temp_c);

                F(i,j) = original - inc;
                double Xm1 = fConcentration(F, mu_c, c_c, temp_c);

                F(i,j) = original -2.0*inc;
                double Xm2 = fConcentration(F, mu_c, c_c, temp_c);

                Deriv_c_F(i,j) = (-Xp2 + 8.0*Xp1 - 8.0*Xm1 + Xm2)/(12.0*inc);

                F(i,j) = original;
                theMP.updateCurrentState(time_c, F, temp_c, gradT_c, mu_c, gradMu_c);

            }
        }


        num_M = -temp_c*(num_M1 + Deriv_entropy_c*Deriv_c_F);


        const double T_original = temp;
        const double C_original = c_c;

        double J    = F_c.determinant();
        double Js   = 1.0 + O*c_c;
        double Je   = J/Js;
        double phi  = 1.0/Js;
        double lje  = log(Je);

        incr_T = 1e-5*theThermoMechMassMaterial.referenceTemperature();

        temp = T_original + incr_T;
        double P1MU_p1_1 = mu0;
        double P1MU_p1_2 = R * temp * (log(1.0-phi));
        double P1MU_p1_3 = R * temp * (phi);
        double P1MU_p1_4 = R * temp * (CHItheta(temp)*phi*phi);
        double P1MU_p1_5 = 0.5*K*O*lje*lje;
        double P1MU_p1_6 = - O*K*lje;
        double P1MU_p1 = P1MU_p1_1 + P1MU_p1_2 + P1MU_p1_3 + P1MU_p1_4 + P1MU_p1_5 + P1MU_p1_6;

        temp = T_original + 2.0*incr_T;

        double P1MU_p2_1 = mu0;
        double P1MU_p2_2 = R * temp * (log(1.0-phi));
        double P1MU_p2_3 = R * temp * (phi);
        double P1MU_p2_4 = R * temp * (CHItheta(temp)*phi*phi);
        double P1MU_p2_5 = 0.5*K*O*lje*lje;
        double P1MU_p2_6 = - O*K*lje;
        double P1MU_p2 = P1MU_p2_1 + P1MU_p2_2 + P1MU_p2_3 + P1MU_p2_4 + P1MU_p2_5 + P1MU_p2_6;

        temp = T_original - incr_T;

        double P1MU_m1_1 = mu0;
        double P1MU_m1_2 = R * temp * (log(1.0-phi));
        double P1MU_m1_3 = R * temp * (phi);
        double P1MU_m1_4 = R * temp * (CHItheta(temp)*phi*phi);
        double P1MU_m1_5 = 0.5*K*O*lje*lje;
        double P1MU_m1_6 = - O*K*lje;
        double P1MU_m1 = P1MU_m1_1 + P1MU_m1_2 + P1MU_m1_3 + P1MU_m1_4 + P1MU_m1_5 + P1MU_m1_6;

        temp = T_original - 2.0*incr_T;

        double P1MU_m2_1 = mu0;
        double P1MU_m2_2 = R * temp * (log(1.0-phi));
        double P1MU_m2_3 = R * temp * (phi);
        double P1MU_m2_4 = R * temp * (CHItheta(temp)*phi*phi);
        double P1MU_m2_5 = 0.5*K*O*lje*lje;
        double P1MU_m2_6 = - O*K*lje;
        double P1MU_m2 = P1MU_m2_1 + P1MU_m2_2 + P1MU_m2_3 + P1MU_m2_4 + P1MU_m2_5 + P1MU_m2_6;

        temp = T_original;

        // fourth order approximation of the derivative
        nP1MU = (-P1MU_p2 + 8.0*P1MU_p1 - 8.0*P1MU_m1 + P1MU_m2)/(12.0*incr_T);


        incr_C = 1e-5*c_c;
        conce = C_original + incr_C;
        Js   = 1.0 + O*conce;
        Je   = J/Js;
        phi  = 1.0/Js;
        lje  = log(Je);

        double P2MU_p1_1 = mu0;
        double P2MU_p1_2 = R * temp_c * (log(1.0-phi));
        double P2MU_p1_3 = R * temp_c * (phi);
        double P2MU_p1_4 = R * temp_c * (CHItheta(temp_c)*phi*phi);
        double P2MU_p1_5 = 0.5*K*O*lje*lje;
        double P2MU_p1_6 = - O*K*lje;
        double P2MU_p1 = P2MU_p1_1 + P2MU_p1_2 + P2MU_p1_3 + P2MU_p1_4 + P2MU_p1_5 + P2MU_p1_6;

        conce = C_original + 2.0*incr_C;
        Js   = 1.0 + O*conce;
        Je   = J/Js;
        phi  = 1.0/Js;
        lje  = log(Je);
        double P2MU_p2_1 = mu0;
        double P2MU_p2_2 = R * temp_c * (log(1.0-phi));
        double P2MU_p2_3 = R * temp_c * (phi);
        double P2MU_p2_4 = R * temp_c * (CHItheta(temp_c)*phi*phi);
        double P2MU_p2_5 = 0.5*K*O*lje*lje;
        double P2MU_p2_6 = - O*K*lje;
        double P2MU_p2 = P2MU_p2_1 + P2MU_p2_2 + P2MU_p2_3 + P2MU_p2_4 + P2MU_p2_5 + P2MU_p2_6;


        conce = C_original - incr_C;
        Js   = 1.0 + O*conce;
        Je   = J/Js;
        phi  = 1.0/Js;
        lje  = log(Je);
        double P2MU_m1_1 = mu0;
        double P2MU_m1_2 = R * temp_c * (log(1.0-phi));
        double P2MU_m1_3 = R * temp_c * (phi);
        double P2MU_m1_4 = R * temp_c * (CHItheta(temp_c)*phi*phi);
        double P2MU_m1_5 = 0.5*K*O*lje*lje;
        double P2MU_m1_6 = - O*K*lje;
        double P2MU_m1 = P2MU_m1_1 + P2MU_m1_2 + P2MU_m1_3 + P2MU_m1_4 + P2MU_m1_5 + P2MU_m1_6;

        conce = C_original - 2.0*incr_C;
        Js   = 1.0 + O*conce;
        Je   = J/Js;
        phi  = 1.0/Js;
        lje  = log(Je);
        double P2MU_m2_1 = mu0;
        double P2MU_m2_2 = R * temp_c * (log(1.0-phi));
        double P2MU_m2_3 = R * temp_c * (phi);
        double P2MU_m2_4 = R * temp_c * (CHItheta(temp_c)*phi*phi);
        double P2MU_m2_5 = 0.5*K*O*lje*lje;
        double P2MU_m2_6 = - O*K*lje;
        double P2MU_m2 = P2MU_m2_1 + P2MU_m2_2 + P2MU_m2_3 + P2MU_m2_4 + P2MU_m2_5 + P2MU_m2_6;

        conce = C_original;
        Js   = 1.0 + O*conce;
        Je   = J/Js;
        phi  = 1.0/Js;
        lje  = log(Je);

        // fourth order approximation of the derivative
        nP2MU = (-P2MU_p2 + 8.0*P2MU_p1 - 8.0*P2MU_m1 + P2MU_m2)/(12.0*incr_C);

        temp = T_original + incr_T;
        double PtethaC_p1 = fConcentration(F_c, mu_c, c_c, temp);

        temp = T_original + 2.0*incr_T;
        double PtethaC_p2 = fConcentration(F_c, mu_c, c_c, temp);

        temp = T_original - incr_T;
        double PtethaC_m1 = fConcentration(F_c, mu_c, c_c, temp);

        temp = T_original - 2.0*incr_T;
        double PtethaC_m2 = fConcentration(F_c, mu_c, c_c, temp);

        temp = T_original;

        // fourth order approximation of the derivative
        nPtethaC = (-PtethaC_p2 + 8.0*PtethaC_p1 - 8.0*PtethaC_m1 + PtethaC_m2)/(12.0*incr_T);

        num_DiffThercoup = temp_c*nP1MU + temp_c*nP2MU*nPtethaC;
    }



    // compare 1st PK stress with derivative of free energy wrt F
    if (testDE)
    {
        itensor pr_P;
        firstPiolaKirchhoffStress(pr_P);
        itensor errorP = num_P - pr_P;
        isok = (errorP.norm()/pr_P.norm() < 1e-4);

        of << "\n   1. Comparing P with derivative [d Psi / d F].";
        if (isok)
        {
            of << " Test passed.";
        }
        else
        {
            of << "\n ***** Test FAILED *****. Relative error in 1st PK computation: " << errorP.norm()/pr_P.norm();
            of << "\n " << pr_P;
            of << "\n " << num_P;
        }
    }
    else
    {
        of << "\n   2. Comparing 1PK with derivative of Psi";
    }

    // test the consistency of the stress tensors sigma and P
    {
        istensor sigma, S;
        itensor pr_P;
        CauchyStress(sigma);
        firstPiolaKirchhoffStress(pr_P);
        secondPiolaKirchhoffStress(S);

        itensor P1 = F.determinant() * sigma * F.inverse().transpose();
        itensor P2 = F*S;

        itensor errorP1 = P1 - pr_P;
        itensor errorP2 = P2 - pr_P;
        double  error = errorP1.norm() + errorP2.norm();
        isok = error/pr_P.norm() < 1e-4;
        of << "\n   3. Checking consistency of the 1PK, 2PK, and Cauchy stresses.";
        if (isok)
        {
            of << " Test passed.";
        }
        else
        {
            of << "\n ***** Test FAILED *****. Relative error: " << error/pr_P.norm();
            of << "\n P: " << pr_P;
            of << "\n P1: " << P1;
            of << "\n P2: " << P2;
            of << "\n numP: " << num_P;
            of << "\n Cauchy: " << sigma;
        }
        of << std::flush;
    }

    // compare concentration with derivative of GCP wrt mu
    {
        double pr_c = concentration();

        double error = num_c - pr_c;
        isok = (fabs(error)/fabs(pr_c) < 1e-4);
        of << "\n   4. Comparing concentration potential with  -[d GCP / d mu ].";
        if (isok)
        {
            of << " Test passed.";
        }
        else
        {
            of << "\n Test ***** FAILED *****. Relative error " << fabs(error)/fabs(pr_c);
            of << "\n Programmed c: " << pr_c;
            of << "\n Numeric c:    " << num_c;
        }
    }


    // compare material tangent with derivative of the 1st PK w.r.t. F
    if (testDDE)
    {
        // programmed material tangent
        itensor4 pr_A;
        materialTangent(pr_A);

        // relative error
        itensor4 errorA = num_A - pr_A;
        double error = errorA.norm();
        double norm = pr_A.norm();
        isok  = (error/norm < 1e-3);

        of << "\n   5. Comparing material tangent with [d 1PK / d F ].";
        if (isok)
        {
            of << " Test passed.";
        }
        else
        {
            of << "\n Test ***** FAILED. *****";
            of << "\n Relative error in DStress computation:  " <<  error/norm;
            of << "NUMÉRICA numA:  " << num_A;
            of << "ANALÍTICA prA:  " << pr_A;
            of << "DIFERENCIA ENTRE AMBAS:  " << errorA;
        }
    }

    // test tangent as derivative of the stress
    if (testDDE)
    {
        // programmed convected tangent
        itensor4 tg;
        convectedTangent(tg);

        // numeric convected tangent
        itensor4 nTg;
        nTg.setZero();

        // transform num_A to get the convected tangent
        itensor  J  = F.inverse();
        istensor C  = istensor::tensorTransposedTimesTensor(F);
        istensor Ci = C.inverse();
        istensor S;
        secondPiolaKirchhoffStress(S);
        for (unsigned a=0; a<3; a++)
            for (unsigned b=0; b<3; b++)
                for (unsigned c=0; c<3; c++)
                    for (unsigned d=0; d<3; d++)
                    {
                        nTg(c,a,d,b) = - S(a,b)*Ci(c,d);

                        for (unsigned i=0; i<3; i++)
                            for (unsigned j=0; j<3; j++)
                                nTg(c,a,d,b) += J(c,i)*num_A(i,a,j,b)*J(d,j);
                    }

        // relative
        double error = 0.0;
        double norm = 0.0;
        for (unsigned i=0; i<3; i++)
            for (unsigned j=0; j<3; j++)
                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        error += pow(nTg(i,j,k,l)-tg(i,j,k,l),2);
                        norm  += pow(tg(i,j,k,l),2);
                    }

        error = sqrt(error);
        norm = sqrt(norm);
        isok = (error/norm < 1e-3);

        of << "\n   6. Comparing convected tangent with derivative of stress.";
        if (isok)
        {
            of << " Test passed.";
        }
        else
        {
            of << "\n      ***** Test FAILED.*****";
            of << "\n      Relative error in DStress computation: " <<  error/norm;
            of << "     " ;
            of << "NUMERICA nTg" << nTg;
            of << "ANALITICA tg" << tg;
            itensor4 errorTg = nTg - tg;
            of << "DIFERENCIA ENTRE AMBAS" << errorTg;
        }
        of << std::flush;
    }



    // test coupling tensor with d P / d mu
    {
        itensor pr_coupling = materialCouplingTensor();
        itensor error = num_coupling - pr_coupling;
        isok = (error.norm()/pr_coupling.norm() < 2e-3);
        of << "\n   7. Comparing M with [d P / d mu].";
        if (isok)
        {
            of << " Test passed.";
        }
        else
        {
            of << "\n ***** Test FAILED.***** Relative error in coupling tensor computation." << error.norm()/pr_coupling.norm();
            of << "\n " << pr_coupling;
            of << "\n " << num_coupling;
        }
    }


    // compare chemical tangent
    {
        double pr_kmu = chemicalTangent();

        double error = num_kmu - pr_kmu;
        isok = (fabs(error)/fabs(pr_kmu) < 1e-3);
        of << "\n   8. Comparing chemical tangent  -[d c / d mu].";
        if (isok)
        {
            of << " Test passed.";
        }
        else
        {
            of << "\n ***** Test FAILED *****. Relative error kmu computation %e." << fabs(error)/fabs(pr_kmu);
            of << "\n " << pr_kmu;
            of << "\n " << num_kmu;
        }
    }

    // compare numerical and analytical Ms
    {
        itensor pr_M = ThermoMechCouplingTensor();
        itensor error = num_M - pr_M;
        isok = (error.norm()/pr_M.norm() < 2e-3);
        of << "\n   11. Comparing Ms with 0.5 * theta * [d S / d theta].";
        if (isok)
        {
            of << " Test passed.";
            of << "\n " << pr_M;
            of << "\n " << num_M;
            of << "\n " << temp;
        }
        else
        {
            of << "\n ***** Test FAILED *****. Relative error in coupling tensor computation." << error.norm()/pr_M.norm();
            of << "\n " << pr_M;
            of << "\n " << num_M;
            of << "\n " << temp;
        }
    }


    // compare numerical and analytical Diff-Therm parameter
    {
        double pr_DiffThercoup = DiffThermalCouplingParameter();

        double error = num_DiffThercoup - pr_DiffThercoup;
        isok = (fabs(error)/fabs(pr_DiffThercoup) < 2e-3);
        of << "\n   12. Comparing concentration potential with  -[d GCP / d mu ].";
        if (isok)
        {
            of << " Test passed.";
            of << "\n Programmed c: " << pr_DiffThercoup;
            of << "\n Numeric c:    " << num_DiffThercoup;
        }
        else
        {
            of << "\n ***** Test FAILED *****. Relative error " << fabs(error)/fabs(pr_DiffThercoup);
            of << "\n Programmed: " << pr_DiffThercoup;
            of << "\n Numeric:    " << num_DiffThercoup;
        }
    }



    // compare numerical and analytical Heat Capacity
    {
        double pr_HeatCapacity = heatCapacity();

        double error = num_HeatCapac - pr_HeatCapacity;
        isok = (fabs(error)/fabs(pr_HeatCapacity) < 2e-3);
        of << "\n   13. numerical and analytical Heat Capacity";
        if (isok)
        {
            of << " Test passed.";
            of << "\n Programmed c: " << pr_HeatCapacity;
            of << "\n Numeric c:    " << num_HeatCapac;
        }
        else
        {
            of << "\n ***** Test FAILED *****. Relative error " << fabs(error)/fabs(pr_HeatCapacity);
            of << "\n Programmed: " << pr_HeatCapacity;
            of << "\n Numeric:    " << num_HeatCapac;
        }
    }



    // approximation of derivative
    const size_t nnumder = 4;
    const double ndtimes[] = {+1.0, +2.0, -1.0, -2.0};
    const double ndfact[]  = {+8.0, -1.0, -8.0, +1.0};
    const double ndden = 12.0;


    // Compute numerical value of flux j = d Omega / d G
    {
        ivector num_j;
        for (unsigned i=0; i<3; i++)
        {
            double der = 0.0;
            for (unsigned q=0; q<nnumder; q++)
            {
                ivector gmu = gradMu_n;
                gmu[i] = gradMu_n[i] + inc*ndtimes[q];
                theMP.updateCurrentState(tn1, F_n, temp, gradT, mu_n, gmu);
                der += ndfact[q]*diffusionPotential();
                theMP.resetCurrentState();
            }
            der /= inc*ndden;
            num_j(i) = der;
        }

        // compare mass fluxes
        {
            ivector pr_j = materialMassFlux();

            double error = (num_j - pr_j).norm();
            isok = (fabs(error)/pr_j.norm() < 1e-3);
            of << "\n   14. Comparing mass flux.";
            if (isok)
            {
                of << " Test passed.";
            }
            else
            {
                of << "\n ***** Test FAILED. *****";
                of << "\n Relative error mass flux computation:" << fabs(error)/pr_j.norm();
                of << "\n " << pr_j;
                of << "\n " << num_j;
            }
        }
    }


    // Compute numerical value of hessian h = - d j / d G
    {
        istensor num_h;
        for (unsigned i=0; i<3; i++)
        {
            for (unsigned j=0; j<3; j++)
            {
                double der = 0.0;
                for (unsigned q=0; q<nnumder; q++)
                {
                    ivector gmu = gradMu_n;
                    gmu[j] = gradMu_n[j] + inc*ndtimes[q];
                    theMP.updateCurrentState(tn1, F_n, temp, gradT, mu_n, gmu);
                    der += ndfact[q]*materialMassFlux()(i);
                    theMP.resetCurrentState();
                }
                der /= inc*ndden;
                num_h(i,j) = -der;
            }
        }

        // compare mass hessian
        {
            istensor pr_h = diffusionTangent();

            double error = (num_h - pr_h).norm();
            isok = (fabs(error)/pr_h.norm() < 1e-3);
            of << "\n   15. Comparing mass diffusion hessian.";
            if (isok)
            {
                of << " Test passed.";
            }
            else
            {
                of << "\n ***** Test FAILED. *****";
                of << "\n Relative error:" << fabs(error)/pr_h.norm();
                of << "\n " << pr_h;
                of << "\n " << num_h;
            }
        }
    }

    return true;
}




double AOT_ThermoMechMassMP::thermalPotential() const
{
    return -0.5 * theThermoMechMassMaterial._thermalConductivity * gradT_c.squaredNorm();
}




double AOT_ThermoMechMassMP::volumeFraction() const
{
    const double O = theThermoMechMassMaterial._Omega;

    double Js   = 1.0 + O*c_c;
    double phi  = 1.0/Js;
    return phi;
}




double AOT_ThermoMechMassMP::volumetricStiffness() const
{
    itensor4 tg;
    spatialTangent(tg);

    double vs = 0.0;
    for (unsigned i=0; i<3; i++)
        for (unsigned j=0; j<3; j++)
            vs += tg(i,i,j,j);

    return vs/9.0;
}




void AOT_ThermoMechMassMP::updateCurrentState(const double theTime, const itensor& F,
                                              const double temp, const ivector& gradT,
                                              const double mu, const ivector& gradMu)
{
    time_c   = theTime;
    F_c      = F;
    J_c      = F.determinant();
    temp_c   = temp;
    gradT_c  = gradT;
    mu_c     = mu;
    gradMu_c = gradMu;
    c_c = fConcentration(F_c, mu_c, c_n, temp_c);
    
    if (std::isnan(c_c))
    {
        std::cout << "\n nan in concentration";
        c_c = fConcentration(F_c, mu_c, c_c, temp_c);
    }
}
