/****************************************************************************
*
*                                  I R I S
*
*     A general-purpose code for research in computational solid, fluid,
*     and structural mechanics.
*
*     Copyright 2020 Universidad Politecnica de Madrid & IMDEA Materials
*
*     Ignacio Romero (ignacio.romero@upm.es,imdea.org)
*
*     This file is part of IRIS.
*
*     IRIS is a registered software and you must have received this file
*     from the author and only use it under his authorisation.
*
****************************************************************************/

#pragma once
#ifndef _muesli_species_h
#define _muesli_species_h

#include "muesli/material.h"
#include "muesli/tensor.h"
#include <iostream>

namespace muesli
{
    class speciesMP;

    class speciesMaterial : public muesli::material
    {
    public:
                                 speciesMaterial(const std::string& name);
                                 speciesMaterial(const std::string& name,
                                                const materialProperties& cl);

        virtual                  ~speciesMaterial();

        virtual bool             check() const = 0;
        virtual speciesMP*       createMaterialPoint() const = 0;
        double                   getProperty(const propertyName p) const = 0;
        virtual void             print(std::ostream &of=std::cout) const;
        virtual void             setRandom() = 0;
        virtual bool             test(std::ostream& os=std::cout) = 0;
        
    protected:
        friend class             speciesMP;
    };




    class speciesMP : public muesli::materialPoint
    {
    public:
        speciesMP(const speciesMaterial& mat);

        // thermodynamic potentials and kinetic potential
        virtual double              effectiveGrandCanonicalPotential() const = 0;
        virtual double              energyDissipationInStep() const = 0;
        virtual double              freeEnergy() const = 0;
        virtual double              grandCanonicalPotential() const = 0;
        virtual double              kineticPotential() const = 0;

        // chemicalCapacity: derivative of concentration wrt the chemical potential
        virtual double              chemicalCapacity() const = 0;
        
        // chemicalCapacityDerivative: derivative of chemicalCapacity wrt the chemical potential
        virtual double              chemicalCapacityDerivative() const = 0;
                
        virtual double              chemicalPotential() const = 0;

        // concentration as a function of the chemical potential
        virtual double              concentration() const = 0;
        virtual double              concentrationT() const = 0;
        virtual double              convergedConcentration() const = 0;

        // effectiveChemicalCapacity: derivative of effectiveConcentration wrt the chemical potential
        virtual double              effectiveChemicalCapacity() const = 0;
        
        // effectiveChemicalCapacityDerivative: derivative of effectiveChemicalCapacity wrt the chemical potential
        virtual double              effectiveChemicalCapacityDerivative() const = 0;
        
        // effectiveConcentration as a function of the chemical potential
        virtual double              effectiveConcentration() const = 0;

        // mass flux and derivative wrt chemical potential
        virtual void                massFlux(ivector &q) const = 0;
        virtual void                massFluxDerivative(ivector &qprime) const = 0;

        // mobility and derivative wrt chemical potential
        virtual double              mobility() const = 0;
        virtual double              mobilityDerivative() const = 0;

        // contraction is the linearization of the mass flux
        virtual void                contractTangent(const ivector& na,
                                                    const ivector& nb, double& tg) const = 0;

        virtual const speciesMaterial* getMaterial() = 0;
        virtual double              density() const;
        virtual void                setRandom();
        virtual bool                testImplementation(std::ostream& os) const;


        // bookkeeping
        virtual void                commitCurrentState();
        virtual materialState       getConvergedState() const;
        virtual materialState       getCurrentState() const;
        virtual void                resetCurrentState();
        virtual void                updateCurrentState(double theTime,
                                                       double chemicalPot,
                                                       const ivector& gradMu);

    protected:
        const speciesMaterial*      theSpeciesMaterial;
        double                      time_n;     // time for internal variables:
        double                      mu_n;       // chemical potential at tn
        ivector                     gradMu_n;   // grad chemical pot at tn

        double                      time_c;     // time for internal variables:
        double                      mu_c;       // chemical potential at tc
        ivector                     gradMu_c;   // grad chemical pot at tc
    };

    inline double speciesMP::density() const {return theSpeciesMaterial->density();}
}




namespace muesli
{
    class linearDiffusionMP;

    class linearDiffusionMaterial : public muesli::speciesMaterial
    {
    public:
        linearDiffusionMaterial(const std::string& name);
        linearDiffusionMaterial(const std::string& name,
                          const materialProperties& cl);

        virtual                  ~linearDiffusionMaterial();

        virtual bool             check() const;
        virtual speciesMP*       createMaterialPoint() const;
        double                   getProperty(const propertyName p) const;
        virtual void             print(std::ostream &of=std::cout) const;
        virtual void             setRandom();
        virtual bool             test(std::ostream& os=std::cout);

    private:
        friend class             linearDiffusionMP;
        double                   diffusivity, R, t_ref, mu0L, mu0T;
        unsigned                 NL, NT;
        
    protected:
        linearDiffusionMaterial*    theLinearDiffusionMaterial;
    };


    class linearDiffusionMP : public muesli::speciesMP
    {
    public:
        linearDiffusionMP(const linearDiffusionMaterial& mat);

        // thermodynamic potentials and kinetic potential
        virtual double              effectiveGrandCanonicalPotential() const;
        virtual double              energyDissipationInStep() const;
        virtual double              freeEnergy() const;
        virtual double              grandCanonicalPotential() const;
        virtual double              kineticPotential() const;

        // chemicalCapacity: derivative of concentration wrt the chemical potential
        virtual double              chemicalCapacity() const;

        // chemicalCapacityDerivative: derivative of chemicalCapacity wrt the chemical potential
        virtual double              chemicalCapacityDerivative() const;
        
        virtual double              chemicalPotential() const;

        // concentration as a function of the chemical potential
        virtual double              concentration() const;
        virtual double              concentrationT() const;
        virtual double              convergedConcentration() const;
        
        // effectiveChemicalCapacity: derivative of effectiveConcentration wrt the chemical potential
        virtual double              effectiveChemicalCapacity() const;
        
        // effectiveChemicalCapacityDerivative: derivative of effectiveChemicalCapacity wrt the chemical potential
        virtual double              effectiveChemicalCapacityDerivative() const;
        
        // effectiveConcentration as a function of the chemical potential
        virtual double              effectiveConcentration() const;


        // mass flux and derivative wrt chemical potential
        virtual void                massFlux(ivector &q) const;
        virtual void                massFluxDerivative(ivector &qprime) const;

        // mobility and derivative wrt chemical potential
        virtual double              mobility() const;
        virtual double              mobilityDerivative() const;

        // contraction is the linearization of the mass flux
        virtual void                contractTangent(const ivector& na,
                                                    const ivector& nb,
                                                    double& tg) const;
        virtual const linearDiffusionMaterial* getMaterial();
        virtual void                setRandom();

    protected:
        const linearDiffusionMaterial*    theLinearDiffusionMaterial;
    };
}




namespace muesli
{
    class nonLinearDiffusionMP;

    class nonLinearDiffusionMaterial : public muesli::speciesMaterial
    {
    public:
        nonLinearDiffusionMaterial(const std::string& name);
        nonLinearDiffusionMaterial(const std::string& name,
                          const materialProperties& cl);

        virtual                  ~nonLinearDiffusionMaterial();

        virtual bool             check() const;
        virtual speciesMP*       createMaterialPoint() const;
        double                   getProperty(const propertyName p) const;
        virtual void             print(std::ostream &of=std::cout) const;
        virtual void             setRandom();
        virtual bool             test(std::ostream& os=std::cout);

    private:
        friend class             nonLinearDiffusionMP;
        double                   diffusivity, R, t_ref, mu0L, mu0T;
        unsigned                 NL, NT;
        
    protected:
        nonLinearDiffusionMaterial*    theNonLinearDiffusionMaterial;
    };


    class nonLinearDiffusionMP : public muesli::speciesMP
    {
    public:
        nonLinearDiffusionMP(const nonLinearDiffusionMaterial& mat);
        
        // thermodynamic potentials and kinetic potential
        virtual double              effectiveGrandCanonicalPotential() const;
        virtual double              energyDissipationInStep() const;
        virtual double              freeEnergy() const;
        virtual double              grandCanonicalPotential() const;
        virtual double              kineticPotential() const;

        // chemicalCapacity: derivative of concentration wrt the chemical potential
        virtual double              chemicalCapacity() const;

        // chemicalCapacityDerivative: derivative of chemicalCapacity wrt the chemical potential
        virtual double              chemicalCapacityDerivative() const;
        
        virtual double              chemicalPotential() const;
        
        // concentration as a function of the chemical potential
        virtual double              concentration() const;
        virtual double              concentrationT() const;
        virtual double              convergedConcentration() const;

        // effectiveChemicalCapacity: derivative of effectiveConcentration wrt the chemical potential
        virtual double              effectiveChemicalCapacity() const;
        
        // effectiveChemicalCapacityDerivative: derivative of effectiveChemicalCapacity wrt the chemical potential
        virtual double              effectiveChemicalCapacityDerivative() const;
        
        // effectiveConcentration as a function of the chemical potential
        virtual double              effectiveConcentration() const;
        
        // mass flux and derivative wrt chemical potential
        virtual void                massFlux(ivector &q) const;
        virtual void                massFluxDerivative(ivector &qprime) const;

        // mobility and derivative wrt chemical potential
        virtual double              mobility() const;
        virtual double              mobilityDerivative() const;

        // contraction is the linearization of the mass flux
        virtual void                contractTangent(const ivector& na,
                                                    const ivector& nb,
                                                    double& tg) const;
        virtual const nonLinearDiffusionMaterial* getMaterial();
        virtual void                setRandom();

    protected:
        const nonLinearDiffusionMaterial*    theNonLinearDiffusionMaterial;
    };
}




namespace muesli
{
    class dissolvedHydrogenMP;

    class dissolvedHydrogenMaterial : public muesli::speciesMaterial
    {
    public:
        dissolvedHydrogenMaterial(const std::string& name);
        dissolvedHydrogenMaterial(const std::string& name,
                          const materialProperties& cl);

        virtual                  ~dissolvedHydrogenMaterial();

        virtual bool             check() const;
        virtual speciesMP*       createMaterialPoint() const;
        double                   getProperty(const propertyName p) const;
        virtual void             print(std::ostream &of=std::cout) const;
        virtual void             setRandom();
        virtual void             setVariational();
        virtual bool             test(std::ostream& os=std::cout);

    private:
        friend class             dissolvedHydrogenMP;
        double                   diffusivity, R, t_ref, mu0L, mu0T, nl, nt;
        //unsigned                 NL, NT;
        
    protected:
        dissolvedHydrogenMaterial*  theDissolvedHydrogenMaterial;
    };


    class dissolvedHydrogenMP : public muesli::speciesMP
    {
    public:
        dissolvedHydrogenMP(const dissolvedHydrogenMaterial& mat);
        
        // thermodynamic potentials and kinetic potential
        virtual double              convergedFreeEnergy() const;
        virtual double              effectiveGrandCanonicalPotential() const;
        virtual double              energyDissipationInStep() const;
        virtual double              freeEnergy() const;
        virtual double              grandCanonicalPotential() const;
        virtual double              kineticPotential() const;

        // chemicalCapacity: derivative of concentration wrt the chemical potential
        virtual double              chemicalCapacity() const;
        virtual double              chemicalCapacityL() const;
        virtual double              chemicalCapacityT() const;

        
        // chemicalCapacityDerivative: derivative of chemicalCapacity wrt the chemical potential
        virtual double              chemicalCapacityDerivative() const;
        virtual double              chemicalCapacityDerivativeL() const;
        virtual double              chemicalCapacityDerivativeT() const;
        
        virtual double              chemicalPotential() const;
        
        // effectiveChemicalCapacity: effective chemical capacity from lattice + effective chemical capacity from trapping
        virtual double              effectiveChemicalCapacity() const;
        virtual double              effectiveChemicalCapacityT() const;

        // effectiveChemicalCapacityDerivative: derivative of effective chemical capacity from lattice + effective chemical capacity from trapping wrt the chemical potential
        virtual double              effectiveChemicalCapacityDerivative() const;
        virtual double              effectiveChemicalCapacityDerivativeT() const;
        
        // effectiveConcentration as a function of the chemical potential
        virtual double              effectiveConcentration() const;
        virtual double              effectiveConcentrationT() const;
        virtual double              effectiveConvergedConcentration() const;

        // concentration as a function of the chemical potential
        virtual double              concentration() const;
        virtual double              concentrationL() const;
        virtual double              concentrationT() const;
        virtual double              convergedConcentration() const;

        // mass flux and derivative wrt chemical potential
        virtual void                massFlux(ivector &q) const;
        virtual void                massFluxDerivative(ivector &qprime) const;

        // mobility and derivative wrt chemical potential
        virtual double              mobility() const;
        virtual double              mobilityDerivative() const;

        // contraction is the linearization of the mass flux
        virtual void                contractTangent(const ivector& na,
                                                    const ivector& nb,
                                                    double& tg) const;

        virtual void                getStateVariable(stateVariableName name, stateVariable& v) const;
        virtual const dissolvedHydrogenMaterial* getMaterial();
        virtual void                setRandom();

    protected:
        const dissolvedHydrogenMaterial*    theDissolvedHydrogenMaterial;
    };
}


#endif
