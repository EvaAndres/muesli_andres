/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/

#include "species.h"

using namespace muesli;


speciesMaterial::speciesMaterial(const std::string& name):
    material(name)
 {
 }




speciesMaterial::speciesMaterial(const std::string& name,
                                 const materialProperties& cl):
material(name, cl)
{
}




speciesMaterial::~speciesMaterial()
{
}




void speciesMaterial::print(std::ostream &of) const
{
    material::print(of);
}




speciesMP::speciesMP(const speciesMaterial& mat):
    theSpeciesMaterial(&mat),
    time_n(0.0), mu_n(0.0), gradMu_n(0.0, 0.0, 0.0),
    time_c(0.0), mu_c(0.0), gradMu_c(0.0, 0.0, 0.0)
{
}




void speciesMP::commitCurrentState()
{
    time_n   = time_c;
    mu_n     = mu_c;
    gradMu_n = gradMu_c;
}




materialState speciesMP::getConvergedState() const
{
    materialState state;

    state.theTime = time_n;
    state.theDouble.push_back(mu_n);
    state.theVector.push_back(gradMu_n);

    return state;
}




materialState speciesMP::getCurrentState() const
{
    materialState state;

    state.theTime = time_c;
    state.theDouble.push_back(mu_c);
    state.theVector.push_back(gradMu_c);

    return state;
}




void speciesMP::resetCurrentState()
{
    time_c   = time_n;
    mu_c     = mu_n;
    gradMu_c = gradMu_n;
}




void speciesMP::setRandom()
{
    mu_n  = 0.0;
    mu_c  = 0.0;
    gradMu_n.setRandom();
    gradMu_c.setRandom();
}




bool speciesMP::testImplementation(std::ostream& os) const
{
    bool isok = true;
    speciesMP& theMP = const_cast<speciesMP&>(*this);

    // set a random update in the material
    double tn1 = muesli::randomUniform(0.1,1.0);

    ivector gradMu;
    gradMu.setRandom();

    double mu;
    mu = randomUniform(-20000.0, 20000.0);
    
    theMP.updateCurrentState(tn1, mu, gradMu);

    // Derivatives with respect to mu
    if (true)
    {
        // Compute numerical value of concentration chi =  -(d GCP / d mu)
        const double inc = 1.0e-4;
        
        double num_chi;
                
        theMP.updateCurrentState(tn1, mu+inc, gradMu);
        double GCP_p1          = grandCanonicalPotential();
        
        theMP.updateCurrentState(tn1, mu+2.0*inc, gradMu);
        double GCP_p2          = grandCanonicalPotential();

        theMP.updateCurrentState(tn1, mu-inc, gradMu);
        double GCP_m1          = grandCanonicalPotential();

        theMP.updateCurrentState(tn1, mu-2.0*inc, gradMu);
        double GCP_m2          = grandCanonicalPotential();
        
        // fourth order approximation of the derivative
        num_chi = - (-GCP_p2 + 8.0*GCP_p1 - 8.0*GCP_m1 + GCP_m2)/(12.0*inc);

        // programmed concentration
        double pr_chi = concentration();

        // compare concentration with derivative of GCP wrt mu
        // relative error less than 0.01%
        double error = num_chi - pr_chi;
        
        if (fabs(pr_chi) < 1e-3)
        {
            isok = (fabs(error) < 1e-4);
            os << "\n   1. Comparing concentration with - [d GCP / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";

            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in concentration computation %e " << fabs(error)/fabs(pr_chi);
                os << "\n Programmed concentration: " << pr_chi;
                os << "\n Numeric concentration:    " << num_chi;
            }
        }
        
        else
        {
            isok = (fabs(error)/fabs(pr_chi) < 1e-4);
            os << "\n   1. Comparing concentration with - [d GCP / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";

            }
            
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in concentration computation %e " << fabs(error)/fabs(pr_chi);
                os << "\n Programmed concentration: " << pr_chi;
                os << "\n Numeric concentration:    " << num_chi;
            }
        }
    }
    
    if (true)
    {
        // Compute numerical value of effective concentration chi =  -(d effGCP / d mu)
        const double inc = 1.0e-4;
        
        double num_effchi;
                
        theMP.updateCurrentState(tn1, mu+inc, gradMu);
        double effGCP_p1       = effectiveGrandCanonicalPotential();
        
        theMP.updateCurrentState(tn1, mu+2.0*inc, gradMu);
        double effGCP_p2       = effectiveGrandCanonicalPotential();

        theMP.updateCurrentState(tn1, mu-inc, gradMu);
        double effGCP_m1       = effectiveGrandCanonicalPotential();

        theMP.updateCurrentState(tn1, mu-2.0*inc, gradMu);
        double effGCP_m2       = effectiveGrandCanonicalPotential();
        
        // fourth order approximation of the derivative
        num_effchi = - (-effGCP_p2 + 8.0*effGCP_p1 - 8.0*effGCP_m1 + effGCP_m2)/(12.0*inc);

        // programmed concentration
        double pr_effchi = effectiveConcentration();

        // compare concentration with derivative of GCP wrt mu
        // relative error less than 0.01%
        double error = num_effchi - pr_effchi;
        
        if (fabs(pr_effchi) < 1e-3)
        {
            isok = (fabs(error) < 1e-4);
            os << "\n   2. Comparing effective concentration with - [d effGCP / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";

            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in effective concentration computation %e " << fabs(error)/fabs(pr_effchi);
                os << "\n Programmed effective concentration: " << pr_effchi;
                os << "\n Numeric effective concentration:    " << num_effchi;
            }
        }
        
        else
        {
            isok = (fabs(error)/fabs(pr_effchi) < 1e-4);
            os << "\n   2. Comparing effective concentration with - [d effGCP / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";

            }
            
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in effective concentration computation %e " << fabs(error)/fabs(pr_effchi);
                os << "\n Programmed effective concentration: " << pr_effchi;
                os << "\n Numeric effective concentration:    " << num_effchi;
            }
        }
    }
    
    if (true)
    {
        // Compute numerical value of chemical capacity  chemCap = d chi / d mu
        const double inc = 1.0e-3;
        
        double num_chemCap;
                
        theMP.updateCurrentState(tn1, mu+inc, gradMu);
        double chi_p1          = concentration();
        
        theMP.updateCurrentState(tn1, mu+2.0*inc, gradMu);
        double chi_p2          = concentration();

        theMP.updateCurrentState(tn1, mu-inc, gradMu);
        double chi_m1          = concentration();

        theMP.updateCurrentState(tn1, mu-2.0*inc, gradMu);
        double chi_m2          = concentration();
        
        // fourth order approximation of the derivative
        num_chemCap = (-chi_p2 + 8.0*chi_p1 - 8.0*chi_m1 + chi_m2)/(12.0*inc);
        
        // programmed chemicalCapacity
        double pr_chemCap = chemicalCapacity();

        
        // compare chemical capacity with derivative of concentration wrt mu
        // relative error less than 0.01%
        double error = num_chemCap - pr_chemCap;
        
        if (fabs(num_chemCap) == 0.0)
        {
            isok = (fabs(error)< 1e-11);
            os << "\n   3. Comparing chemicalCapacity with  [d chi / d mu ].";

            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in chemicalCapacity computation %e " << fabs(error)/fabs(pr_chemCap);
                os << "\n Programmed chemical capacity: " << pr_chemCap;
                os << "\n Numeric chemical capacity:    " << num_chemCap;
            }
        }
        
        else if (fabs(pr_chemCap) < 5e-8)
        {
            isok = (fabs(num_chemCap)< 1e-7);
            os << "\n   3. Comparing chemicalCapacity with  [d chi / d mu ].";

            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in chemicalCapacity computation %e " << fabs(error)/fabs(pr_chemCap);
                os << "\n Programmed chemical capacity: " << pr_chemCap;
                os << "\n Numeric chemical capacity:    " << num_chemCap;
            }
        }
        
        else
        {
            isok = (fabs(error)/fabs(pr_chemCap) < 5e-4);
            os << "\n   3. Comparing chemicalCapacity with  [d chi / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in chemicalCapacity computation %e " << fabs(error)/fabs(pr_chemCap);
                os << "\n Programmed chemical capacity: " << pr_chemCap;
                os << "\n Numeric chemical capacity:    " << num_chemCap;
            }
        }
    }
    
    if (true)
    {
        // Compute numerical value of chemical capacity derivative chemCapDer = d chemCap / d mu
        const double inc = 1.0e-3;
        
        double num_chemCapDer;
                
        theMP.updateCurrentState(tn1, mu+inc, gradMu);
        double chemCap_p1    = chemicalCapacity();
        
        theMP.updateCurrentState(tn1, mu+2.0*inc, gradMu);
        double chemCap_p2    = chemicalCapacity();

        theMP.updateCurrentState(tn1, mu-inc, gradMu);
        double chemCap_m1    = chemicalCapacity();

        theMP.updateCurrentState(tn1, mu-2.0*inc, gradMu);
        double chemCap_m2    = chemicalCapacity();
        
        // fourth order approximation of the derivative
        num_chemCapDer = (-chemCap_p2 + 8.0*chemCap_p1 - 8.0*chemCap_m1 + chemCap_m2)/(12.0*inc);

        // programmed chemicalCapacityDerivative
        double pr_chemCapDer = chemicalCapacityDerivative();


        // Compare chemical capacity derivative with derivative of chemical capacity wrt mu
        // relative error less than 0.01%
        double error = num_chemCapDer - pr_chemCapDer;
        
        if (fabs(pr_chemCapDer) == 0.0)
        {
            isok = (fabs(error)< 1e-13);
            os << "\n   4. Comparing chemicalCapacityDerivative with [ d chemCap / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in chemicalCapacityDerivative computation %e " << fabs(error)/fabs(pr_chemCapDer);
                os << "\n Programmed chemical capacity derivative: " << pr_chemCapDer;
                os << "\n Numeric chemical capacity derivative:    " << num_chemCapDer;
            }
        }
        
        else
        {
            isok = (fabs(error)/fabs(pr_chemCapDer) < 1e-4);
            os << "\n   4. Comparing chemicalCapacityDerivative with [ d chemCap / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in chemicalCapacityDerivative computation %e " << fabs(error)/fabs(pr_chemCapDer);
                os << "\n Programmed chemical capacity derivative: " << pr_chemCapDer;
                os << "\n Numeric chemical capacity derivative:    " << num_chemCapDer;
            }
        }
    }
    
    if (true)
    {
        // Compute numerical value of effective chemical capacity effChemCap = d effCconcent / d mu
        const double inc = 1.0e-3;
        
        double num_effChemCap;
                
        theMP.updateCurrentState(tn1, mu+inc, gradMu);
        double effConc_p1    = effectiveConcentration();
        
        theMP.updateCurrentState(tn1, mu+2.0*inc, gradMu);
        double effConc_p2    = effectiveConcentration();

        theMP.updateCurrentState(tn1, mu-inc, gradMu);
        double effConc_m1    = effectiveConcentration();

        theMP.updateCurrentState(tn1, mu-2.0*inc, gradMu);
        double effConc_m2    = effectiveConcentration();
        
        // fourth order approximation of the derivative
        num_effChemCap = (-effConc_p2 + 8.0*effConc_p1 - 8.0*effConc_m1 + effConc_m2)/(12.0*inc);

        // programmed effectiveChemicalCapacityt
        double pr_effChemCap = effectiveChemicalCapacity();

        // Compare effective chemical capacity derivative wrt mu with derivative of effective chemical capacity wrt mu
        // relative error less than 0.01%
        double error = num_effChemCap - pr_effChemCap;
        
        if (fabs(num_effChemCap) == 0.0)
        {
            isok = (fabs(error)< 1e-11);
            os << "\n   5. Comparing effectiveChemicalCapacity with [ d effConcent / d mu ].";

            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in chemicalCapacity computation %e " << fabs(error)/fabs(pr_effChemCap);
                os << "\n Programmed effective chemical capacity: " << pr_effChemCap;
                os << "\n Numeric effective chemical capacity:    " << num_effChemCap;
            }
        }
        
        else if (fabs(pr_effChemCap) < 5e-8)
        {
            isok = (fabs(num_effChemCap)< 1e-7);
            os << "\n   5. Comparing effectiveChemicalCapacity with [ d effConcent / d mu ].";

            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in effConcentDerivativeWrtMu computation %e " << fabs(error)/fabs(pr_effChemCap);
                os << "\n Programmed effective concentration derivative wrt mu: " << pr_effChemCap;
                os << "\n Numeric effective concentration derivative wrt mu:    " << num_effChemCap;
            }
        }
        
        else
        {
            isok = (fabs(error)/fabs(pr_effChemCap) < 5e-4);
            os << "\n   5. Comparing effectiveChemicalCapacity with  [d effConcent / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in chemicalCapacity computation %e " << fabs(error)/fabs(pr_effChemCap);
                os << "\n Programmed effective chemical capacity: " << pr_effChemCap;
                os << "\n Numeric effective chemical capacity:    " << num_effChemCap;
            }
        }
    }
    
    if (true)
    {
        // Compute numerical value of effective chemical capacity derivative effChemCapDer = d effChemCap / d mu
        const double inc = 1.0e-4;
        
        double num_effChemCapDer;
                
        theMP.updateCurrentState(tn1, mu+inc, gradMu);
        double effChemCap_p1    = effectiveChemicalCapacity();
        
        theMP.updateCurrentState(tn1, mu+2.0*inc, gradMu);
        double effChemCap_p2    = effectiveChemicalCapacity();

        theMP.updateCurrentState(tn1, mu-inc, gradMu);
        double effChemCap_m1    = effectiveChemicalCapacity();

        theMP.updateCurrentState(tn1, mu-2.0*inc, gradMu);
        double effChemCap_m2    = effectiveChemicalCapacity();
        
        // fourth order approximation of the derivative
        num_effChemCapDer = (-effChemCap_p2 + 8.0*effChemCap_p1 - 8.0*effChemCap_m1 + effChemCap_m2)/(12.0*inc);

        // programmed effectiveChemicalCapacityt
        double pr_effChemCapDer = effectiveChemicalCapacityDerivative();

        // Compare effective chemical capacity derivative wrt mu with derivative of effective chemical capacity wrt mu
        // relative error less than 0.01%
        double error = num_effChemCapDer - pr_effChemCapDer;
        
        isok = (fabs(error)/fabs(pr_effChemCapDer) < 1e-4);
        os << "\n   6. Comparing effectiveChemicalCapacityDerivative with [ d effChemCap / d mu ].";
            
        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in effChemicalCapacityDerivativeWrtMu computation %e " << fabs(error)/fabs(pr_effChemCapDer);
            os << "\n Programmed chemical capacity derivative wrt mu: " << pr_effChemCapDer;
            os << "\n Numeric chemical capacity derivative wrt mu:    " << num_effChemCapDer;
        }
    }
    
    
    
    if (true)
    {
        // Compute numerical value of mobility derivative mobilityDerivative = -(d mobility / d mu)
        const double inc = 1.0e-3;
        
        double num_mDer;
        
        theMP.updateCurrentState(tn1, mu+inc, gradMu);
        double mDer_p1    = mobility();
        
        theMP.updateCurrentState(tn1, mu+2.0*inc, gradMu);
        double mDer_p2    = mobility();

        theMP.updateCurrentState(tn1, mu-inc, gradMu);
        double mDer_m1    = mobility();

        theMP.updateCurrentState(tn1, mu-2.0*inc, gradMu);
        double mDer_m2    = mobility();
        
        // fourth order approximation of the derivative
        num_mDer = (-mDer_p2 + 8.0*mDer_p1 - 8.0*mDer_m1 + mDer_m2)/(12.0*inc);

        // programmed mobilityDerivative
        double pr_mDer = mobilityDerivative();
        
        // compare mobilityDerivative with derivative of mobility wrt mu
        // relative error less than 0.01%
        double error = num_mDer - pr_mDer;
        
        if (fabs(pr_mDer) == 0.0)
        {
            isok = (fabs(error)< 2e-13);
            os << "\n   7. Comparing mobilityDerivative with [d m / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in mobilityDerivative computation %e " << fabs(error)/fabs(pr_mDer);
                os << "\n Programmed mobility derivative: " << pr_mDer;
                os << "\n Numeric mobility derivative:    " << num_mDer;
            }
        }
        
        else if (pr_mDer < 1e-9)
        {
            isok = (fabs(error) < 1e-10);
            os << "\n   7. Comparing mobilityDerivative with [d m / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in mobilityDerivative computation %e " << fabs(error)/fabs(pr_mDer);
                os << "\n Programmed mobility derivative: " << pr_mDer;
                os << "\n Numeric mobility derivative:    " << num_mDer;
            }
        }
            
        else
        {
            isok = (fabs(error)/fabs(pr_mDer) < 1e-4);
            os << "\n   7. Comparing mobilityDerivative with [d m / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in mobilityDerivative computation %e " << fabs(error)/fabs(pr_mDer);
                os << "\n Programmed mobility derivative: " << pr_mDer;
                os << "\n Numeric mobility derivative:    " << num_mDer;
            }
        }
    }
    
    if (true)
    {
        // Compute numerical value of mass flux derivative wrt mu massFluxDerivative = d massFlux / d mu
        const double inc = 1.0e-3;
        
        ivector num_jDer;
        
        theMP.updateCurrentState(tn1, mu+inc, gradMu);
        ivector jDer_p1; massFlux(jDer_p1);
        
        theMP.updateCurrentState(tn1, mu+2.0*inc, gradMu);
        ivector jDer_p2; massFlux(jDer_p2);

        theMP.updateCurrentState(tn1, mu-inc, gradMu);
        ivector jDer_m1; massFlux(jDer_m1);

        theMP.updateCurrentState(tn1, mu-2.0*inc, gradMu);
        ivector jDer_m2; massFlux(jDer_m2);
        
        // fourth order approximation of the derivative
        num_jDer = (-jDer_p2 + 8.0*jDer_p1 - 8.0*jDer_m1 + jDer_m2)/(12.0*inc);

        // programmed massFluxDerivative
        ivector pr_jDer;
        massFluxDerivative(pr_jDer);
        
        // compare mobilityDerivative with derivative of mobility wrt mu
        // relative error less than 0.01%
        ivector error = num_jDer - pr_jDer;
    
        if (pr_jDer.norm() == 0.0)
        {
            isok = (error.norm() < 5e-13);
            os << "\n   8. Comparing massFluxDerivative with [d massFlux / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in massFluxDerivative computation %e " << error.norm()/pr_jDer.norm();
                os << "\n Programmed mass flux derivative: " << pr_jDer;
                os << "\n Numeric mass flux derivative:    " << num_jDer;
            }
        }
        
        else if (pr_jDer.norm() < 1e-9)
        {
            isok = (error.norm() < 1e-10);
            os << "\n   8. Comparing massFluxDerivative with [d massFlux / d mu ].";

            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in massFluxDerivative computation %e " << error.norm()/pr_jDer.norm();
                os << "\n Programmed mass flux derivative: " << pr_jDer;
                os << "\n Numeric mass flux derivative:    " << num_jDer;
            }
        }
        
        else
        {
            isok = (error.norm()/pr_jDer.norm() < 1e-4);
            os << "\n   8. Comparing massFluxDerivative with [d massFlux / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in massFluxDerivative computation %e " << error.norm()/pr_jDer.norm();
                os << "\n Programmed mass flux derivative: " << pr_jDer;
                os << "\n Numeric mass flux derivative:    " << num_jDer;
            }
        }
    }
        
    // Derivatives with respect to gradMu
    if (true)
    {
        // Compute numerical value of mass flux massFlux = -(d kineticPotential / d gradMu)
        ivector num_massFlux;
        num_massFlux.setZero();
        const double inc = 1.0e-4;

        for (size_t i=0; i<3; i++)
        {
            double original = gradMu(i);
            
            gradMu(i) = original + inc;
            theMP.updateCurrentState(tn1, mu, gradMu);
            double Omega_p1 = kineticPotential();

            gradMu(i) = original + 2.0*inc;
            theMP.updateCurrentState(tn1, mu, gradMu);
            double Omega_p2 = kineticPotential();

            gradMu(i) = original - inc;
            theMP.updateCurrentState(tn1, mu, gradMu);
            theMP.updateCurrentState(tn1, mu, gradMu);
            double Omega_m1 = kineticPotential();

            gradMu(i) = original - 2.0*inc;
            theMP.updateCurrentState(tn1, mu, gradMu);
            double Omega_m2 = kineticPotential();

            // fourth order approximation of the derivative
            double der = (-Omega_p2 + 8.0*Omega_p1 - 8.0*Omega_m1 + Omega_m2)/(12.0*inc);
            num_massFlux(i) = der;

            gradMu(i) = original;
            
        }
        
        // programmed mass flux
        ivector pr_massFlux; massFlux(pr_massFlux);

        // compare mass flux with derivative of kinetic potential wrt gradMu

        // relative error less than 0.01%
        ivector error = num_massFlux - pr_massFlux;
        isok = (error.norm()/pr_massFlux.norm() < 1e-3);
        os << "\n   9. Comparing massFlux with [d Omega / d gradMu ].";

        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in massFlux computation %e." <<  error.norm()/pr_massFlux.norm();
            os << "\n Programmed mass flux: " << pr_massFlux;
            os << "\n Numeric mass flux:    " << num_massFlux;
        }
    }
    
    if (true)
    {
        // Compute numerical value (approximation of derivative) of mobility  m = -(d j / d gradMu)
        const double inc = 1.0e-3;

        const size_t nnumder = 4.0;
        const double ndtimes[] = {+1.0, +2.0, -1.0, -2.0};
        const double ndfact[]  = {+8.0, -1.0, -8.0, +1.0};
        const double ndden = 12.0;
        istensor num_m;
        for (unsigned i=0; i<3; i++)
        {
            for (unsigned j=0; j<3; j++)
            {
                double der = 0.0;
                for (unsigned q=0; q<nnumder; q++)
                {
                    ivector gmu = gradMu_n;
                    gmu[j] = gradMu_n[j] + inc*ndtimes[q];
                    theMP.updateCurrentState(tn1, mu_n, gmu);
                    
                    ivector massFlux_aux; massFlux(massFlux_aux);
                    der += ndfact[q]*massFlux_aux(i);
                    theMP.resetCurrentState();
                }
                der /= inc*ndden;
                num_m(i,j) = -der;
            }
        }

        // compare programmed mobility with numerical derivative of mass flux wrt gradMu
        istensor pr_m = mobility()*istensor::identity();

        double error = (num_m - pr_m).norm();
        isok = (fabs(error)/pr_m.norm() < 1e-4);
            
        os << "\n   10. Comparing mobility with [d massFlux / d gradMu ].";
        if (isok)
        {
            os << " Test passed.";
        }
        
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in mobility computation %e." <<  fabs(error)/pr_m.norm();
            os << "\n Programmed mobility: " << pr_m;
            os << "\n Numeric mobility:    " << num_m.norm();
        }
    }
    
    return true;
}




void speciesMP::updateCurrentState(double theTime,
                                   double chemicalPot,
                                   const ivector& gradMu)
{
    time_c   = theTime;
    mu_c     = chemicalPot;
    gradMu_c = gradMu;
}




linearDiffusionMaterial::linearDiffusionMaterial(const std::string& name):
    speciesMaterial(name),
    diffusivity(0.0), R(0.0),
    t_ref(273.0),
    mu0L(0.0), mu0T(0.0),
    NL(1), NT(0)
{
}




linearDiffusionMaterial::linearDiffusionMaterial(const std::string& name,
                                     const materialProperties& cl):
    speciesMaterial(name, cl),
    diffusivity(0.0), R(0.0), t_ref(273.0), mu0L(0.0), mu0T(0.0), NL(1), NT(0)
{
    muesli::assignValue(cl, "diffusivity", diffusivity);
    muesli::assignValue(cl, "r", R);
    muesli::assignValue(cl, "tref", t_ref);
    muesli::assignValue(cl, "mu0", mu0L);
    double dN;
    muesli::assignValue(cl, "n", dN);  NL = (unsigned) dN;
}




linearDiffusionMaterial::~linearDiffusionMaterial()
{
}




bool linearDiffusionMaterial::check() const
{
    bool ret = true;

    if (diffusivity <= 0.0)
    {
        ret = false;
        std::cout << "Error in linearDiffusionMaterial. Non-positive diffusivity";
    }

    return ret;
}




speciesMP* linearDiffusionMaterial::createMaterialPoint() const
{
    speciesMP *mp = new linearDiffusionMP(*this);
    return mp;
}




double linearDiffusionMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;
    
    switch (p)
    {
        case PR_DIFFUSIVITY:    ret = diffusivity; break;
        case PR_MU0L:           ret = mu0L;        break;
        case PR_N:              ret = NL;          break;
        case PR_Rgas:           ret = R;           break;
        case PR_TREF:           ret = t_ref;       break;
            
        default:
            std::cout << "\n Error in linearDiffusionMaterial. Property not defined";
    }
    return ret;
}




void linearDiffusionMaterial::print(std::ostream &of) const
{
    of  << "\n   Material for linear diffusion of species on a host material"
        << "\n   Diffusivity       : " << diffusivity
        << "\n   Gas constant      : " << R
        << "\n   Ref. chemical pot.: " << mu0L
        << "\n   Atoms/mol         : " << NL
        << "\n   Ref. temperature  : " << t_ref;

    speciesMaterial::print(of);
}




void linearDiffusionMaterial::setRandom()
{
    diffusivity = randomUniform(1.0, 10.0);
    R           = randomUniform(1.0, 10.0);
    t_ref       = randomUniform(100.0, 400.0);
    mu0L        = randomUniform(250.0, 300.0);
    mu0T        = 0.0;
    NL          = (unsigned) randomUniform(250.0, 300.0);
    NT          = (unsigned) 0;
}




bool linearDiffusionMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

    speciesMP* p = this->createMaterialPoint();
    isok = p->testImplementation(os);
    delete p;
    return isok;
}




linearDiffusionMP::linearDiffusionMP(const linearDiffusionMaterial& mat):
    speciesMP(mat),
    theLinearDiffusionMaterial(&mat)
{
}




double linearDiffusionMP::chemicalCapacity() const
{
    //linear model, then the chemical capacity (d chi / d mu ) is constant
    double N      = theLinearDiffusionMaterial->NL;
    double R      = theLinearDiffusionMaterial->R;
    double Theta0 = theLinearDiffusionMaterial->t_ref;
    
    return N/(4.0*R*Theta0);
}




double linearDiffusionMP::chemicalCapacityDerivative() const
{
    return 0.0;
}




double linearDiffusionMP::chemicalPotential() const
{
    return mu_c;
}




double linearDiffusionMP::concentration() const
{
    //since the model is linear, chi (mu) = chi (mu0) + c0 * ( mu - mu0)
    //thus its derivative (the chemical capacity) is constant
    double mu0    = theLinearDiffusionMaterial->mu0L;
    double R      = theLinearDiffusionMaterial->R;
    double N      = theLinearDiffusionMaterial->NL;
    double Theta0 = theLinearDiffusionMaterial->t_ref;
    
    double c0 = N /(4.0*R*Theta0);
    return 0.5*N + c0 * (mu_c-mu0);
}




double linearDiffusionMP::concentrationT() const
{
    return 0.0;
}




void linearDiffusionMP::contractTangent(const ivector& na,
                                  const ivector& nb,
                                  double& tg) const
{
    tg = na.dot(nb)*mobility();
}




double linearDiffusionMP::convergedConcentration() const
{
    //since the model is linear, chi (mu) = chi (mu0) + c0 * ( mu - mu0)
    //thus its derivative (the chemical capacity) is constant
    double mu0    = theLinearDiffusionMaterial->mu0L;
    double R      = theLinearDiffusionMaterial->R;
    double N      = theLinearDiffusionMaterial->NL;
    double Theta0 = theLinearDiffusionMaterial->t_ref;
    
    double c0 = N /(4.0*R*Theta0);
    return 0.5*N + c0 * (mu_n-mu0);
}




double linearDiffusionMP::effectiveChemicalCapacity() const
{
    //linear model, then the chemical capacity (d chi / d mu ) is constant
    return chemicalCapacity();
}




double linearDiffusionMP::effectiveChemicalCapacityDerivative() const
{
    return 0.0;
}




double linearDiffusionMP::effectiveConcentration() const
{
    //since the model is linear, chi (mu) = chi (mu0) + c0 * ( mu - mu0)
    return concentration();
}




double linearDiffusionMP::effectiveGrandCanonicalPotential() const
{
    return freeEnergy() - mu_c*effectiveConcentration();
}




double linearDiffusionMP::energyDissipationInStep() const
{
    ivector j; massFlux(j);
    ivector g = -gradMu_c;
    
    return j.dot(g);
}




double linearDiffusionMP::freeEnergy() const
{
    double mu0    = theLinearDiffusionMaterial->mu0L;
    double N      = theLinearDiffusionMaterial->NL;
    double R      = theLinearDiffusionMaterial->R;
    double Theta0 = theLinearDiffusionMaterial->t_ref;
    
    double c0   = N/(4.0*R*Theta0);
    double chi0 = 0.5*N;
    double chi  = concentration();

    return mu0*chi + chi*(0.5*chi-chi0)/c0;
}




const linearDiffusionMaterial* linearDiffusionMP::getMaterial()
{
    return theLinearDiffusionMaterial;
}




double linearDiffusionMP::grandCanonicalPotential() const
{
    double chi  = concentration();

    return freeEnergy() - mu_c*chi;
}




double linearDiffusionMP::kineticPotential() const
{
    ivector g = -gradMu_c;

    return -0.5*mobility()*g.squaredNorm();
}




void linearDiffusionMP::massFlux(ivector &q) const
{
    double m = mobility();
    q        = -m*gradMu_c;
}




void linearDiffusionMP::massFluxDerivative(ivector &qprime) const
{
    double mprime = mobilityDerivative();
    qprime        = -mprime*gradMu_c;
}




double linearDiffusionMP::mobility() const
{
    double D      = theLinearDiffusionMaterial->diffusivity;
    double N      = theLinearDiffusionMaterial->NL;
    double R      = theLinearDiffusionMaterial->R;
    double Theta0 = theLinearDiffusionMaterial->t_ref;
    
    return D*N/(2.0*R*Theta0);
}




double linearDiffusionMP::mobilityDerivative() const
{
    return 0.0;
}




void linearDiffusionMP::setRandom()
{
    speciesMP::setRandom();
}




nonLinearDiffusionMaterial::nonLinearDiffusionMaterial(const std::string& name):
    speciesMaterial(name),
    diffusivity(0.0), R(0.0),
    t_ref(273.0),
    mu0L(0.0), mu0T(0.0),
    NL(1), NT(0)
{
}




nonLinearDiffusionMaterial::nonLinearDiffusionMaterial(const std::string& name,
                                     const materialProperties& cl):
    speciesMaterial(name, cl),
    diffusivity(0.0), R(0.0),
    t_ref(273.0),
    mu0L(0.0), mu0T(0.0),
    NL(1), NT(0)
{
    muesli::assignValue(cl, "diffusivity", diffusivity);
    muesli::assignValue(cl, "r", R);
    muesli::assignValue(cl, "tref", t_ref);
    muesli::assignValue(cl, "mu0", mu0L);
    double dN;
    muesli::assignValue(cl, "n", dN);  NL = (unsigned) dN;
}




nonLinearDiffusionMaterial::~nonLinearDiffusionMaterial()
{
}




bool nonLinearDiffusionMaterial::check() const
{
    bool ret = true;

    if (diffusivity <= 0.0)
    {
        ret = false;
        std::cout << "Error in nonLinearDiffusionMaterial. Non-positive diffusivity";
    }

    return ret;
}




speciesMP* nonLinearDiffusionMaterial::createMaterialPoint() const
{
    speciesMP *mp = new nonLinearDiffusionMP(*this);
    return mp;
}




double nonLinearDiffusionMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;
    
    switch (p)
    {
        case PR_DIFFUSIVITY:    ret = diffusivity; break;
        case PR_MU0L:           ret = mu0L;        break;
        case PR_N:              ret = NL;          break;
        case PR_Rgas:           ret = R;           break;
        case PR_TREF:           ret = t_ref;       break;
            
        default:
            std::cout << "\n Error in nonLinearDiffusionMaterial. Property not defined";
    }
    return ret;
}




void nonLinearDiffusionMaterial::print(std::ostream &of) const
{
    of  << "\n   Material for non linear diffusion of species on a host material"
        << "\n   Diffusivity       : " << diffusivity
        << "\n   Gas constant      : " << R
        << "\n   Ref. chemical pot.: " << mu0L
        << "\n   Atoms/mol         : " << NL
        << "\n   Ref. temperature  : " << t_ref;

    speciesMaterial::print(of);
}




void nonLinearDiffusionMaterial::setRandom()
{
    diffusivity = randomUniform(1.0, 10.0);
    R           = randomUniform(1.0, 10.0);
    t_ref        = randomUniform(100.0, 400.0);
    mu0L        = randomUniform(250.0, 300.0);
    mu0T        = 0.0;
    NL          = (unsigned) randomUniform(250.0, 300.0);
    NT          = (unsigned) 0;
}




bool nonLinearDiffusionMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

    speciesMP* p = this->createMaterialPoint();
    isok = p->testImplementation(os);
    delete p;
    return isok;
}




nonLinearDiffusionMP::nonLinearDiffusionMP(const nonLinearDiffusionMaterial& mat):
    speciesMP(mat),
    theNonLinearDiffusionMaterial(&mat)
{
}




double nonLinearDiffusionMP::chemicalCapacity() const
{
    double mu0    = theNonLinearDiffusionMaterial->mu0L;
    double N      = theNonLinearDiffusionMaterial->NL;
    double R      = theNonLinearDiffusionMaterial->R;
    double Theta0 = theNonLinearDiffusionMaterial->t_ref;

    double E  = exp((mu_c-mu0)/(R*Theta0));
    double Ep = E/(R*Theta0);
  
    return N*Ep/((1.0+E)*(1.0+E));
}




double nonLinearDiffusionMP::chemicalCapacityDerivative() const
{
    double mu0    = theNonLinearDiffusionMaterial->mu0L;
    double N      = theNonLinearDiffusionMaterial->NL;
    double R      = theNonLinearDiffusionMaterial->R;
    double Theta0 = theNonLinearDiffusionMaterial->t_ref;

    double E   = exp((mu_c-mu0)/(R*Theta0));
    double Ep  = E/(R*Theta0);
    double Epp = E/((R*Theta0)*(R*Theta0));
    
    return N*Epp/((1.0+E)*(1.0+E)) - 2.0*N*Ep*Ep/((1.0+E)*(1.0+E)*(1.0+E));
}




double nonLinearDiffusionMP::chemicalPotential() const
{
    return mu_c;
}




double nonLinearDiffusionMP::concentration() const
{
    double mu0    = theNonLinearDiffusionMaterial->mu0L;
    double N      = theNonLinearDiffusionMaterial->NL;
    double R      = theNonLinearDiffusionMaterial->R;
    double Theta0 = theNonLinearDiffusionMaterial->t_ref;

    double E = exp((mu_c-mu0)/(R*Theta0));
    
    return N * E/(1.0+E);
}




double nonLinearDiffusionMP::concentrationT() const
{
    return 0.0;
}




void nonLinearDiffusionMP::contractTangent(const ivector& na,
                                  const ivector& nb,
                                  double& tg) const
{
    tg = na.dot(nb)*mobility();
}




double nonLinearDiffusionMP::convergedConcentration() const
{
    double mu0    = theNonLinearDiffusionMaterial->mu0L;
    double N      = theNonLinearDiffusionMaterial->NL;
    double R      = theNonLinearDiffusionMaterial->R;
    double Theta0 = theNonLinearDiffusionMaterial->t_ref;

    double E = exp((mu_n-mu0)/(R*Theta0));
    
    return N * E/(1.0+E);
}




double nonLinearDiffusionMP::effectiveChemicalCapacity() const
{
    return chemicalCapacity();
}




double nonLinearDiffusionMP::effectiveChemicalCapacityDerivative() const
{
    return chemicalCapacityDerivative();
}




double nonLinearDiffusionMP::effectiveConcentration() const
{
    return concentration();
}




double nonLinearDiffusionMP::effectiveGrandCanonicalPotential() const
{
    return freeEnergy() - mu_c*effectiveConcentration();
}




double nonLinearDiffusionMP::energyDissipationInStep() const
{
    ivector j; massFlux(j);
    ivector g = -gradMu_c;
    
    return j.dot(g);
}




double nonLinearDiffusionMP::freeEnergy() const
{
    double chi    = concentration();
    double mu0    = theNonLinearDiffusionMaterial->mu0L;
    double N      = theNonLinearDiffusionMaterial->NL;
    double R      = theNonLinearDiffusionMaterial->R;
    double Theta0 = theNonLinearDiffusionMaterial->t_ref;
    
    double Theta  = chi/N;
    
    double phi    = 0.0;
    phi = mu0*chi + R*Theta0*N * ( Theta*log(Theta) + (1.0 - Theta)*log(1.0 - Theta) );
    
    return phi;
}




const nonLinearDiffusionMaterial* nonLinearDiffusionMP::getMaterial()
{
    return theNonLinearDiffusionMaterial;
}




double nonLinearDiffusionMP::grandCanonicalPotential() const
{
    double chi  = concentration();

    return freeEnergy() - mu_c*chi;
}




double nonLinearDiffusionMP::kineticPotential() const
{
    ivector g = -gradMu_c;
    
    return -0.5*mobility()*g.squaredNorm();
}




void nonLinearDiffusionMP::massFlux(ivector &q) const
{
    double m = mobility();
    q        = -m*gradMu_c;
}




void nonLinearDiffusionMP::massFluxDerivative(ivector &qprime) const
{
    double mprime = mobilityDerivative();
    qprime        = -mprime*gradMu_c;
}




double nonLinearDiffusionMP::mobility() const
{
    double D      = theNonLinearDiffusionMaterial->diffusivity;
    double R      = theNonLinearDiffusionMaterial->R;
    double Theta0 = theNonLinearDiffusionMaterial->t_ref;
    
    return D*concentration()/(R*Theta0);
}




double nonLinearDiffusionMP::mobilityDerivative() const
{
    double D      = theNonLinearDiffusionMaterial->diffusivity;
    double R      = theNonLinearDiffusionMaterial->R;
    double Theta0 = theNonLinearDiffusionMaterial->t_ref;

    return D*chemicalCapacity()/(R*Theta0);
}




void nonLinearDiffusionMP::setRandom()
{
    speciesMP::setRandom();
}




dissolvedHydrogenMaterial::dissolvedHydrogenMaterial(const std::string& name):
    speciesMaterial(name),
    diffusivity(0.0), R(8.31),
    t_ref(273.0),
    mu0L(0.0), mu0T(0.0),
    nl(1), nt(1)
{
}




dissolvedHydrogenMaterial::dissolvedHydrogenMaterial(const std::string& name, const materialProperties& cl):
    speciesMaterial(name, cl),
    diffusivity(0.0), R(0.0),
    t_ref(273.0),
    mu0L(0.0), mu0T(0.0),
    nl(1), nt(1)
{
    muesli::assignValue(cl, "diffusivity", diffusivity);
    muesli::assignValue(cl, "r", R);
    muesli::assignValue(cl, "tref", t_ref);
    muesli::assignValue(cl, "mu0l", mu0L);
    muesli::assignValue(cl, "mu0t", mu0T);
    muesli::assignValue(cl, "nl", nl);
    muesli::assignValue(cl, "nt", nt);
}




dissolvedHydrogenMaterial::~dissolvedHydrogenMaterial()
{
}




bool dissolvedHydrogenMaterial::check() const
{
    bool ret = true;

    if (diffusivity <= 0.0)
    {
        ret = false;
        std::cout << "Error in dissolvedHydrogenMaterial. Non-positive diffusivity";
    }

    return ret;
}




speciesMP* dissolvedHydrogenMaterial::createMaterialPoint() const
{
    speciesMP *mp = new dissolvedHydrogenMP(*this);
    return mp;
}




double dissolvedHydrogenMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;
    
    switch (p)
    {
        case PR_DIFFUSIVITY:    ret = diffusivity; break;
        case PR_Rgas:           ret = R;           break;
        case PR_TREF:           ret = t_ref;       break;
        case PR_MU0L:           ret = mu0L;        break;
        case PR_MU0T:           ret = mu0T;        break;
        case PR_NL:             ret = nl;          break;
        case PR_NT:             ret = nt;          break;

        default:
            std::cout << "\n Error in dissolvedHydrogenMaterial. Property not defined";
    }
    return ret;
}




void dissolvedHydrogenMaterial::print(std::ostream &of) const
{
    of  << "\n   Material for hydrogen dissolved in a host metal"
        << "\n   Variational formulation  : " << isVariational()
        << "\n   Diffusivity              : " << diffusivity
        << "\n   Gas constant             : " << R
        << "\n   Ref. chemical pot. for L : " << mu0L
        << "\n   Ref. chemical pot. for T : " << mu0T
        << "\n   Atoms/mol for L          : " << nl
        << "\n   Atoms/mol for T          : " << nt
        << "\n   Reference temperature    : " << t_ref;

    speciesMaterial::print(of);
}




void dissolvedHydrogenMaterial::setRandom()
{
    diffusivity = randomUniform(0.5, 1.5)*1e-8;
    R           = randomUniform(1.0, 10.0);
    t_ref       = randomUniform(100.0, 400.0);
    mu0L        = randomUniform(25.0, 35.0);
    mu0T        = randomUniform(25.0, 35.0)*(-1.0);
    nl          = (unsigned) randomUniform(1.0, 10.0)*1e5;
    nt          = (unsigned) randomUniform(1.0, 10.0)*1e5;
}




void dissolvedHydrogenMaterial::setVariational()
{
    material::setVariational();
}




bool dissolvedHydrogenMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

    speciesMP* p = this->createMaterialPoint();
    isok = p->testImplementation(os);
    delete p;
    return isok;
}




dissolvedHydrogenMP::dissolvedHydrogenMP(const dissolvedHydrogenMaterial& mat):
    speciesMP(mat),
    theDissolvedHydrogenMaterial(&mat)
{
}




// chemical capacity (lattice + trapped) from chiT hat
double dissolvedHydrogenMP::chemicalCapacity() const
{
    double cL          = chemicalCapacityL();
    double cT          = chemicalCapacityT();
    
    return cL + cT;
}




// chemical capacity (lattice)
double dissolvedHydrogenMP::chemicalCapacityL() const
{
    double R           = theDissolvedHydrogenMaterial->R;
    double Theta0      = theDissolvedHydrogenMaterial->t_ref;
    
    double mu0L        = theDissolvedHydrogenMaterial->mu0L;
    double NL          = theDissolvedHydrogenMaterial->nl;
    double EL          = exp( (mu_c-mu0L)/(R*Theta0));
    double ELp         = EL/(R*Theta0);
        
    return NL*ELp/((1.0 + EL)*(1.0+EL));
}




// chemical capacity (trapped) chiT hat
double dissolvedHydrogenMP::chemicalCapacityT() const
{
    double R           = theDissolvedHydrogenMaterial->R;
    double Theta0      = theDissolvedHydrogenMaterial->t_ref;
    
    double mu0T        = theDissolvedHydrogenMaterial->mu0T;
    double NT          = theDissolvedHydrogenMaterial->nt;
    double ET          = exp( (mu_c-mu0T)/(R*Theta0));
    double ETp         = ET/(R*Theta0);
        
    return NT*ETp/((1.0 + ET)*(1.0+ET));
}




// chemical capacity derived wrt mu
double dissolvedHydrogenMP::chemicalCapacityDerivative() const
{
    double dcL_dmuL    = chemicalCapacityDerivativeL();
    double dcT_dmuT    = chemicalCapacityDerivativeT();

    return dcL_dmuL + dcT_dmuT;
}




// chemical capacity L derived wrt mu
double dissolvedHydrogenMP::chemicalCapacityDerivativeL() const
{
    double R           = theDissolvedHydrogenMaterial->R;
    double Theta0      = theDissolvedHydrogenMaterial->t_ref;

    double mu0L        = theDissolvedHydrogenMaterial->mu0L;
    double NL          = theDissolvedHydrogenMaterial->nl;
    double EL          = exp( (mu_c-mu0L)/(R*Theta0));
    double ELp         = EL/(R*Theta0);
    double ELpp        = EL/((R*Theta0)*(R*Theta0));
    
    double dcL_dmuL    = NL*ELpp/((1.0 + EL)*(1.0+EL)) - 2.0*NL*ELp*ELp/((1.0+EL)*(1.0+EL)*(1.0+EL));

    return dcL_dmuL;
}




// chemical capacity T derived wrt mu
double dissolvedHydrogenMP::chemicalCapacityDerivativeT() const
{
    double R           = theDissolvedHydrogenMaterial->R;
    double Theta0      = theDissolvedHydrogenMaterial->t_ref;

    double mu0T        = theDissolvedHydrogenMaterial->mu0T;
    double NT          = theDissolvedHydrogenMaterial->nt;
    double ET          = exp( (mu_c-mu0T)/(R*Theta0));
    double ETp         = ET/(R*Theta0);
    double ETpp        = ET/((R*Theta0)*(R*Theta0));
    
    double dcT_dmuT    = NT*ETpp/((1.0 + ET)*(1.0+ET)) - 2.0*NT*ETp*ETp/((1.0+ET)*(1.0+ET)*(1.0+ET));

    return dcT_dmuT;
}




double dissolvedHydrogenMP::chemicalPotential() const
{
    return mu_c;
}




// disolved lattice hydrogen + trapped hydrogen
double dissolvedHydrogenMP::concentration() const
{
    double chiL = concentrationL();
    double chiT = concentrationT();
        
    return chiL + chiT;
}




// disolved lattice hydrogen
double dissolvedHydrogenMP::concentrationL() const
{
    double mu0L   = theDissolvedHydrogenMaterial->mu0L;
    double NL     = theDissolvedHydrogenMaterial->nl;
    double R      = theDissolvedHydrogenMaterial->R;
    double Theta0 = theDissolvedHydrogenMaterial->t_ref;

    double EL = exp( (mu_c-mu0L)/(R*Theta0));
        
    return NL * EL/(1.0+EL);
}




// disolved trapped hydrogen chi hat
double dissolvedHydrogenMP::concentrationT() const
{
    double mu0T   = theDissolvedHydrogenMaterial->mu0T;
    double NT     = theDissolvedHydrogenMaterial->nt;
    double R      = theDissolvedHydrogenMaterial->R;
    double Theta0 = theDissolvedHydrogenMaterial->t_ref;
        
    double ET = exp( (mu_c-mu0T)/(R*Theta0));
    
    return NT * ET/(1.0+ET);
}




void dissolvedHydrogenMP::contractTangent(const ivector& na,
                                  const ivector& nb,
                                  double& tg) const
{
    tg = na.dot(nb)*mobility();
}




// converged concentration of total hydrogen chi hat
double dissolvedHydrogenMP::convergedConcentration() const
{
    double mu0L   = theDissolvedHydrogenMaterial->mu0L;
    double mu0T   = theDissolvedHydrogenMaterial->mu0T;
    double NL     = theDissolvedHydrogenMaterial->nl;
    double NT     = theDissolvedHydrogenMaterial->nt;
    double R      = theDissolvedHydrogenMaterial->R;
    double Theta0 = theDissolvedHydrogenMaterial->t_ref;

    double EL = exp( (mu_n-mu0L)/(R*Theta0));
    double ET = exp( (mu_n-mu0T)/(R*Theta0));
        
    return NL * EL/(1.0+EL) + NT * ET/(1.0+ET);
}




double dissolvedHydrogenMP::convergedFreeEnergy() const
{
    double R      = theDissolvedHydrogenMaterial->R;
    double Theta0 = theDissolvedHydrogenMaterial->t_ref;
    
    
    
    double mu0L   = theDissolvedHydrogenMaterial->mu0L;
    double mu0T   = theDissolvedHydrogenMaterial->mu0T;
    double NL     = theDissolvedHydrogenMaterial->nl;
    double NT     = theDissolvedHydrogenMaterial->nt;
    
    double ET     = exp( (mu_n-mu0T)/(R*Theta0));
    double chiT   = NT * ET/(1.0+ET);
    double chiL   = convergedConcentration() - chiT;
    double ThetaL = chiL/NL;
    double ThetaT = chiT/NT;
    
    double phiL = mu0L*chiL + R*Theta0*NL * ( ThetaL*log(ThetaL) + (1.0 - ThetaL)*log(1.0 - ThetaL) );
    double phiT = mu0T*chiT + R*Theta0*NT * ( ThetaT*log(ThetaT) + (1.0 - ThetaT)*log(1.0 - ThetaT) );
    
    return phiL + phiT;
}




//TOTAL effective chemical capacity (cL + cTtilde) from chiT tilde
double dissolvedHydrogenMP::effectiveChemicalCapacity() const
{
    double cL    = chemicalCapacityL();
    double cTeff = effectiveChemicalCapacityT();
    
    return cL + cTeff;
}




// effective trapped chemical capacity
double dissolvedHydrogenMP::effectiveChemicalCapacityT() const
{
    double chiL        = concentrationL();
    double cL          = chemicalCapacityL();
    double mu0L        = theDissolvedHydrogenMaterial->mu0L;
    double mu0T        = theDissolvedHydrogenMaterial->mu0T;
    double NL          = theDissolvedHydrogenMaterial->nl;
    double NT          = theDissolvedHydrogenMaterial->nt;
    double R           = theDissolvedHydrogenMaterial->R;
    double Theta0      = theDissolvedHydrogenMaterial->t_ref;
       
    double Wb          = mu0L - mu0T;
    double KT          = exp(Wb/(R*Theta0));
    double dchiT_dchiL = (KT*NT*NL)/((NL - chiL + KT*chiL)*(NL - chiL + KT*chiL));
        
    return dchiT_dchiL * cL;
}




// TOTAL effective chemical capacity derived wrt mu from chiT tilde
double dissolvedHydrogenMP::effectiveChemicalCapacityDerivative() const
{
    double R           = theDissolvedHydrogenMaterial->R;
    double Theta0      = theDissolvedHydrogenMaterial->t_ref;

    double mu0L        = theDissolvedHydrogenMaterial->mu0L;
    double NL          = theDissolvedHydrogenMaterial->nl;
    double EL          = exp( (mu_c-mu0L)/(R*Theta0));
    double ELp         = EL/(R*Theta0);
    double cL          = NL*ELp/((1.0 + EL)*(1.0+EL));
    
    double chiL        = concentrationL();
    double mu0T        = theDissolvedHydrogenMaterial->mu0T;
    double NT          = theDissolvedHydrogenMaterial->nt;
    double Wb          = mu0L - mu0T;
    double KT          = exp(Wb/(R*Theta0));
    double dchiT_dchiL = (KT*NT*NL)/((NL - chiL + KT*chiL)*(NL - chiL + KT*chiL));
    
    double ELpp        = EL/((R*Theta0)*(R*Theta0));
    double dcL_dmuL    = NL*ELpp/((1.0 + EL)*(1.0+EL)) - 2.0*NL*ELp*ELp/((1.0+EL)*(1.0+EL)*(1.0+EL));
    
    double sum1 = (1.0 + dchiT_dchiL) * dcL_dmuL;
    double sum2 = 2.0 * cL * cL * KT*NT*NL*(1.0 - KT) / ((NL - chiL + KT*chiL)*(NL - chiL + KT*chiL)*(NL - chiL + KT*chiL));
    
    return sum1 + sum2;
}




// TOTAL effective chemical capacity derived wrt mu from chiT tilde
double dissolvedHydrogenMP::effectiveChemicalCapacityDerivativeT() const
{
    double R           = theDissolvedHydrogenMaterial->R;
    double Theta0      = theDissolvedHydrogenMaterial->t_ref;
    double mu0L        = theDissolvedHydrogenMaterial->mu0L;
    double mu0T        = theDissolvedHydrogenMaterial->mu0T;
    double NL          = theDissolvedHydrogenMaterial->nl;
    double NT          = theDissolvedHydrogenMaterial->nt;
    double Wb          = mu0L - mu0T;
    double KT          = exp(Wb/(R*Theta0));
    double chiL        = concentrationL();
    double cL          = chemicalCapacityL();
    double dcL_dmuL    = chemicalCapacityDerivativeL();
    
    double dchiT_dchiL = (KT*NT*NL)/((NL - chiL + KT*chiL)*(NL - chiL + KT*chiL));
    
    double sum1 = dchiT_dchiL * dcL_dmuL;
    double sum2 = 2.0 * cL * cL * KT*NT*NL*(1.0 - KT) / ((NL - chiL + KT*chiL)*(NL - chiL + KT*chiL)*(NL - chiL + KT*chiL));
    
    return sum1 + sum2;
}




// lattice hydrogen + effective disolved trapped hydrogen: chiT tilde + chiL
double dissolvedHydrogenMP::effectiveConcentration() const
{
    double chiL        = concentrationL();
    double chiTtilde   = effectiveConcentrationT();
        
    return chiL + chiTtilde;
}




// effective disolved trapped hydrogen: chiT tilde
double dissolvedHydrogenMP::effectiveConcentrationT() const
{
    double chiL        = concentrationL();
    double mu0L        = theDissolvedHydrogenMaterial->mu0L;
    double mu0T        = theDissolvedHydrogenMaterial->mu0T;
    double NL          = theDissolvedHydrogenMaterial->nl;
    double NT          = theDissolvedHydrogenMaterial->nt;
    double R           = theDissolvedHydrogenMaterial->R;
    double Theta0      = theDissolvedHydrogenMaterial->t_ref;
    
    double Wb          = mu0L - mu0T;
    double KT          = exp(Wb/(R*Theta0));
            
    return KT * chiL * NT /(NL - chiL + KT*chiL);
}




// converged concentration lattice hydrogen + effective disolved trapped hydrogen: chiT tilde _n + chiL_n
double dissolvedHydrogenMP::effectiveConvergedConcentration() const
{
    double mu0L         = theDissolvedHydrogenMaterial->mu0L;
    double mu0T         = theDissolvedHydrogenMaterial->mu0T;
    double NL           = theDissolvedHydrogenMaterial->nl;
    double NT           = theDissolvedHydrogenMaterial->nt;
    double R            = theDissolvedHydrogenMaterial->R;
    double Theta0       = theDissolvedHydrogenMaterial->t_ref;
    
    double Wb           = mu0L - mu0T;
    double KT           = exp(Wb/(R*Theta0));
    double EL           = exp( (mu_n-mu0L)/(R*Theta0));

    double chi_n        = NL * EL/(1.0+EL);                
    double chit_tilde_n = KT * chi_n * NT /(NL - chi_n + KT*chi_n);
        
    return chi_n + chit_tilde_n;
}

 


//concentration defined with effectiveConcentrationT (from chiT tilde)
double dissolvedHydrogenMP::effectiveGrandCanonicalPotential() const
{
    return freeEnergy() - mu_c*effectiveConcentration();
}




double dissolvedHydrogenMP::energyDissipationInStep() const
{
    ivector j; massFlux(j);
    ivector g = -gradMu_c;
    
    return j.dot(g);
}




double dissolvedHydrogenMP::freeEnergy() const
{
    double mu0L   = theDissolvedHydrogenMaterial->mu0L;
    double mu0T   = theDissolvedHydrogenMaterial->mu0T;
    double NL     = theDissolvedHydrogenMaterial->nl;
    double NT     = theDissolvedHydrogenMaterial->nt;
    double R      = theDissolvedHydrogenMaterial->R;
    double Theta0 = theDissolvedHydrogenMaterial->t_ref;
    double ThetaL = concentrationL()/NL;
    double ThetaT = concentrationT()/NT;

    double phi = 0.0;
    phi = mu0L*concentrationL() + mu0T*concentrationT();
    phi += R*Theta0*NL * ( ThetaL*log(ThetaL) + (1.0 - ThetaL)*log(1.0 - ThetaL) ) + R*Theta0*NT * ( ThetaT*log(ThetaT) + (1.0 - ThetaT)*log(1.0 - ThetaT) );

    return phi;
}




const dissolvedHydrogenMaterial* dissolvedHydrogenMP::getMaterial()
{
    return theDissolvedHydrogenMaterial;
}




void dissolvedHydrogenMP::getStateVariable(stateVariableName name, stateVariable& v) const
{
    if (name == SV_TRAPPED_H)
        v.d = concentrationT();
    else if (name == SV_LATTICE_H)
        v.d = concentrationL();
}




double dissolvedHydrogenMP::grandCanonicalPotential() const
{
    return freeEnergy() - mu_c*concentration();
}




double dissolvedHydrogenMP::kineticPotential() const
{
    ivector g = -gradMu_c;
    
    return -0.5*mobility()*g.squaredNorm();
}




void dissolvedHydrogenMP::massFlux(ivector &q) const
{
    double m = mobility();
    q = -m*gradMu_c;
}




void dissolvedHydrogenMP::massFluxDerivative(ivector &qprime) const
{
    double mprime = mobilityDerivative();
    qprime = -mprime*gradMu_c;
}




double dissolvedHydrogenMP::mobility() const
{
    double D = theDissolvedHydrogenMaterial->diffusivity;
    double R = theDissolvedHydrogenMaterial->R;
    double Theta0 = theDissolvedHydrogenMaterial->t_ref;

    return D*concentrationL()/(R*Theta0);
}




double dissolvedHydrogenMP::mobilityDerivative() const
{
    double D = theDissolvedHydrogenMaterial->diffusivity;
    double R      = theDissolvedHydrogenMaterial->R;
    double Theta0 = theDissolvedHydrogenMaterial->t_ref;

    double cL   = chemicalCapacityL();
    
    return D*cL/(R*Theta0);
}




void dissolvedHydrogenMP::setRandom()
{
    speciesMP::setRandom();
}
