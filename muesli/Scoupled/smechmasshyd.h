/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/



#pragma once
#ifndef __muesli_smechmasshyd_h
#define __muesli_smechmasshyd_h

#include "muesli/material.h"


namespace muesli
{
    class smallStrainMaterial;
    class smallStrainMP;
    class sMechMassHydMP;

    class sMechMassHydMaterial : public muesli::material
    {
    public:
                                sMechMassHydMaterial(const std::string& name,
                                                  const materialProperties& cl);
        virtual                 ~sMechMassHydMaterial();

        virtual bool            check() const;
        virtual sMechMassHydMP* createMaterialPoint() const;
        virtual double          density() const;
        virtual double          getProperty(const propertyName p) const;
        virtual void            print(std::ostream &of=std::cout) const;
        double                  kt() const;
        double                  referenceChemicalPotential() const;
        double                  referenceTemperature() const;
        virtual void            setRandom();
        virtual void            setVariational();
        virtual bool            test(std::ostream& of=std::cout);
        virtual double          waveVelocity() const;

    private:
        smallStrainMaterial     *theSSMaterial;
        double                  diffusivity, massExpansion, mu0l, mu0t, muref, nl, nt, nt0, nt1, nt2, nta, R, tref;

        friend class            sMechMassHydMP;
    };

    inline double sMechMassHydMaterial::referenceTemperature() const {return tref;}



    class sMechMassHydMP : public muesli::materialPoint
    {
    public:
                                    sMechMassHydMP(const sMechMassHydMaterial &m);
        virtual                     ~sMechMassHydMP();
        virtual void                setRandom();
        virtual bool                freeEnergyCheck() const;
        bool                        testImplementation(std::ostream& of=std::cout) const;

        // info
        virtual istensor            getConvergedPlasticStrain() const;
        virtual materialState       getConvergedState() const;
        virtual istensor            getCurrentPlasticStrain() const;
        virtual materialState       getCurrentState() const;
        istensor&                   getCurrentStrain();
        const istensor&             getCurrentStrain() const;
        
        // energies per unit volume
        virtual double              deviatoricEnergy() const;
        virtual double              diffusionPotential() const;
        virtual double              effectiveGrandCanonicalPotential() const;
        virtual double              effectiveStoredEnergy() const;
        virtual double              energyDissipationInStep() const;
        virtual double              effectiveFreeEnergy() const;
        virtual double              freeEnergy() const;
        virtual double              grandCanonicalPotential() const;
        virtual double              internalEnergy() const;
        virtual double              kineticPotential() const;
        virtual double              volumetricEnergy() const;
        
        // dissipations
        virtual double              diffusiveDissipationInStep() const;
        virtual double              mechanicalDissipationInStep() const;
        virtual itensor             mechanicalDissipationInStepDEps() const;
        virtual double              mechanicalDissipationInStepDMu() const;


        // gradients
        // chemicalCapacity: derivative of concentration wrt the chemical potential
        virtual double              chemicalCapacity() const;
        virtual double              chemicalCapacityl() const;
        virtual double              chemicalCapacityt() const;
        virtual double              effectiveChemicalCapacity() const;
        virtual double              effectiveChemicalCapacityt() const;
        // chemicalCapacityDerivative: derivative of chemicalCapacity wrt the chemical potential
        virtual istensor            chemicalCapacityDerivativeWrtEps() const;
        virtual double              chemicalCapacityDerivativeWrtMu() const;
        virtual istensor            chemicalCapacitylDerivativeWrtEps() const;
        virtual double              chemicalCapacitylDerivativeWrtMu() const;
        virtual istensor            chemicalCapacitytDerivativeWrtEps() const;
        virtual double              chemicalCapacitytDerivativeWrtMu() const;
        virtual istensor            effectiveChemicalCapacityDerivativeWrtEps() const;
        virtual double              effectiveChemicalCapacityDerivativeWrtMu() const;
        virtual double              chemicalPotential() const;
        // concentration as a function of the chemical potential
        virtual double              concentration() const;
        virtual istensor            concentrationDerivativeWrtEps() const;
        virtual double              concentrationl() const;
        virtual istensor            concentrationlDerivativeWrtEps() const;
        virtual double              concentrationt() const;
        virtual istensor            concentrationtDerivativeWrtEps() const;
        virtual double              effectiveConcentration() const;
        virtual istensor            effectiveConcentrationDerivativeWrtEps() const;
        virtual double              effectiveConcentrationt() const;
        virtual istensor            effectiveConcentrationtDerivativeWrtEps() const;
        virtual double              effectiveConvergedConcentration() const;
        virtual double              effectiveConvergedConcentrationt() const;
        virtual double              thetal() const;
        virtual double              thetat() const;
        virtual double              effectiveThetat() const;

        virtual double              convergedConcentration() const;
        virtual double              convergedConcentrationl() const;
        virtual double              convergedConcentrationt() const;
        
        // mass flux and derivative wrt chemical potential
        virtual void                massFlux(ivector &q) const;
        virtual void                massFluxDerivativeWrtEps(ivector &qprime) const;
        virtual void                massFluxDerivativeWrtMu(ivector &qprime) const;

        // mobility and derivative wrt chemical potential
        virtual double              mobility() const;
        virtual istensor            mobilityDerivativeWrtEps() const;
        virtual double              mobilityDerivativeWrtMu() const;
        
        //stress functions
        virtual void                deviatoricStress(istensor& s) const;
        virtual void                effectiveStress(istensor& sigma) const;
        virtual void                effectiveStressChemicalTensor(istensor& M) const;
        virtual void                effectiveStressTangentTensor(itensor4& c) const;
        virtual double              pressure() const;
        virtual void                stress(istensor& sigma) const;
        virtual void                stressChemicalTensor(istensor& M) const;
        virtual void                stressTangentTensor(itensor4& c) const;

        virtual double              plasticSlip() const;

        // tangents
        // linearization of the mass flux
        virtual double              contractWithMobility(const ivector &v1, const ivector &v2) const;
        virtual void                contractWithTangent(const ivector &v1,
                                                    const ivector &v2,
                                                    itensor &T) const;
        virtual void                effectiveContractWithTangent(const ivector &v1,
                                                    const ivector &v2,
                                                    itensor &T) const;
        virtual void                contractWithDeviatoricTangent(const ivector &v1,
                                                              const ivector &v2,
                                                              itensor &T) const;
        virtual void                dissipationTangent(itensor4& D) const;
        virtual double              volumetricStiffness() const;


        // bookkeeping
        virtual void                commitCurrentState();
        virtual void                resetCurrentState();
        virtual void                updateCurrentState(const double t,                                             const istensor& strain,
                                                   const double mu, const ivector& gradMu);

        double                      density() const;
        const sMechMassHydMaterial& parentMaterial() const;


    private:
        double                       mu_n, mu_c;
        ivector                      gradMu_n, gradMu_c;
        smallStrainMP*               theSSMP;
        const sMechMassHydMaterial&  theMechMassHydMaterial;
        double                       nt() const;
    };

    inline double sMechMassHydMP::density() const  {return theMechMassHydMaterial.density();}
    inline const sMechMassHydMaterial& sMechMassHydMP::parentMaterial() const {return theMechMassHydMaterial;}
}

#endif

