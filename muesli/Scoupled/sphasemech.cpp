/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/

// This is work in progress
// this should be used as:
/*
material, label = 23, type = phasemech, subtype = elastic, young = 210e9, \
  poisson = 0.3, ell = 0.01, gc = 200.0, k0 = 1e-5
*/

#include <stdio.h>
#include "sphasemech.h"
#include "muesli/Smallstrain/smallstrainlib.h"

using namespace muesli;


sPhaseMechMaterial::sPhaseMechMaterial(const std::string& name,
                                     const materialProperties& cl)
:
material(name, cl),
theSSMaterial(0),
tref(273.0),
ell(1.0),
gc(1.0),
k0(1e-6)
{
    if       (cl.find("subtype elastic") != cl.end())      theSSMaterial = new elasticIsotropicMaterial(name, cl);
    else if  (cl.find("subtype plastic") != cl.end())      theSSMaterial = new splasticMaterial(name, cl);
    else if  (cl.find("subtype viscoelastic") != cl.end()) theSSMaterial = new viscoelasticMaterial(name, cl);
    else if  (cl.find("subtype viscoplastic") != cl.end()) theSSMaterial = new viscoplasticMaterial(name, cl);

    muesli::assignValue(cl, "ell", ell);
    muesli::assignValue(cl, "gc",  gc);
    muesli::assignValue(cl, "k0",  k0);
}




sPhaseMechMaterial::~sPhaseMechMaterial()
{
    if (theSSMaterial != 0) delete(theSSMaterial);
}




bool sPhaseMechMaterial::check() const
{
    return theSSMaterial->check();
}




sPhaseMechMP* sPhaseMechMaterial::createMaterialPoint() const
{
    return new sPhaseMechMP(*this);
}




double sPhaseMechMaterial::density() const
{
    return theSSMaterial->density();
}




double sPhaseMechMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    switch (p)
    {
        case PR_CHAR_LENGTH:    ret = ell; break;
        case PR_G_CRITICAL:     ret = gc; break;
        default:
            ret = theSSMaterial->getProperty(p);
    }
    return ret;
}




void sPhaseMechMaterial::print(std::ostream &of) const
{
    of  << "\n Small strain mechanical material with phase field fracture."
        << "\n Surrogate mechanical model: ";
    theSSMaterial->print(of);

    of  << "\n Characteristic length:                 " << ell
        << "\n Critical energy release rate Gc:       " << gc
        << "\n Residual stiffness k0:                 " << k0;
}




void sPhaseMechMaterial::setRandom()
{
    int mattype = discreteUniform(1, 1);
    std::string name = "surrogate small strain material";
    materialProperties mp;

    if (mattype == 0)
        theSSMaterial = new elasticIsotropicMaterial(name, mp);

    else if (mattype == 1)
        theSSMaterial = new splasticMaterial(name, mp);

    else if (mattype == 2)
        theSSMaterial = new viscoelasticMaterial(name, mp);

    else if (mattype == 3)
        theSSMaterial = new viscoplasticMaterial(name, mp);

    else if (mattype == 4)
        theSSMaterial = new GTN_Material(name, mp);

    theSSMaterial->setRandom();
    ell = randomUniform(1.0, 2.0);
    gc  = randomUniform(100.0, 400.0);
}




bool sPhaseMechMaterial::test(std::ostream& of)
{
    bool isok = true;
    setRandom();

    sPhaseMechMP* p = this->createMaterialPoint();

    isok = p->testImplementation(of);
    delete p;

    return isok;
}




double sPhaseMechMaterial::waveVelocity() const
{
    return theSSMaterial->waveVelocity();
}




sPhaseMechMP::sPhaseMechMP(const sPhaseMechMaterial &m)
:
  phase_n(0.0), phase_c(0.0),
  theSSMP(nullptr),
  thePhaseMechMaterial(m)
{
    theSSMP = m.theSSMaterial->createMaterialPoint();
    gradPh_n.setZero();
    gradPh_c.setZero();
}




sPhaseMechMP::~sPhaseMechMP()
{
    if (theSSMP != 0) delete (theSSMP);
}




void sPhaseMechMP::commitCurrentState()
{
    phase_n = phase_c;
    gradPh_n = gradPh_c;
    theSSMP->commitCurrentState();
}




void sPhaseMechMP::contractWithTangent(const ivector &v1, const ivector &v2, itensor &T) const
{
    theSSMP->contractWithTangent(v1, v2, T);
}




void sPhaseMechMP::contractWithDeviatoricTangent(const ivector &v1, const ivector &v2, itensor &T) const
{
    theSSMP->contractWithDeviatoricTangent(v1, v2, T);
}




double sPhaseMechMP::deviatoricEnergy() const
{
    return theSSMP->deviatoricEnergy();
}




void sPhaseMechMP::deviatoricStress(istensor& s) const
{
    theSSMP->deviatoricStress(s);
}




double sPhaseMechMP::energyDissipationInStep() const
{
    double dissMech = theSSMP->energyDissipationInStep();
    return dissMech;
}




void sPhaseMechMP::dissipationTangent(itensor4& D) const
{
    theSSMP -> dissipationTangent(D);
}




double sPhaseMechMP::effectiveStoredEnergy() const
{
    double  dt_Psistar = energyDissipationInStep();
    double  fmec       = theSSMP->storedEnergy();

    return fmec + dt_Psistar;
}




double sPhaseMechMP::freeEnergy() const
{
  istensor eps_plus;
  istensor eps_minus;
  double psi_plus=0.0;
  double psi_minus=0.0;


  double fmec = ((1.0-phase_c)*(1.0-phase_c) + thePhaseMechMaterial.k0) * psi_plus + psi_minus;
  return fmec;
}




istensor sPhaseMechMP::getConvergedPlasticStrain() const
{
    return theSSMP->getConvergedPlasticStrain();
}




materialState sPhaseMechMP::getConvergedState() const
{
    materialState mat = theSSMP->getConvergedState();
    mat.theDouble.push_back(phase_n);
    mat.theVector.push_back(gradPh_n);

    return mat;
}




istensor sPhaseMechMP::getCurrentPlasticStrain() const
{
    return theSSMP->getCurrentPlasticStrain();
}




materialState sPhaseMechMP::getCurrentState() const
{
    // first store the state of the ssmp
    materialState mat = theSSMP->getCurrentState();

    mat.theDouble.push_back(phase_c);
    mat.theVector.push_back(gradPh_c);

    return mat;
}




istensor& sPhaseMechMP::getCurrentStrain()
{
    return theSSMP->getCurrentState().theStensor[0];
}




double sPhaseMechMP::plasticSlip() const
{
    return theSSMP->plasticSlip();
}




double sPhaseMechMP::pressure() const
{
    return theSSMP->pressure();
}




void sPhaseMechMP::resetCurrentState()
{
    phase_c = phase_n;
    gradPh_c = gradPh_n;
    theSSMP->resetCurrentState();
}




void sPhaseMechMP::setRandom()
{
    phase_c = randomUniform(0.1, 1.0);
    gradPh_c.setRandom();
    theSSMP->setRandom();
}




void sPhaseMechMP::stress(istensor& sigma) const
{
    theSSMP->stress(sigma);
}




void sPhaseMechMP::tangentElasticities(itensor4& c) const
{
    theSSMP->tangentTensor(c);
}




double& sPhaseMechMP::phaseField()
{
    return phase_c;
}




const double& sPhaseMechMP::phaseField() const
{
  return phase_c;
}




double sPhaseMechMP::recoverableFreeEnergy() const
{
  return freeEnergy();
}





bool sPhaseMechMP::testImplementation(std::ostream& os) const
{
    bool isok = true;

    // set a random update in the material
    istensor eps;
    eps.setZero();

    double temp;
    temp = randomUniform(0.7, 1.3) * thePhaseMechMaterial.tref;

    ivector gradPh;
    gradPh.setRandom();

    sPhaseMechMP* ssmp = const_cast<sPhaseMechMP*>(this);
    ssmp->updateCurrentState(0.0, eps, temp, gradPh);
    ssmp->commitCurrentState();

    double tn1 = muesli::randomUniform(0.1,1.0);
    eps.setRandom();
    sPhaseMechMP& theMP = const_cast<sPhaseMechMP&>(*this);
    theMP.updateCurrentState(tn1, eps, temp, gradPh);

    // programmed tangent of elasticities
    itensor4 tg;
    tangentElasticities(tg);

    // (1)  compare DEnergy with the derivative of Energy
    if (true)
    {
        // programmed stress
        istensor sigma;
        this->stress(sigma);

        // numerical differentiation stress
        istensor numSigma;
        numSigma.setZero();
        const double   inc = 1.0e-3;

        for (size_t i=0; i<3; i++)
        {
            for (size_t j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, phase_c, gradPh_c);
                double Wp1 = effectiveStoredEnergy();

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, phase_c, gradPh_c);
                double Wp2 = effectiveStoredEnergy();

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, phase_c, gradPh_c);
                double Wm1 = effectiveStoredEnergy();

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, phase_c, gradPh_c);
                double Wm2 = effectiveStoredEnergy();

                // fourth order approximation of the derivative
                double der = (-Wp2 + 8.0*Wp1 - 8.0*Wm1 + Wm2)/(12.0*inc);
                numSigma(i,j) = der;
                if (i != j) numSigma(i,j) *= 0.5;

                numSigma(j,i) = numSigma(i,j);

                eps(i,j) = eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, phase_c, gradPh_c);
            }
        }

        // relative error less than 0.01%
        istensor error = numSigma - sigma;
        isok = (error.norm()/sigma.norm() < 1e-4);

        os << "\n   1. Comparing stress with DWeff.";
        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n      Test failed.";
            os << "\n      Relative error in DWeff computation: " <<  error.norm()/sigma.norm();
            os << "\n      Stress:\n" << sigma;
            os << "\n      Numeric stress:\n" << numSigma;
        }
    }


    // (2) compare tensor c with derivative of stress
    if (true)
    {
        // numeric C
        itensor4 nC;
        nC.setZero();

        // numerical differentiation sigma
        istensor dsigma, sigmap1, sigmap2, sigmam1, sigmam2;
        double   inc = 1.0e-3;

        for (unsigned i=0; i<3; i++)
        {
            for (unsigned j=i; j<3; j++)
            {
                double original = eps(i,j);

                eps(i,j) = eps(j,i) = original + inc;
                theMP.updateCurrentState(tn1, eps, phase_c, gradPh_c);
                stress(sigmap1);

                eps(i,j) = eps(j,i) = original + 2.0*inc;
                theMP.updateCurrentState(tn1, eps, phase_c, gradPh_c);
                stress(sigmap2);

                eps(i,j) = eps(j,i) = original - inc;
                theMP.updateCurrentState(tn1, eps, phase_c, gradPh_c);
                stress(sigmam1);

                eps(i,j) = eps(j,i) = original - 2.0*inc;
                theMP.updateCurrentState(tn1, eps, phase_c, gradPh_c);
                stress(sigmam2);

                // fourth order approximation of the derivative
                dsigma = (-sigmap2 + 8.0*sigmap1 - 8.0*sigmam1 + sigmam2)/(12.0*inc);

                if (i != j) dsigma *= 0.5;


                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        nC(k,l,i,j) = dsigma(k,l);
                        nC(k,l,j,i) = dsigma(k,l);
                    }

                eps(i,j) = original;
                eps(j,i) = original;
                theMP.updateCurrentState(tn1, eps, phase_c, gradPh_c);
            }
        }

        // relative error less than 0.01%
        double error = 0.0;
        double norm = 0.0;
        for (unsigned i=0; i<3; i++)
            for (unsigned j=0; j<3; j++)
                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        error += pow(nC(i,j,k,l)-tg(i,j,k,l),2);
                        norm  += pow(tg(i,j,k,l),2);
                    }
        error = sqrt(error);
        norm = sqrt(norm);
        isok = (error/norm < 1e-4);

        os << "\n   2. Comparing tensor C with DStress.";
        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n      Test failed.";
            os << "\n      Relative error in DWeff computation: " <<  error/norm;
        }
    }


    // (3) compare stress and voigt stress


    // (4) compare contract tangent with cijkl
    if (true)
    {
        itensor Tvw;
        ivector v, w;
        v.setRandom();
        w.setRandom();
        contractWithTangent(v, w, Tvw);

        istensor S; S.setRandom();

        itensor nTvw; nTvw.setZero();
        for (unsigned i=0; i<3; i++)
            for (unsigned j=0; j<3; j++)
                for (unsigned k=0; k<3; k++)
                    for (unsigned l=0; l<3; l++)
                    {
                        nTvw(i,k) += tg(i,j,k,l)*v(j)*w(l);
                    }

        // relative error less than 0.01%
        itensor error = Tvw - nTvw;
        isok = (error.norm()/Tvw.norm() < 1e-4);

        os << "\n   4. Comparing contract tangent with C_ijkl v_j w_l.";
        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n      Test failed.";
            os << "\n      Relative error: " << error.norm()/Tvw.norm();
            os << "\n      C{a,b} \n" << Tvw;
            os << "\n      C_ijkl a_j b_l:\n" << nTvw;
        }
    }


    // (5) compare volumetric and deviatoric tangent contraction

    // (6) compare tangent with volumetric+deviatoric


    return isok;
}




void sPhaseMechMP::updateCurrentState(const double t, const istensor& strain, const double phase, const ivector& gradPh)
{
    phase_c  = phase;
    gradPh_c = gradPh;
    theSSMP->updateCurrentState(t, strain);

}




double sPhaseMechMP::volumetricEnergy() const
{
    return theSSMP->volumetricEnergy();
}




double sPhaseMechMP::volumetricStiffness() const
{
    return theSSMP->volumetricStiffness();
}
