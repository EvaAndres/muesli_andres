\documentclass[a4paper,12pt]{article}% {report} % {article}
% \usepackage[a4paper, top=2.5cm, bottom=2.5cm, left=2.2cm, right=2.2cm]{geometry}

% \usepackage{afterpage} 
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
% \usepackage{appendix}
\usepackage{array}
\usepackage{bm}
\usepackage{caption}
\usepackage{cite}
\usepackage{color}
\usepackage{comment}
\usepackage{dsfont}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{epstopdf}
\usepackage{fancyhdr}
\usepackage{float}
\usepackage{footnote}
\usepackage{fullpage}		% No headers, full page
\usepackage{gensymb}		% To enhance the number of symbols
\usepackage{graphicx}
\usepackage[hang,flushmargin]{footmisc} 
\usepackage{latexsym}
\usepackage{listings}
\usepackage[mathscr]{euscript}
\usepackage{mathrsfs}
\usepackage{multirow}
\usepackage{nomencl}
\usepackage{pdflscape}
\usepackage{placeins}
\usepackage{setspace}
\usepackage[english]{babel}
\usepackage{sectsty}
\usepackage{subfigure}
\usepackage{tablefootnote}
\usepackage{tabto}
\usepackage{textgreek}
\usepackage{times}			% Font
\usepackage{url}
\usepackage[utf8x]{inputenc}


\DeclareGraphicsExtensions{.pdf,.fig,.eps,.ps,.png,.svg}


\definecolor{codeblue}{rgb}{0,0,0.9}
\definecolor{codeblack}{rgb}{0,0,0}
\definecolor{codered}{rgb}{0.8,0,0}

\lstdefinestyle{stylepy}{
commentstyle=\color{codeblue},
keywordstyle=\color{codeblack}
}
\lstset{style=stylepy} % This style is selected for listings through this sentence

\setcounter{MaxMatrixCols}{30} % It allows more than 10 columns in a matrix
\setlength\parindent{0pt}     % Removes all indentation from paragraphs

\newcommand{\tr}{\operatorname{tr}}

\let\OLDthebibliography=\thebibliography
\def\thebibliography#1{\OLDthebibliography{#1}%
\addcontentsline{toc}{section}{11 \refname}}

\title{\Large DIFFUSION: \texttt{species Material} \& \texttt{masstransport Element}}
\author{Eva Mar\'ia Andr\'es L\'opez}

\fancypagestyle{bibliography}{%
	\pagestyle{fancy}
	\fancyhf{}
	\fancyhead[LE,RO]{\thepage}
	\fancyhead[RE,LO]{\fancyplain{}{Bibliography}}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\maketitle

\tableofcontents

\section{Symbols and dimensions}

\begin{table}[H]
  \begin{center}
    \caption{Symbols and dimensions}
    \begin{tabular} {l l l}
      \hline
      Description & Symbol & Dimension\\
      \hline
      Bulk modulus & $\kappa$ & M / L T$^2$ \\
      Coefficient of chemical expansion & $\beta$ &  \\
      Concentration of species & $\chi$ & mol (or M) / L$^3$ \\
      Concentration of lattice hydrogen & $\chi_L$ & mol (or M) / L$^3$ \\
      Concentration of trapped hydrogen & $\chi_T$ & mol (or M) / L$^3$ \\
      Chemical potential & $\mu$ & E / mol (or M) \\
      Density & $\rho$ & \\
      Density of host metal & $\rho_M$ & M/L$^3$\\
      Diffusivity & $D$ & L$^2$T\\
      Energetic resistance to plastic flow & $Y_{en}$ & M / L T$^2$ \\
      Free energy density & $\psi$ & E/L$^3$ \\
      Grand canonical potential & $G$ & E/L$^3$ \\
      Hydrostatic stress & $\sigma_h$ & F/L$^2$\\
      Mass diffusivity & $D$ & L$^2$/T\\
      Mass expansion & & 1/(mol/L$^3$)\\
      Mass flux & $\boldsymbol{j}$ & m/(L$^2$T)\\
      Mobility & $M$ & mol$^2$/(FL$^2$T) \\
      Molar mass of host metal & $M_M$ & M / mol \\
      Number of atoms / mol & $N$ & 1 \\
      Number of moles of lattice positions per reference volume & $N_L$ & mol /L$^3$\\
      Number of moles of trapping sites per reference volume & $N_T$ & mol /L$^3$\\
      Number of sites per trap & $\alpha$ & \\
      NILS per metal atom & $\beta_{NILS}$ & \\
      Occupancy & $\theta$ & $0 \leq \theta \leq 1$\\
      Occupancy in interstitials & $\theta_L$ & $0 \leq \theta \leq 1$\\
      Occupancy in trapping sites & $\theta_T$ & $0 \leq \theta \leq 1$\\
      Reference temperature & $\theta_0$ & T \\
      Transformation rate from lattice to trapped hydrogen & $h_{L \rightarrow T}$ & mol (or M) / L$^3$ T \\
      Universal gas constant & $R$ & E / (mol K) \\
      Volume of 1 mole of interstitials & $V_L$ & L$^3$/mol \\
      Volume of 1 mole of trap positions & $V_T$ & L$^3$/mol\\
      \hline
    \end{tabular}
  \end{center} 
\end{table}

\section{Hydrogen diffusion}

The diffusion of atomic hydrogen through a metal is governed by the hops of such atoms among interstitial lattice sites, and their chances of remaining trapped in  heterogeneities (dislocations, impurities, grain boundaries, voids, interfaces, etc.).\\

Hydrogen can reside either in normal interstitial lattice sites (NILS) or at trapping sites. Oriani postulated that these two populations must be in equilibrium. This is true when diffusion times are relatively large compared to the time it takes to replenish the sites \cite{Oriani1994}.\\

\subsection{The coupled problem. Balance laws}
\label{coupled_problem_balance_laws}

We consider a body to be an open bounded set $\Omega \in \mathbb{R} ^3$ with smooth boundary $\partial \Omega$. To model the deformation of this body and the diffusion of a single species through it, we define a vector field of displacements $\mathbf{u}: \Omega \rightarrow \mathbb{R}^3$ and a scalar field $\mu : \Omega \rightarrow \mathbb{R}$ representing the chemical potential of the species. To introduce the boundary conditions, we split $\partial \Omega$ as

\begin{equation}
  \partial \Omega = \overline{\partial_u \Omega \cup \partial_t \Omega} = \overline{\partial_\mu \Omega \cup \partial_j \Omega}
\end{equation}

with $\partial_u \Omega \cup \partial_t \Omega = \partial_\mu \Omega \cup \partial_j \Omega = \emptyset$. At every point $\boldsymbol{x} \in \Omega$, let us assume the existence of a
grand canonical potential density $G = G(\boldsymbol{\varepsilon} (\boldsymbol{x}), \mu(\boldsymbol{x}), \boldsymbol{\xi}(\boldsymbol{x}), \boldsymbol{x})$, where $\boldsymbol{\varepsilon} (\boldsymbol{x}) = \nabla ^s \boldsymbol{u}(\boldsymbol{x})$ is the infinitesimal strain tensor and $\boldsymbol{\xi} (\boldsymbol{x})$ is an array of
internal variables that are employed to model the inelastic response of the material.
Here, and below, $\nabla$ denotes the gradient operator on scalar or vector fields and $\nabla ^ s$ refers to its symmetrized form. Standard thermodynamic arguments lead to
the definitions

\begin{equation}
  \boldsymbol{\sigma} := \frac{\partial G (\boldsymbol{\varepsilon}, \mu, \boldsymbol {\xi})}{\partial \boldsymbol{\varepsilon}}, \quad \chi := \frac{\partial G (\boldsymbol{\varepsilon}, \mu, \boldsymbol {\xi})}{\partial \mu}, \quad \boldsymbol{\beta} := -\frac{\partial G (\boldsymbol{\varepsilon}, \mu, \boldsymbol {\xi})}{\partial \boldsymbol{\xi}} 
\end{equation}

for the stress tensor, concentration and dissipative forces, respectively. We note
that in these equations, and henceforth, we have removed the dependency on the point $\boldsymbol {x}$ for economy of notation.

The coupled stress-diffusion problem is described by the quasistatic mechanical equations

\begin{equation}
  \begin{split}
    \nabla \cdot \boldsymbol{\sigma} + \boldsymbol {f} = \boldsymbol {0} & \quad \text{in $\Omega$,} \\
    \boldsymbol{\sigma} \boldsymbol{n} = \boldsymbol{t} & \quad \text{on $\partial_t \Omega$,}\\
    \boldsymbol{u} = \bar{\boldsymbol{u}} & \quad \text{on $\partial_u \Omega$,}
  \end{split}
\end{equation}

and the transport problem

\begin{equation}
  \begin{split}
    \dot{\chi} + \nabla \cdot \boldsymbol{j} = 0 & \quad \text{in $\Omega$,} \\
    \boldsymbol{j} \cdot \boldsymbol{n} = -\tilde{j} & \quad \text{on $\partial_t \Omega$,}\\
    \mu = \bar{\mu} & \quad \text{on $\partial_u \Omega$}.
  \end{split}
  \label{transport_problem_eq}
\end{equation}

Here, $\boldsymbol{f}$ are the applied body forces, $\boldsymbol{n}$ is the unit outward vector to the boundary, $\nabla \cdot$ stands for the divergence operator, the superposed dot indicates the time derivative, $\boldsymbol{j}$ is the massflux, $\boldsymbol{t}$, is the imposed traction and $\tilde{j}$ is the boundary flux of mass. We have employed a dot between tensors or vectors to denote the corresponding dot product.

However, it is more convenient to formulate the transport problem in terms of its natural variable, the chemical potential $\mu$, which leads to the following expressions:

\begin{equation}
  \begin{split}
    c\dot{\mu} + \nabla \cdot \boldsymbol{j} = 0 & \quad \text{in $\Omega$,} \\
    \boldsymbol{j} \cdot \boldsymbol{n} = -\tilde{j} & \quad \text{on $\partial_t \Omega$,}\\
    \mu = \bar{\mu} & \quad \text{on $\partial_u \Omega$}.
  \end{split}
\end{equation}\\

\section{Pure diffusion problem}
\label{pure_diffusion_problem}

Prior to formulate the stress-diffusion problem, or the pure diffusion of two species (NILS and trapped hydrogen), it is important to understand the problem of pure diffusion of one species. This problem will be approached hereafter.

\subsection{Balance laws}

We consider the same body, scalar field $\mu : \Omega \rightarrow \mathbb{R}$ and boundary conditions stated above. At every point $\boldsymbol{x} \in \Omega$, let us assume the existence of a
grand canonical potential density $G = G(\mu(\boldsymbol{x}), \boldsymbol{x})$. Standard thermodynamic arguments lead to
the definition of the concentration:

\begin{equation}
 \chi := - \frac{\partial G(\mu)}{\partial \mu} \end{equation}

where the dependency on the point $\boldsymbol {x}$ has been removed for economy of notation.

The diffusion problem of one species is described by the transport problem

\begin{equation}
  \begin{split}
    c\dot{\mu} + \nabla \cdot \boldsymbol{j} = 0 & \quad \text{in $\Omega$,} \\
    \boldsymbol{j} \cdot \boldsymbol{n} = -\tilde{j} & \quad \text{on $\partial_t \Omega$,}\\
    \mu = \bar{\mu} & \quad \text{on $\partial_u \Omega$}.
  \end{split}
\end{equation}\\

\section{Equilibrium thermodynamics}

\subsection{State variables}

The following table shows the state variables and their conjugated variables.

\begin{table}[H]
  \begin{center}
    \caption{State variables}
    \begin{tabular} {l c c}
      \hline
      Mechanism	&	State variable	&	Conjugated variable\\
      \hline
       Chemical potential	&	$\mu$		&	$\chi$ \\
      \hline  
    \end{tabular}
  \end{center} 
\end{table}

\subsection{State laws}

Let assume that there is a Helmholtz free energy density function $\psi = \hat{\psi} (\chi)$, convex on its argument.
The Legendre Transform $G$ is the grand canonical potential defined by

\begin{equation}
  G(\mu) = \min_{\chi} (\psi(\chi) - \chi \mu).
\end{equation}

The following relation holds

\begin{equation}
  \psi = -\frac{\partial G}{\partial \mu}.
\end{equation}

Therefore, we have the following state laws

\begin{equation}
  \mu = \frac{\partial \psi}{\partial \chi},
\end{equation}

\begin{equation}
  \chi= - \frac{\partial G}{\partial \mu}.
\end{equation}\\    

\subsection{Free energy for pure diffusion of one species}

The Helmholtz free energy density function is modeled as \cite{Dileo2013}

\begin{equation}
  \hat{\psi} (\chi) = \mu_0 \chi + R \theta_0 N [\theta \log \theta + ( 1-\theta) \log ( 1 - \theta ) ],
\end{equation}

where $\theta = \chi/N$ is the occupancy factor.\\

\section{Thermodynamic derivation}

\subsection{First derivative: Concentration $\chi$}

  \begin{equation}
    \mu = \frac{\partial \psi}{\partial \chi}
  \end{equation}
		
  \begin{equation}
    \mu = \mu_0 + R \theta_0 \log{ \left( \frac{\theta}{ 1-\theta} \right)}
  \end{equation}

  \begin{equation}
    \chi = \frac{NE}{1+E}
  \end{equation}
			
  where
  
  \begin{equation}
    E = \exp{\big(\frac{\mu-\mu_0}{R\theta_0}\big)}.
  \end{equation}
  
\subsection{Second derivative: Chemical capacity $c$}

  \begin{equation}
    c = - \frac{\partial^2 G}{\partial \mu^2} = \frac{\partial \chi}{\partial \mu} = \frac{NE'}{1+E} 
  \end{equation}
  
  where
  
  \begin{equation}
    E' = \frac{E}{R\theta_0}.
  \end{equation}\\

\section{Diffusion. Fick's Laws}

\subsection{First law}

Fick's first law states that the mass flux $\boldsymbol{j}$ satisfies

\begin{equation}
  \boldsymbol{j} = - D \nabla \chi
\end{equation}

for some diffusivity $D$. This is justified since the standard kinematic argument lead to a nonlinear relation of the form

\begin{equation}
  \boldsymbol{j} = - B \chi \nabla \mu,
\end{equation}

where $\mu$ is the chemical potential. In equilibrium $\mu = C \log \chi$ and Fick's law follows. The term $B \chi$ is the mobility $M$. Theoretically, the mobility is a tensor but it is often taken as isotropic. In such case, we can rewrite the previous expression as

\begin{equation}
  \boldsymbol{j} = - m \chi \nabla \mu,
\end{equation}

\subsection{Second law}

It reads

\begin{equation}
  \frac{\partial \chi}{\partial t} - \nabla \cdot (D \nabla \chi) = 0.
\end{equation}

\section{Kinetics}

For this model, it is convenient to use the kinetic potential
  \begin{equation}
    \Omega = \frac{1}{2} m ||\boldsymbol{g}||^2
  \end{equation}
  
  where $\boldsymbol{g} = -\nabla \mu$, that leads to the mass flux
  
  \begin{equation}
    \boldsymbol{j} = - \frac{\partial \Omega}{\partial \nabla \mu} = \frac{\partial \Omega}{\partial \boldsymbol g} = - m \nabla \mu.
  \end{equation}
  
  We also have the tangent
  
    \begin{equation}
      \boldsymbol m = - \frac{\partial \boldsymbol j}{\partial \nabla \mu} = \frac{\partial \Omega}{\partial (\nabla \mu)^2} \cdot \boldsymbol 1.
    \end{equation}
    
    which is the mobility tensor.\\

\section{Linear diffusion}

  The following expressions summarize the previous equations particularized for linear diffusion. It must be highlighted that both chemical capacity and mobility are constant in a linear diffusion problem.

  \begin{itemize}
    \item \textbf{First derivative: Concentration}
      \begin{equation}
        \chi = \chi (\mu_0) + c_0 (\mu - \mu_0)
      \end{equation}
        where $\chi (\mu_0) = N/2$.
        
    \item \textbf{Second derivative: Constant chemical capacity}
      \begin{equation}
        c_0 = N/(4R\theta_0)
      \end{equation}
      
    \item \textbf{Third derivative: Chemical capacity derivative}
      \begin{equation}
        \frac{\partial c}{\partial \mu} = 0
      \end{equation}
      
    \item \textbf{Mobility}
      \begin{equation}
        \boldsymbol{m} = m \cdot \boldsymbol{1}
      \end{equation}
      
      where
      \begin{equation}
        m = \frac{DN}{2R \theta_0}.
      \end{equation}
      
      \item \textbf{Mass flux derivative}
        \begin{equation}
          \frac{\partial \boldsymbol j}{\partial \mu} = \boldsymbol 0
        \end{equation}\\
      
  \end{itemize}

\section{Non linear diffusion}

  The following expressions summarize the application for non linear

  \begin{itemize}
  
    \item \textbf{First derivative: Concentration}
    
    \begin{equation}
      \mu = \frac{\partial \psi}{\partial \chi}
    \end{equation}
    
    \begin{equation}
      \mu = \mu_0 + R \theta_0 \log{ \left( \frac{\theta}{ 1-\theta} \right)}
    \end{equation}
     
    \begin{equation}
      \chi = \frac{NE}{1+E}
    \end{equation}
    where $E = \exp{\big(\frac{\mu-\mu_0}{R\theta_0}\big)}$ 
        
    \item \textbf{Second derivative: Chemical capacity}
      \begin{equation}
        c = - \frac{\partial^2 G}{\partial \mu^2} = \frac{\partial \chi}{\partial \mu} = \frac{NE'}{1+E}
      \end{equation}
      
      where $E' = E/(R\theta_0)$ 
      
    \item \textbf{Third derivative: Chemical capacity derivative}
      \begin{equation}
        \frac{\partial c}{\partial \mu} = - \frac{\partial^3 G}{\partial \mu^3} = \frac{NE''}{(1+E)^2}  - \frac{2NE'^2}{(1+E)^3}
      \end{equation}
      
      where $E'' = E/(R\theta_0)^2$.
      
    \item \textbf{Mobility}
      \begin{equation}
        \boldsymbol{m} = \frac{D\chi}{R \theta_0} \cdot \boldsymbol{1}.      \end{equation}
      
      \item \textbf{Mass flux derivative}
        \begin{equation}
          \frac{\partial \boldsymbol{j}}{\partial \mu} = \frac{DNE'}{R \theta_0 (1+E)^2} \nabla \mu
        \end{equation}\\
      
  \end{itemize}

\section{Hydrogen diffusion}

Hereafter, the problem of pure diffusion of hydrogen through a metal, considering the two species (NILS and trapped hydrogen) is approached.

\subsection{Balance laws}

As already mention in section \ref{coupled_problem_balance_laws}, the diffusion problem is described by the transport problem equations \ref{transport_problem_eq}. In the hydrogen diffusion problem, we have

\begin{equation}
  \dot{\chi} = \dot{\chi}_L + \dot{\chi}_T
\end{equation}

and since $m_T = 0$ for the trapped hydrogen mobility, $\boldsymbol{j}_T = 0$. Then

\begin{equation}
  \boldsymbol{j} = \boldsymbol{j}_L + \boldsymbol{j}_T = \boldsymbol{j}_L.
\end{equation}

Therefore, the hydrogen diffusion problem can be described as

\begin{equation}
  \begin{split}
    \dot{\chi}_L + \dot{\chi}_T + \nabla \cdot \boldsymbol{j}_L = 0 & \quad \text{in $\Omega$,} \\
    \boldsymbol{j} \cdot \boldsymbol{n} = -\tilde{j} & \quad \text{on $\partial_t \Omega$,}\\
    \chi = \bar{\chi} & \quad \text{on $\partial_\chi \Omega$}.
  \end{split}
\end{equation}

We also have

\begin{equation}
    \dot{\chi}_L + \dot{\chi}_T = \left[ 1 + \frac{\partial \chi_T}{\partial \chi_L} \right] \dot{\chi}_L = \left[ 1 + \frac{\partial \chi_T}{\partial \chi_L} \right] \frac{d}{dt} \left( - \frac{\partial G}{\partial \mu_L} \right),
\end{equation}\\

and taking into account that $G = G (\mu_L,\mu_T)$ and the equilibrium condition $\mu_T \equiv \mu_L$ \cite{Oriani1994}:

\begin{equation}
  \begin{split}
    \left[ 1 + \frac{\partial \chi_T}{\partial \chi_L} \right] \frac{d}{dt} \left( - \frac{\partial G}{\partial \mu_L} \right) =  \left[ 1 + \frac{\partial \chi_T}{\partial \chi_L} \right] \left[\frac{\partial}{\partial \mu_T} \left( - \frac{\partial G}{\partial \mu_L} \right) \dot{\mu}_T + \frac{\partial}{\partial \mu_L} \left( - \frac{\partial G}{\partial \mu_L} \right) \dot{\mu}_L   \right] = \\
    \left[ 1 + \frac{\partial \chi_T}{\partial \chi_L} \right] \left[\frac{\partial}{\partial \mu_T} \left( - \frac{\partial G}{\partial \mu_L} \right) + \frac{\partial}{\partial \mu_L} \left( - \frac{\partial G}{\partial \mu_L} \right) \right]  \dot{\mu}_L
  \end{split}    
\end{equation}\\

We can rewrite this equation as follows

\begin{equation}
    \dot{\chi}_L + \dot{\chi}_T = c_{LT}^{eff} \cdot \dot{\mu}_L
    \label{cL_eff_general}
\end{equation}

where

\begin{equation}
  c_{LT}^{eff}  = \left[ 1 + \frac{\partial \chi_T}{\partial \chi_L} \right] \cdot c_L^{eff}
\end{equation}

and

\begin{equation}
  c_L^{eff} = \left[\frac{\partial}{\partial \mu_T} \left( - \frac{\partial G}{\partial \mu_L} \right) + \frac{\partial}{\partial \mu_L} \left( - \frac{\partial G}{\partial \mu_L} \right) \right]. 
\end{equation}\\

Finally, the hydrogen diffusion problem is written as

\begin{equation}
  \begin{split}
    c_{LT}^{eff} \dot{\mu}_L + \nabla \cdot \boldsymbol{j}_L = 0 & \quad \text{in $\Omega$,} \\
    \boldsymbol{j} \cdot \boldsymbol{n} = -\tilde{j} & \quad \text{on $\partial_t \Omega$,}\\
    \mu = \bar{\mu} & \quad \text{on $\partial_\mu \Omega$}.
  \end{split}
\end{equation}

The expressions for $c_L^{eff}$ and $c_{LT}^{eff}$ shall be obtained in section \ref{hydrogen_thermodynamic_derivation}.\\

\subsection{State variables}

The following table shows the state variables and their conjugated variables.

\begin{table}[H]
  \begin{center}
    \caption{State variables}
    \begin{tabular} {l c c}
      \hline
      Mechanism	&	State variable	&	Conjugated variable\\
      \hline
       NILS chemical potential	&	$\mu_L$		&	$\chi_L$ \\
       Trapping chemical potential	&	$\mu_T$		&	$\chi_T$ \\
      \hline  
    \end{tabular}
  \end{center} 
\end{table}

\subsection{State laws}

Let assume that there is a Helmholtz free energy density function $\psi = \hat{\psi} (\chi) = \hat{\psi} (\chi_L,\chi_T)$, convex on its argument.
The Legendre Transform $G$ is the grand canonical potential defined by

\begin{equation}
  G(\mu) = \min_{\chi} (\psi(\chi) - \chi \mu).
\end{equation}

The following relation holds

\begin{equation}
  \psi = -\frac{\partial G}{\partial \mu}.
\end{equation}

Therefore, we have the following state laws

\begin{equation}
  \mu = \frac{\partial \psi}{\partial \chi},
\end{equation}

\begin{equation}
  \chi= - \frac{\partial G}{\partial \mu}.
\end{equation}    

\subsection{Helmholtz free energy for hydrogen diffusion}

The Helmholtz free energy density function is now modeled as

\begin{equation}
    \hat{\psi} (\chi) = \hat{\psi}_L (\chi) + \hat{\psi}_T (\chi),
\end{equation}

where 

\begin{equation}
  \hat{\psi}_{\alpha} (\chi_{\alpha})  = \mu_{\alpha 0} \chi_{\alpha} + R \theta_0 N_{\alpha} [\theta_{\alpha} \log \theta_{\alpha} + ( 1-\theta_{\alpha}) \log ( 1 - \theta_{\alpha} ) ],
\end{equation}

where $\theta_{\alpha} = \chi_{\alpha}/N_{\alpha}$ is the occupancy factor and $\alpha$ stands for $L$ or $T$, as required.

\subsection{Thermodynamic derivation}
\label{hydrogen_thermodynamic_derivation}

\subsubsection{First derivative: Concentration}

\begin{equation}
  \mu_{\alpha} = \frac{\partial \psi}{\partial \chi_{\alpha}}
\end{equation}
		
\begin{equation}
  \mu_{\alpha} = \mu_{\alpha 0} + R \theta_0 \log{ \left( \frac{\theta_{\alpha}}{ 1-\theta_{\alpha}} \right)}
  \label{mu_alpha_eq}
\end{equation}

Then

\begin{equation}
 \chi_{\alpha} = \frac{N_{\alpha}E_{\alpha}}{1+E_{\alpha}}
\end{equation}
			
where

\begin{equation}
  E_{\alpha} = \exp{\big(\frac{\mu_{\alpha}-\mu_{\alpha 0}}{R\theta_0}\big)}
\end{equation}

and

\begin{equation}
  \chi = \chi_L + \chi_T
\end{equation}

It is important to highlight that, using the condition equilibrium \cite{Oriani1994} and the equations \ref{mu_alpha_eq}, we can deduce the following relationship between $\mu_T$ and $\mu_L$:
  
\begin{equation}
  \frac{\theta_{T}}{1 - \theta_T} = \frac{\theta_{L}}{1 - \theta_L} K_T,
  \label{relation_theta_T_theta_L}
\end{equation}
 
where

\begin{equation}
  K_T = \exp{\left( \frac{W_B}{R \theta_0} \right)}
\end{equation}  

and

\begin{equation}
  W_B = \mu_{L 0} - \mu_{T 0}.
\end{equation} \\
  
\subsubsection{Second derivative: Effective chemical capacity}

The first step is to obtain the chemical capacity of each species:
\begin{equation}
  c_{\alpha} = \frac{\partial \chi_{\alpha}}{\partial \mu_{\alpha}} = \frac{N_{\alpha} E'_{\alpha}}{1 + E_{\alpha}},
\end{equation}

\begin{equation}
  E'_{\alpha} = \frac{E_{\alpha}}{R \theta_0},
\end{equation}

\begin{equation}
  E_{\alpha} = \exp{ \left( \frac{\mu_{\alpha} - \mu_{{\alpha}0}}{R \theta_0} \right) }.
\end{equation}

From the relation \ref{relation_theta_T_theta_L} we obtain:

\begin{equation}
  \frac{\partial \chi_T}{\partial \chi_L} = \frac{K_T N_T N_L}{\left(N_L - \chi_L + K_T \chi_L \right)^2 }.
\end{equation}

Additionally, making the necessary derivatives and finally considering the equilibrium condition \cite{Oriani1994}, we have

\begin{equation}
  - \frac{\partial G}{\partial \mu_L} = \chi_L + \chi_T,
\end{equation}

\begin{equation}
   c_{L}^{eff} = \frac{\partial}{\partial \mu_T} \left(- \frac{\partial G}{\partial \mu_L} \right) + \frac{\partial}{\partial \mu_L} \left(- \frac{\partial G}{\partial \mu_L} \right) =  \frac{\partial}{\partial \mu_T} \left( \chi_L + \chi_T \right) + \frac{\partial}{\partial \mu_L} \left( \chi_L + \chi_T \right).
\end{equation}

Then

\begin{equation}
  \begin{split}
  c_{L}^{eff} = \left(  \frac{\partial \chi_T}{\partial \mu_T} + \frac{\partial \chi_L}{\partial \mu_L} \right) = c_L + c_T.
  \end{split}
\end{equation}

And finally

\begin{equation}
  c_{LT}^{eff} = \left[ 1 + \frac{\partial \chi_T}{\partial \chi_L} \right] c_L^{eff}= \left[ 1 + \frac{K_T N_T N_L}{\left(N_L - \chi_L + K_T \chi_L \right)^2 } \right]\left(c_L + c_T \right).
\end{equation}

\subsubsection{Third derivative: Effective chemical capacity}

\begin{equation}
  \begin{split}
    \frac{\partial c_{LT}^{eff}}{\partial \mu} = \left[ 1 + \frac{K_T N_T N_L}{\left(N_L - \chi_L + K_T \chi_L \right)^2 } \right] \left( \frac{\partial c_L}{\partial \mu_L} + \frac{\partial c_T}{\partial \mu_T} \right) + \\
    c_L \left( c_L + c_T \right) \frac{ \left( 2 K_T N_T N_L \right) \left( K_T - 1\right)}{ \left(N_L - \chi_L + K_T \chi_L \right)^3 }.
  \end{split}
\end{equation}

\subsection{Kinetics}

For this model, it is convenient to use the kinetic potential

\begin{equation}
  \Omega = \frac{1}{2} m ||\boldsymbol{g}||^2
\end{equation}
  
where $\boldsymbol{g} = -\nabla \mu$, that leads to the mass flux
  
\begin{equation}
  \boldsymbol{j} = - \frac{\partial \Omega}{\partial \nabla \mu} = \frac{\partial \Omega}{\partial \boldsymbol g} = - m_L \nabla \mu_L.
\end{equation}
  
We also have the tangent
  
\begin{equation}
    \boldsymbol m_L = - \frac{\partial \boldsymbol j}{\partial \nabla \mu_L} = \frac{\partial \Omega}{\partial (\nabla \mu_L)^2} \cdot \boldsymbol 1.
\end{equation}
    
which is the mobility tensor.
    
\begin{itemize}
  
  \item \textbf{Mobility}
    \begin{equation}
        \boldsymbol{m_L} = \frac{D\chi_L}{R \theta_0} \cdot \boldsymbol{1}.      \end{equation}
      
    \item \textbf{Mass flux derivative}
      \begin{equation}
        \frac{\partial \boldsymbol{j}}{\partial \mu_L} = \frac{D}{R \theta_0} \frac{\partial \chi_L}{\partial \mu_L} \nabla \mu_L = \frac{DN_LE_L'}{R \theta_0 (1+E_L)^2} \nabla \mu_L
        \end{equation}\\
      
  \end{itemize}

\section{Implementation in MUESLI and IRIS}

In this section, there is a summary of the functions implemented in \texttt{MUESLI} for the diffusion material, \texttt{species.h} \texttt{species.cpp}. The diffusive element is implemented in \texttt{IRIS} through the files \texttt{masstransport.h} and \texttt{masstransport.cpp}. A brief description of \texttt{textImplementation(std::ostream\& os)} is included

\subsection{Pure diffusion}

\begin{itemize}
  \item \textbf{State variables:} $\mu$, $\nabla \mu$	
  
  \item \textbf{MUESLI functions:}
  
      \texttt{concentration()}
      
      \texttt{chemicalCapacity()}
      
      \texttt{chemicalCapacityDerivative()}\\
      
      \texttt{chemicalPotential()}
      
      \texttt{massFlux(ivector \&q)}
      
      \texttt{massFluxDerivative()}\\
      
      \texttt{mobility()}
      
      \texttt{mobilityDerivative()}\\
		
      \texttt{freeEnergy()}
      
      \texttt{grandCanonicalPotential()}
      
      \texttt{kineticPotential()}
      
    \item \textbf{\texttt{testImplementation(std::ostream\& os):}}
      
      \begin{enumerate}
      
      	  \item{Concentration}
	  
      	    Comparison between the programmed concentration function, \texttt{\footnotesize concentration()}, and the numerical derivative of the grand canonical potential with respect to $\mu$, $- \frac{\partial G(\mu)}{\partial \mu}\big |_{num}$.\\
	    
	    \item{Chemical capacity}
	  
      	    Comparison between the programmed chemical capacity function, \texttt{\footnotesize chemicalCapacity()} and the numerical derivative of the concentration with respect to $\mu$, $ \frac{\partial \chi(\mu)}{\partial \mu}\big |_{num}$.\\
	    
	    \item{Chemical capacity derivative}
	  
      	    Comparison between the programmed chemical capacity derivative function,\\ \texttt{\footnotesize chemicalCapacityDerivative()} and the numerical derivative of the chemical capacity derivative with respect to $\mu$, $ \frac{\partial c(\mu)}{\partial \mu}\big |_{num}$.\\
	    
	    \item{Mobility derivative}
	  
      	    Comparison between the programmed mobility derivative function, \texttt{\footnotesize mobilityDerivative()} and the numerical derivative of the mobility with respect to $\mu$, $ \frac{\partial m(\mu)}{\partial \mu}\big |_{num}$.\\
	    
	    \item{Mass flux derivative}
	  
      	    Comparison between the programmed mass flux derivative function, \texttt{\footnotesize massFluxDerivative()} and the numerical derivative of the mobility with respect to $\mu$, $ \frac{\partial \boldsymbol j (\mu)}{\partial \mu}\big |_{num}$.\\
	    
	    \item{Mass flux}
	  
      	    Comparison between the programmed mass flux function, \texttt{\footnotesize massFlux(ivector \&q)}  and the numerical derivative of the kinetic potential with respect to $\nabla \mu$, $ - \frac{\partial \Omega}{\partial \nabla \mu}\big |_{num}$.\\
	    
	    \item{Mobility}
	  
      	    Comparison between the programmed mobility function, \texttt{\footnotesize mobility()}  and the numerical derivative of the mass flux with respect to $\nabla \mu$, $ - \frac{\partial \boldsymbol j}{\partial \nabla \mu}\big |_{num}$.

	\end{enumerate}

  \end{itemize}
  
  \subsection{Hydrogen diffusion}

\begin{itemize}
  \item \textbf{State variables:} $\mu_L$, $\nabla \mu_L$	
  
  \item \textbf{MUESLI functions:}
  
      \texttt{concentration()}
      
      \texttt{concentrationT()}
      
      \texttt{chemicalCapacity()}
      
      \texttt{chemicalCapacityDerivative()}
    
    \texttt{effectiveChemicalCapacity()}
    
    \texttt{effectiveChemicalCapacityDerivative()}\\

    \texttt{chemicalPotential()}
      
    \texttt{massFlux(ivector \&q)}
      
    \texttt{massFluxDerivative()}\\
      
    \texttt{mobility()}
      
    \texttt{mobilityDerivative()}\\
		
    \texttt{freeEnergy()}
      
    \texttt{grandCanonicalPotential()}
      
    \texttt{kineticPotential()}
      
  \item \textbf{\texttt{testImplementation(std::ostream\& os):}}
      
  \begin{enumerate}
      
    \item{Concentration}
	  
    Comparison between the programmed concentration function, \texttt{\footnotesize concentration()}, and the numerical derivative of the grand canonical potential with respect to $\mu$, $- \frac{\partial G(\mu)}{\partial \mu}\big |_{num}$.\\
	    
    \item{Chemical capacity}
	  
    Comparison between the programmed chemical capacity function, \texttt{\footnotesize chemicalCapacity()} and the numerical derivative of the concentration with respect to $\mu$, $ \frac{\partial \chi(\mu)}{\partial \mu}\big |_{num}$.\\
	    
    \item{Chemical capacity derivative}
	  
    Comparison between the programmed chemical capacity derivative function,\\ \texttt{\footnotesize chemicalCapacityDerivative()} and the numerical derivative of the chemical capacity derivative with respect to $\mu$, $ \frac{\partial c(\mu)}{\partial \mu}\big |_{num}$.\\
	    
    \item{Mobility derivative}
	  
    Comparison between the programmed mobility derivative function, \texttt{\footnotesize mobilityDerivative()} and the numerical derivative of the mobility with respect to $\mu$, $ \frac{\partial m(\mu)}{\partial \mu}\big |_{num}$.\\
	    
    \item{Mass flux derivative}
	  
    Comparison between the programmed mass flux derivative function, \texttt{\footnotesize massFluxDerivative()} and the numerical derivative of the mobility with respect to $\mu$, $ \frac{\partial \boldsymbol j (\mu)}{\partial \mu}\big |_{num}$.\\
	    
    \item{Mass flux}
	  
    Comparison between the programmed mass flux function, \texttt{\footnotesize massFlux(ivector \&q)}  and the numerical derivative of the kinetic potential with respect to $\nabla \mu$, $ - \frac{\partial \Omega}{\partial \nabla \mu}\big |_{num}$.\\
	    
    \item{Mobility}
	  
    Comparison between the programmed mobility function, \texttt{\footnotesize mobility()}  and the numerical derivative of the mass flux with respect to $\nabla \mu$, $ - \frac{\partial \boldsymbol j}{\partial \nabla \mu}\big |_{num}$.

  \end{enumerate}

\end{itemize}


\begin{thebibliography}{0}

  \bibitem{Anand2011} Anand L. (2011). A thermo-mechanically-coupled theory accounting for hydrogen diffusion and large elastic-viscoplastic deformations of metals. International Journal of Solids and Structures 48, 6, 962-971.
  
  \bibitem{Dileo2013} Claudio V.D.L., Lallit A. (2013). Hydrogen in metals: A coupled theory for species diffusion and large elastic-plastic deformations. International Journal of Plasticity 43, 42-69.

  \bibitem{Gobbi2018} Gobbi G., Colombo Ch., Miccoli S., Vergani S. (2018). A weakly coupled implementation of hydrogen embrittlement in FE analysis. Finite Elements in Analysis and Design 141, 17-25.
  
  \bibitem{Nak-Hyun2012} Nak-Hyun K., Chang-Sik O., Yun-Jae, K., Kee-Bong Y., Young-Wha M. (2012). Hydrogen-assisted stress corrosion cracking simulation using the stress-modified fracture strain model. Journal of Mechanical Science and Technology 26, 8, 2631-2638.
 
  \bibitem{Martinezpaneda2018} Mart\'inez-Pa\~neda E., Golahmar A., Niordson C.F. (2018). A phase field formulation for hydrogen assisted cracking. Computer Methods in Applied Mechanics and Engineering 342, 742-761.
  
  \bibitem{Oriani1994} Oriani R.A. (1994). The physical and metallurgical aspects of hydrogen in metals. Fusion Technology 26, 4, 235-266.

  \bibitem{Sofronis1989} Sofronis P., McMeeking R.M. (1989). Numerical analysis of hydrogen transport near a blunting crack tip. Journal of the Mechanics and Physics of Solids 37, 3, 317-350.

  \bibitem{taha2001} Taha A., Sofronis P. (2001). A micromechanics approach to the study of hydrogen transport and embrittlement. Engineering Fracture Mechanics 68, 6, 803-837.
  
\end{thebibliography}

\end{document}
